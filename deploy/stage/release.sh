#!/usr/bin/env bash

set -ex;

git tag $1
git push stage && git push stage --tags
./deploy.sh