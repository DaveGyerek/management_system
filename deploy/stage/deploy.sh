#!/bin/bash
source ./env;
set -ex;

function remote-exec {
    echo "deploy script is running remotely...";
    pull-src;
    build-src;
}

function local-exec {
    scp -i ./key ./env root@${REMOTE_HOST_NAME}:~/env
    scp -i ./key ./deploy.sh root@${REMOTE_HOST_NAME}:~/deploy.sh
    ssh -i ./key -t root@${REMOTE_HOST_NAME} "chmod +x ~/deploy.sh && ~/deploy.sh run";
}

function pull-src {
    echo -e "\nrunning pull-src..."
    cd ${SRC_ROOT}
    # PULLING REPOSITORY FROM BIT BUCKET WITH LATEST TAG
    echo "  pulling source from repository..."
    git checkout . && git pull origin master --tags &&
    # GET LATEST TAG
    LATEST_TAG=$(git describe --tags)
    # CHECKOUT THE LATEST TAG TO HAVE TO SOURCE CODE MATCH THAT
    echo "  the latest tag is ${LATEST_TAG}..."
    git checkout ${LATEST_TAG} &> /dev/null

    echo "  change ownership to ${USERNAME} for ${SRC_ROOT}"
    chown -R ${USERNAME}:${USERNAME} ${SRC_ROOT}
    echo -e "pull-src done!\n";
}

function build-src {
    echo -e "\nrunning build-src..."
    cd ${DEPLOY_ROOT}
    echo -e "  building docker image..."
    export LATEST_TAG=$(git describe --tags)
    # REMOVE PREVIOUS VERSIONS
    echo -e "  removing previous docker images..."
    docker-compose rm -f && docker image prune -a --force --filter "until=1h"
    # BUILD SERVICES WITH DOCKER-COMPOSE
    echo -e "  building new images..."
    docker-compose up -d --build
    echo -e "build-src done!\n";
}

if [ $# -eq 0 ]
  then
    echo "deploying deploy script...";
    local-exec;
    exit 0;
  else
    echo "running deploy script...";
    remote-exec;
    exit 0;
fi
