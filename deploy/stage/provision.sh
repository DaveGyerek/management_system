#!/bin/bash
source ./env;

read -d '' SECURITY_SSH_CONFIG << EOF
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding yes
PrintMotd no
AcceptEnv LANG LC_*
Subsystem       sftp    /usr/lib/openssh/sftp-server
PasswordAuthentication no
PermitRootLogin yes
EOF

read -d '' STARTUP_CONFIG << EOF
#!/bin/sh -e
sudo -H -u management_system bash -c "cd ~/code/deploy/stage && export LATEST_TAG=`git describe --tags` && docker-compose up --build -d"
exit 0
EOF

function remote-exec {
    echo "provision script is running remotely...";
    security-config;
    get-src;
    startup-config;
}

function local-exec {
    scp -i ./key ./key root@${REMOTE_HOST_NAME}:~/.ssh/id_rsa &&\
    scp -i ./key ./key.pub root@${REMOTE_HOST_NAME}:~/.ssh/id_rsa.pub &&\
    scp -i ./key ./provision.sh root@${REMOTE_HOST_NAME}:~/provision.sh &&\
    scp -i ./key ./env root@${REMOTE_HOST_NAME}:~/env &&\
    ssh -i ./key -t root@${REMOTE_HOST_NAME} "chmod +x ~/provision.sh && ~/provision.sh run";
}

function security-config {
    echo -e "\nrunning security-config..." &&\
    echo "  update-upgrade..." &&\
    # APT-GET UPDATES FOR SECURITY
    apt-get update -y && apt-get upgrade -y && apt-get update -y && apt autoremove -y &&\
    echo "  private key permissions are secure..." &&\
    # PROTECT PRIVATE KEY WITH PERMISSIONS
    chmod 600 ~/.ssh/id_rsa &&\
    # CONFIGURE SSH, FORBID PASSWORD AUTH
    echo "$SECURITY_SSH_CONFIG" > /etc/ssh/sshd_config && service ssh restart &&\
    echo "  ssh config updated..." &&\
    # GENERATE RANDOM PASSWORD
    PASSWORD=`date +%s | sha256sum | base64 | head -c 64 ; echo` &&\
    # CHANGE PASSWORD FOR USER ROOT
    echo "root:${PASSWORD}" | chpasswd &&\
    echo "  super secure root password set, and nobody knows it anymore..." &&\
    # GENERATE RANDOM PASSWORD
    PASSWORD=`date +%s | sha256sum | base64 | head -c 64 ; echo` &&\
    # ADD NEW USER
    useradd -m -p ${PASSWORD} -s /bin/bash ${USERNAME} &> /dev/null || true  &&\
    echo "  user ${USERNAME} added with super secure password, and nobody knows it anymore..." &&\
    # ADD USER TO DOCKER GROUP
    usermod -a -G docker ${USERNAME} &&\
    echo -e "security-config done!\n";
}

function get-src {
    echo -e "\nrunning get-src..." &&\
    # ADDING REPOSITORY KEY TO KNOWN HOSTS
    echo "  key scanning bit bucket..." &&\
    ssh-keyscan ${REPOSITORY_HOST} > ~/.ssh/known_hosts &&\
    echo "  copy known hosts to ${USERNAME}..." &&\
    mkdir -p /home/${USERNAME}/.ssh &&\
    cp ~/.ssh/known_hosts /home/${USERNAME}/.ssh/known_hosts &&\
    chown ${USERNAME}:${USERNAME} /home/${USERNAME}/.ssh/known_hosts &&\
    # CLONING REPOSITORY FROM BIT BUCKET
    if [ ! -d ${SRC_ROOT} ];
        then
            echo "  cloning repository..." &&\
            git clone ${REPOSITORY} ${SRC_ROOT};
        else
            echo "  repository already exits at ${SRC_ROOT}";
    fi
    # CHANGE OWNERSHIP
    echo "  change ownership to ${USERNAME} for ${SRC_ROOT}" &&\
    chown -R ${USERNAME}:${USERNAME} ${SRC_ROOT} &&\
    echo -e "get-src done!\n";
}

function startup-config {
    echo -e "\nrunning startup-config..." &&\
    echo "  copying configuration to /etc/rc.local..." &&\
    echo "${STARTUP_CONFIG}" > /etc/rc.local &&\
    echo -e "startup-config done!\n";
}

if [ $# -eq 0 ]
  then
    echo "deploying provision script...";
    local-exec;
    exit 0;
  else
    echo "running provision script...";
    remote-exec;
    exit 0;
fi
