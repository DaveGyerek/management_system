FROM python:3.7.2-alpine
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE config.dev.settings
ADD requirements /requirements
RUN apk add --update --no-cache --virtual build-deps gcc python3-dev musl-dev
RUN apk add --update --no-cache \
    postgresql-dev \
    jpeg-dev \
    zlib-dev \
    freetype-dev \
    lcms2-dev \
    openjpeg-dev \
    tiff-dev \
    tk-dev \
    tcl-dev \
    libffi-dev \
    py-cffi \
    gettext \
    postgresql-client \
    bash \
    wkhtmltopdf \
    libgcc libstdc++ libx11 glib libxrender libxext libintl \
    libressl2.7-libcrypto libssl1.1 \
    ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family

RUN apk add --no-cache --virtual .build-deps \
  build-base postgresql-dev libffi-dev \
    && pip install -r requirements/test.txt \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
                | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                | sort -u \
                | xargs -r apk info --installed \
                | sort -u \
    )" \
    && apk add --virtual .rundeps $runDeps \
    && apk del .build-deps
COPY . /code
WORKDIR /code
