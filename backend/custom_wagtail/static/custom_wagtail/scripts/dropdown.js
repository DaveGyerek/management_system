window.onclick = function(event) {
  if (!event.target.matches('.dropdown-btn')) {

    let dropdowns = document.getElementsByClassName("custom-dropdown");
    let i;
    for (i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('open')) {
        openDropdown.classList.remove('open');
      }
    }
  }
  else {
    event.target.closest('.custom-dropdown').classList.toggle("open");
  }
};