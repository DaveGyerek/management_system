from django.forms.utils import flatatt
from django.template.loader import get_template
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from wagtail.contrib.modeladmin.templatetags.modeladmin_tags import result_list, register


@register.inclusion_tag("custom_wagtail/table_list.html", takes_context=True)
def custom_result_list(context):
    return result_list(context)


@register.inclusion_tag("custom_wagtail/table_row.html", takes_context=True)
def custom_result_row_display(context, index):
    obj = context['object_list'][index]
    view = context['view']
    row_attrs_dict = view.model_admin.get_extra_attrs_for_row(obj, context)
    row_attrs_dict['data-object-pk'] = obj.pk
    odd_or_even = 'odd' if (index % 2 == 0) else 'even'
    if 'class' in row_attrs_dict:
        row_attrs_dict['class'] += ' %s' % odd_or_even
    else:
        row_attrs_dict['class'] = odd_or_even
    row_as_link = context['row_as_link']
    if row_as_link:
        row_attrs_dict['class'] += ' clickable-row'

    parent_pk = None
    if hasattr(view, 'parent_pk'):
        parent_pk = view.parent_pk

    context.update({
        'row_as_link': row_as_link,
        'parent_pk': parent_pk,
        'obj': obj,
        'row_attrs': mark_safe(flatatt(row_attrs_dict)),
        'action_buttons': view.get_buttons_for_obj(obj),
    })
    return context


@register.inclusion_tag("custom_wagtail/table_row_actions.html", takes_context=True)
def custom_result_row_actions_display(context):
    return context


@register.simple_tag
def custom_admin_list_filter(view, spec):
    template_name = 'custom_wagtail/table_filter.html'
    template = get_template(template_name)
    return template.render({
        'title': spec.title,
        'choices': list(spec.choices(view)),
        'spec': spec,
    })


@register.simple_tag
def custom_pagination_link_previous(current_page, view):
    if current_page.has_previous():
        previous_page_number0 = current_page.previous_page_number() - 1
        return format_html(
            '<li class="prev"><a href="%s">'
            '<i class="icon icon-fa-angle-left"></i></a></li>' %
            (view.get_query_string({view.PAGE_VAR: previous_page_number0}))
        )
    return format_html(
        '<li class="prev"><a class="disabled">'
        '<i class="icon icon-fa-angle-left"></i></a></li>'
    )


@register.simple_tag
def custom_pagination_link_next(current_page, view):
    if current_page.has_next():
        next_page_number0 = current_page.next_page_number() - 1
        return format_html(
            '<li class="next"><a href="%s">'
            '<i class="icon icon-fa-angle-right"></i></a></li>' %
            (view.get_query_string({view.PAGE_VAR: next_page_number0}))
        )
    return format_html(
        '<li class="next"><a class="disabled">'
        '<i class="icon icon-fa-angle-right"></i></a></li>'
    )


@register.simple_tag
def custom_pagination_link_first(current_page, view):
    if current_page.has_previous():
        return format_html(
            '<li class="first"><a href="%s">'
            '<i class="icon icon-fa-angle-double-left"></i></a></li>' %
            (view.get_query_string({view.PAGE_VAR: 0}))
        )
    return format_html(
        '<li class="first"><a class="disabled">'
        '<i class="icon icon-fa-angle-double-left"></i></a></li>'
    )


@register.simple_tag
def custom_pagination_link_last(current_page, view):
    if current_page.has_next():
        return format_html(
            '<li class="first"><a href="%s">'
            '<i class="icon icon-fa-angle-double-right"></i></a></li>' %
            (view.get_query_string({view.PAGE_VAR: current_page.paginator.num_pages - 1}))
        )
    return format_html(
        '<li class="first"><a class="disabled">'
        '<i class="icon icon-fa-angle-double-right"></i></a></li>'
    )
