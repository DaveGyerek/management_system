from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from wagtail.contrib.modeladmin.views import (
    WMABaseView,
    IndexView as WagtailIndexView,
    CreateView as WagtailCreateView,
    EditView as WagtailEditView,
    DeleteView as WagtailDeleteView,
)

_conf = getattr(settings, 'CUSTOM_WAGTAIL')

BASE_TEMPLATE = _conf.get('base-template') or 'custom_wagtail/_base.html'
INDEX_TEMPLATE = _conf.get('index-template') or 'custom_wagtail/table.html'
CREATE_TEMPLATE = _conf.get('create-template') or 'custom_wagtail/create.html'
EDIT_TEMPLATE = _conf.get('edit-template') or 'custom_wagtail/edit.html'
DELETE_TEMPLATE = _conf.get('delete-template') or 'custom_wagtail/delete.html'


class BaseTemplateContextMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'BASE_TEMPLATE_NAME': BASE_TEMPLATE,
        })
        return context


class IndexView(BaseTemplateContextMixin, WagtailIndexView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Add new attribute to modelAdmin
        context['row_as_link'] = self.model_admin.row_as_link
        return context

    def get_template_names(self):
        return [
            INDEX_TEMPLATE
        ]

    def get_buttons_for_obj(self, obj):
        return self.button_helper.get_buttons_for_obj(
            obj, classnames_add=['button-small'])

    def get_url_for_obj(self, obj):
        url_name = self.url_helper.get_action_url_name('edit')
        return reverse(url_name, kwargs={
            'instance_pk': obj.pk
        })


class CreateView(BaseTemplateContextMixin, WagtailCreateView):
    def get_template_names(self):
        return [
            CREATE_TEMPLATE
        ]


class EditView(BaseTemplateContextMixin, WagtailEditView):
    def get_template_names(self):
        return [
            EDIT_TEMPLATE
        ]


class DeleteView(BaseTemplateContextMixin, WagtailDeleteView):
    def get_template_names(self):
        return [
            DELETE_TEMPLATE
        ]


class CustomView(BaseTemplateContextMixin, LoginRequiredMixin, WMABaseView):
    template_name = None
    menu_icon = None

    # noinspection PyMissingConstructor
    def __init__(self, ):
        pass

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_superuser:
            return self.handle_no_permission()
        return TemplateView.dispatch(self, request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
