from wagtail.contrib.modeladmin.helpers import AdminURLHelper


class CustomAdminURLHelper(AdminURLHelper):
    """
    Helps to remove the `app` name from the start of the URLs
    """

    def _get_action_url_pattern(self, action):
        if action == 'index':
            return r'^%s/$' % self.opts.model_name
        return r'^%s/%s/$' % (self.opts.model_name,
                              action)

    def _get_object_specific_action_url_pattern(self, action):
        return r'^%s/%s/(?P<instance_pk>[-\w]+)/$' % (
            self.opts.model_name, action)
