from wagtail.contrib.modeladmin.options import (
    ModelAdmin as WagtailModelAdmin,
    ModelAdminGroup as WagtailModelAdminGroup,
)
from .helpers import CustomAdminURLHelper
from . import views


class ModelAdmin(WagtailModelAdmin):
    url_helper_class = CustomAdminURLHelper

    index_template_name = 'custom_wagtail/table.html'

    index_view_class = views.IndexView
    create_view_class = views.CreateView
    edit_view_class = views.EditView
    delete_view_class = views.DeleteView

    row_as_link = None


class ModelAdminGroup(WagtailModelAdminGroup):
    def get_menu_icon(self):
        return self.menu_icon
