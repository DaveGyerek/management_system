from datetime import datetime

from django.test import TestCase

from backend.app.features.material.models import Material, MaterialCategory
from backend.app.features.ordered_product.models import OrderedProduct
from backend.app.features.ordered_product.views.save import save_ordered_product
from ..features.delivery_note.models import DeliveryNote
from ..features.manufacture_plan.models import ManufacturePlan
from ..features.client.models import Client
from ..features.order.models import Order, ProductOrder, ProductOrderPart
from ..features.product.models import ProductType, Product, ProductLabel, ProductPart
from ..features.ordered_product.use_cases.add_to_manufacture_plan import AddOrderedProductsToManufacturePlan
from ..features.ordered_product.use_cases.send_to_delivery_note import SentOrderedProductsAndLabelsToDelivery

_add_to_manufacture_plan_data = {
    "id": 1,
    "product_orders":
        [
            {
                "id": 1,
                "quantity": 2
            },
            {
                "id": 2,
                "quantity": 3
            }
        ]
}
_send_to_delivery_note_data = {
    "ordered_products": [1, 2, 3, 4, 5],
    "labels": [],
    "delivery_id": 1
}


def _create_products_and_client():
    product_type = ProductType.objects.create(name='asdf')
    product1 = Product.objects.create(name='product1', price=1000, product_type=product_type)
    product2 = Product.objects.create(name='product2', price=2000, product_type=product_type)
    ProductLabel.objects.create(name='product1_label1', product=product1)
    ProductLabel.objects.create(name='product1_label2', product=product1)
    client = Client.objects.create(name='asdf', email='asdf@asdf.asdf', billing_address='asdf')

    return product1, product2, client


def _create_order():
    product1, product2, client = _create_products_and_client()
    order = Order.objects.create(client=client, billing_address="asd", delivery_address="asd")
    ProductOrder.objects.create(order=order, product=product1, quantity=5, price=1000)
    ProductOrder.objects.create(order=order, product=product2, quantity=5, price=2000)


def _create_order_with_ready_made_product():
    mc = MaterialCategory.objects.create(name='mc')
    product1, product2, client = _create_products_and_client()
    product1.ready_made = True
    product1.save()
    material = Material.objects.create(category=mc, name="mat", ready_made=True)
    ProductPart.objects.create(product=product1, name='part', material=material)

    order = Order.objects.create(client=client, billing_address="asd", delivery_address="asd")
    po = ProductOrder.objects.create(order=order, product=product1, quantity=5, price=1000)
    ProductOrder.objects.create(order=order, product=product2, quantity=5, price=2000)

    ProductOrderPart.objects.create(product_order=po, name='part', material=material, price=2000)
    for ordered_product in OrderedProduct.objects.all():
        save_ordered_product(ordered_product, is_new=True)


def _execute_add_to_manufacture_plan(plan_id = None):
    if plan_id:
        data = {
            **_add_to_manufacture_plan_data,
            'id': plan_id
        }
    else:
        data = _add_to_manufacture_plan_data
    request = AddOrderedProductsToManufacturePlan.Request(data=data)
    use_case = AddOrderedProductsToManufacturePlan(request)
    return use_case.execute()


def _execute_send_to_delivery_note(data=None):
    request = SentOrderedProductsAndLabelsToDelivery.Request(data=data or _send_to_delivery_note_data)
    use_case = SentOrderedProductsAndLabelsToDelivery(request)
    return use_case.execute()


def _add_to_manufacture_plan():
    plan = ManufacturePlan.objects.create(identification_number='asd', date=datetime.now())
    _execute_add_to_manufacture_plan(plan.id)


def _add_to_delivery_note():
    DeliveryNote.objects.create(client=Client.objects.first(),
                                voucher_number="Random123")
    _execute_send_to_delivery_note()


class BaseTestWithOrder(TestCase):
    def setUp(self):
        _create_order()


class BaseTestOrderAddedManufacturePlan(BaseTestWithOrder):
    def setUp(self):
        super().setUp()
        _add_to_manufacture_plan()


class BaseTestOrderAddedToDeliveryNote(BaseTestWithOrder):
    def setUp(self):
        super().setUp()
        _add_to_delivery_note()


class BaseTestWithOrderReadyMade(TestCase):
    def setUp(self):
        _create_order_with_ready_made_product()
