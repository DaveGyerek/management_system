$(document).ready(function () {
    $('#select-all').click(function () {
        let state = this.checked;
        $(':checkbox').each(function() {
            this.checked = state;
        });
    })
});