$(document).ready(function () {
    const priceInput = $("#id_price");
    const netPriceInput = $("#id_net_price");
    const vatInput = $("#id_vat");

    priceInput.change(() => {
        let net_price = (priceInput.val() / (vatInput.val() / 100 + 1 )).toFixed(2);
        netPriceInput.val(net_price);
    });

    netPriceInput.change(() => {
        let price = (netPriceInput.val() * (vatInput.val() / 100 + 1)).toFixed(2);
        priceInput.val(price);
    });

    vatInput.change(() => {
        let price = (netPriceInput.val() * (vatInput.val() / 100 + 1 )).toFixed(2);
        priceInput.val(price);
    });
});
