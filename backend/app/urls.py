from django.conf.urls import url, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='ManagementSystem API')

urlpatterns = [
    url(r'^', include('backend.app.features.user.urls')),
    url(r'^', include('backend.app.features.dashboard.urls')),
    url(r'^', include('backend.app.features.client.urls')),
    url(r'^', include('backend.app.features.order.urls')),
    url(r'^', include('backend.app.features.manufacture_plan.urls')),
    url(r'^', include('backend.app.features.delivery_note.urls')),
    url(r'^', include('backend.app.features.ordered_product.urls')),
    url(r'^', include('backend.app.features.label.urls')),
]
