from django.contrib.admin.templatetags.admin_static import static


class SetOrderedProductsFinishedMixin:
    MANUFACT_DEV_URL = 'http://127.0.0.1:3000/bundle.js'
    # uncomment in dev
    # _manufact_details_js = MANUFACT_DEV_URL
    _manufact_details_js = static('app/scripts/manufacture_plan/details.min.js')
    _manufact_details_css = None if _manufact_details_js == MANUFACT_DEV_URL else static(
        'app/style/manufacture_plan/details.min.css')

    manufact_context = {
        'manufacture_details_js': _manufact_details_js,
        'manufacture_details_css': _manufact_details_css,
    }


class SendToManufactMixin:
    ORDER_DETAILS_DEV_URL = 'http://127.0.0.1:3000/bundle.js'
    # _order_details_js = ORDER_DETAILS_DEV_URL
    _order_details_js = static('app/scripts/order/details.min.js')
    _order_details_css = None if _order_details_js == ORDER_DETAILS_DEV_URL else static(
        'app/style/order/details.min.css')

    order_details_context = {
        'order_details_js': _order_details_js,
        'order_details_css': _order_details_css,
    }

class SendToAnotherDeliveryNoteMixin:
    DELIVERY_DETAILS_DEV_URL = 'http://127.0.0.1:3000/bundle.js'
    # _delivery_note_details_js = DELIVERY_DETAILS_DEV_URL
    _delivery_note_details_js = static('app/scripts/delivery_note/details.min.js')
    _delivery_note_details_css = None if _delivery_note_details_js == DELIVERY_DETAILS_DEV_URL else static(
        'app/style/delivery_note/details.min.css')

    delivery_details_context = {
        'delivery_note_details_js': _delivery_note_details_js,
        'delivery_note_details_css': _delivery_note_details_css,
    }
