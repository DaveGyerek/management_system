from django.http import HttpResponse
from django.views.generic.base import View


class DownloadView(View):
    file_path = None
    file_name = None
    content_type = 'application/pdf'

    def get_file(self, *args, **kwargs):
        assert self.file_path
        return open(self.file_path, 'rb')

    def get_file_name(self, *args, **kwargs):
        assert self.file_name
        return self.file_name

    def get_content_type(self, *args, **kwargs):
        return self.content_type

    def get_content_disposition(self, *args, **kwargs):
        return 'attachment; filename="{}"'.format(
            self.get_file_name(*args, **kwargs)
        )

    def cleanup(self, *args, **kwargs):
        pass

    def get(self, request, *args, **kwargs):
        with self.get_file(*args, **kwargs) as content:
            response = HttpResponse(content=content)
            response['Content-Type'] = self.get_content_type(*args, **kwargs)
            content_disposition = self.get_content_disposition()
            if content_disposition:
                response['Content-Disposition'] = content_disposition
        self.cleanup(*args, **kwargs)
        return response
