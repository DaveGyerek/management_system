from wagtail.contrib.modeladmin.helpers import AdminURLHelper


class UrlHelper(AdminURLHelper):
    def __init__(self, model, url_name):
        self.url_name = url_name
        super().__init__(model)

    def __call__(self, *args, **kwargs):
        return self

    def _get_action_url_pattern(self, action):
        if action == 'index':
            return r'^%s/%s/$' % (self.opts.app_label, self.url_name)
        return r'^%s/%s/%s/$' % (self.opts.app_label, self.url_name, action)

    def _get_object_specific_action_url_pattern(self, action):
        return r'^%s/%s/%s/(?P<instance_pk>[-\w]+)/$' % (
            self.opts.app_label, self.url_name, action)

    def get_action_url_name(self, action):
        return '%s_%s_modeladmin_%s' % (
            self.opts.app_label, self.url_name, action)
