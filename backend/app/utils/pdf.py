import pdfkit

from django.template.loader import get_template


class RenderPdf:
    class Request:
        def __init__(self, output_path: str, template_name: str, context: dict, landscape: bool = False, options: dict = None):
            self.output_path = output_path
            self.template_name = template_name
            self.context = context
            self.landscape = landscape
            self.options = options

    pdf_renderer = pdfkit

    # https://wkhtmltopdf.org/usage/wkhtmltopdf.txt
    pdf_options = {
        'margin-left': "5mm",
        'margin-right': "5mm",
        'margin-bottom': "20mm",
        # 'footer-center': 'Page [topage]/[page]',
        # 'footer-spacing': '5',
        # 'footer-font-size': '10'
    }

    def execute(self, request: Request):
        if request.options:
            self.pdf_options = request.options
        if request.landscape:
            self.pdf_options['orientation'] = 'landscape'
        template = get_template(request.template_name)
        rendered_template = template.render(request.context)
        self.pdf_renderer.from_string(
            rendered_template,
            request.output_path,
            options=self.pdf_options
        )
