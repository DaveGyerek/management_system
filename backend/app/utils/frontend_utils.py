import requests
from django.conf import settings


def get_resource(dev_url, static_url):
    if not settings.DEV:
        return static_url
    try:
        response = requests.get(dev_url)
        if response.status_code == requests.codes.ok:
            return dev_url
    except requests.ConnectionError:
        pass
    return static_url
