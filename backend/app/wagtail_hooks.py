from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.html import format_html
from wagtail.core import hooks


# noinspection PyUnresolvedReferences
from .features.order.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.material.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.product.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.delivery_note.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.manufacture_plan.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.ordered_product.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.client.wagtail_hook import *
# noinspection PyUnresolvedReferences
from .features.user.wagtail_hook import *

def _css(file):
    return format_html('<link rel="stylesheet" type="text/css" href="{}">'.format(static(file)))


def _js(file):
    return format_html('<script src="{}">'.format(static(file)))


@hooks.register('insert_global_admin_css')
def custom_css():
    return ''.join([
        _css('custom_wagtail/style/wagtail-override.css'),
    ])


@hooks.register('insert_global_admin_js')
def custom_js():
    return ''.join([
    ])


# hide from menu what we do not need
@hooks.register('construct_main_menu')
def hide_menu_items(request, menu_items):
    hidden_items = ['explorer', 'images', 'documents', 'settings']
    menu_items[:] = [item for item in menu_items if item.name not in hidden_items]
