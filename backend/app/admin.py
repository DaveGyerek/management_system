from django.contrib import admin

from .features.user.admin import *
from .features.material.admin import *
from .features.product.admin import *
from .features.client.admin import *
from .features.order.admin import *
from .features.ordered_product.admin import *
from .features.transaction.admin import *
from .features.manufacture_plan.admin import *
from .features.delivery_note.admin import *
