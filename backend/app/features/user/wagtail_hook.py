from django.utils.translation import ugettext as _
from wagtail.contrib.modeladmin.options import modeladmin_register
from wagtail.admin.edit_handlers import MultiFieldPanel, FieldPanel

from backend.custom_wagtail.modeladmin import ModelAdmin
from .models import User

User.panels = [
    MultiFieldPanel((
        FieldPanel('first_name'),
        FieldPanel('last_name'),
        FieldPanel('username'),
        FieldPanel('email'),
    ), _('Personal information'))
]


class Admin(ModelAdmin):
    model = User
    menu_icon = 'fa-users'
    list_display = (
        'first_name',
        'last_name',
        'username',
        'email',
    )
    search_fields = (
        'first_name',
        'last_name',
        'email',
        'username',
    )

    row_as_link = 'app_user_modeladmin_edit'


modeladmin_register(Admin)
