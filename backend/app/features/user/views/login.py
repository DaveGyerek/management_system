from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.views import LoginView
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from wagtail.admin import forms

from ..models import User


class CustomLoginView(LoginView):
    template_name = 'custom_wagtail/login.html'
    redirect_field_name = REDIRECT_FIELD_NAME
    form_class = forms.LoginForm
    extra_context = {
        'show_password_reset': True,
        'username_field': User.USERNAME_FIELD,
    }
    redirect_authenticated_user = False

    def form_valid(self, form):
        result = super().form_valid(form)
        return result

    def get_redirect_url(self):
        # TODO should refactor this
        return f'/{self.request.GET.get(self.redirect_field_name, "")}'


@sensitive_post_parameters()
@never_cache
def login_view(request):
    if request.user.is_authenticated and request.user.has_perm('wagtailadmin.access_admin'):
        return redirect('wagtailadmin_home')
    else:
        return CustomLoginView.as_view()(request)
