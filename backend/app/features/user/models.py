from datetime import datetime

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext as _


class User(AbstractUser):
    """
    Represents the users who can log into the app
    and manage things.
    """

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        ordering = ('first_name', 'last_name')

    email = models.EmailField(
        unique=True,
        verbose_name=_('Email'),
    )
    timestamp = models.DateTimeField(
        default=datetime.now,
        verbose_name=_('Created at'),
    )
    activated = models.BooleanField(
        default=False,
        verbose_name=_('Activated'),
    )

    def __str__(self):
        return self.username
