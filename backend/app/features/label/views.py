import os
from easy_pdf.views import PDFTemplateResponseMixin

from django.conf import settings
from django.views.generic import TemplateView

from ...utils.views import DownloadView
from .use_cases.render_label import RenderLabel
from .use_cases.label_context import LabelContext, LabelContextForOrderedProductsAndLabels


# class LabelPdfView(PDFTemplateResponseMixin, TemplateView):
#     def get_template_names(self):
#         return [
#             'app/label/label_easy_pdf.html',
#         ]
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context_use_case = LabelContext()
#         context_request = LabelContext.Request(
#             manufacture_plan_id=kwargs['manufacture_plan_id'],
#         )
#         context_response = context_use_case.execute(context_request)
#         context.update(**context_response.context)
#         return context


class LabelPdfView(DownloadView, TemplateView):
    output_path = os.path.join(str(settings.APPS_DIR), 'tmp.pdf')

    def get_file_name(self, *args, **kwargs):
        identifier = '{}/{}'.format(kwargs['ordered_product_id'], kwargs['label_id']) \
            if kwargs['label_id'] else '{}'.format(kwargs['ordered_product_id'])
        return '{}.pdf'.format(identifier)

    def get_content_disposition(self, *args, **kwargs):
        return None

    def get_file(self, *args, **kwargs):
        context_use_case = LabelContext()
        context_request = LabelContext.Request(
            manufacture_plan_id=kwargs['manufacture_plan_id'],
        )
        context_response = context_use_case.execute(context_request)
        context = context_response.context
        pdf_use_case = RenderLabel()
        pdf_request = RenderLabel.Request(
            output_path=self.output_path,
            context=context,
        )
        pdf_use_case.execute(pdf_request)
        return open(self.output_path, 'rb')


# class ApiLabelPdfView(PDFTemplateResponseMixin, TemplateView):
#     def get_template_names(self):
#         return [
#             'app/label/label_easy_pdf.html',
#         ]
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         try:
#             ordered_products = self.request.GET['ordered_products']
#         except KeyError:
#             ordered_products = None
#         try:
#             labels = self.request.GET['labels']
#         except KeyError:
#             labels = None
#
#         context_request = LabelContextForOrderedProductsAndLabels.Request(
#             ordered_products, labels
#         )
#         context_use_case = LabelContextForOrderedProductsAndLabels(context_request)
#         context_response = context_use_case.execute()
#         context.update(**context_response.context)
#
#         return context

class ApiLabelPdfView(LabelPdfView):
    def get_file(self, *args, **kwargs):
        try:
            ordered_products = self.request.GET['ordered_products']
        except KeyError:
            ordered_products = None
        try:
            labels = self.request.GET['labels']
        except KeyError:
            labels = None

        context_request = LabelContextForOrderedProductsAndLabels.Request(
            ordered_products, labels
        )
        context_use_case = LabelContextForOrderedProductsAndLabels(context_request)
        context_response = context_use_case.execute()
        context = context_response.context
        pdf_use_case = RenderLabel()
        pdf_request = RenderLabel.Request(
            output_path=self.output_path,
            context=context,
        )
        pdf_use_case.execute(pdf_request)
        return open(self.output_path, 'rb')
