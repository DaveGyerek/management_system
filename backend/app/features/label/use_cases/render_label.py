import pdfkit
from django.template.loader import get_template


class RenderLabel:
    class Request:
        def __init__(self, output_path: str, context: dict):
            self.output_path = output_path
            self.context = context

    template_name = 'app/label/label.html'
    pdf_renderer = pdfkit
    # https://wkhtmltopdf.org/usage/wkhtmltopdf.txt
    pdf_options = {
        'dpi': '200',
        'page-width': '30cm',
        'page-height': '20cm',
        'margin-top': '0in',
        'margin-right': '0in',
        'margin-bottom': '0in',
        'margin-left': '0in',
    }

    def execute(self, request: Request):
        template = get_template(self.template_name)
        rendered_template = template.render(request.context)
        self.pdf_renderer.from_string(
            rendered_template,
            request.output_path,
            options=self.pdf_options,
        )
