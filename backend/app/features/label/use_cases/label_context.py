from ..utils.create_data import get_labels_data_for_ordered_products, get_labels_data_for_ordered_product_labels
from ...ordered_product.repository import OrderedProductRepository


class LabelContext:
    class Request:
        def __init__(self, manufacture_plan_id: int,
                     repository: OrderedProductRepository = None):
            self.manufacture_plan_id = manufacture_plan_id
            self.repository = repository or OrderedProductRepository()

    class Response:
        def __init__(self, context: dict):
            self.context = context

    def execute(self, request: Request) -> Response:
        ordered_products = request.repository.get_ordered_products_for_manufacture_plan(request.manufacture_plan_id)

        data = get_labels_data_for_ordered_products(ordered_products, request.repository)

        return self.Response(
            context={
                'labels': data
            }
        )


class LabelContextForOrderedProductsAndLabels:
    class Request:
        def __init__(self, ordered_product_ids, label_ids):
            self.ordered_product_ids = ordered_product_ids.split(',') if ordered_product_ids else []
            self.label_ids = label_ids.split(',') if label_ids else []

    def __init__(self, request: Request,
                 repository: OrderedProductRepository = None):
        self.request = request
        self.repository = repository or OrderedProductRepository()

        self.ordered_product_ids = request.ordered_product_ids
        self.label_ids = request.label_ids
        # Delete the unnecessary ids
        if self.ordered_product_ids:
            labels = self.repository.get_all_labels_for_ordered_product_ids(self.ordered_product_ids) \
                .values_list('id', flat=True)
            self.label_ids = [item for item in self.label_ids if int(item) not in labels]

    class Response:
        def __init__(self, context: dict):
            self.context = context

    def execute(self):
        ordered_products = self.repository.get_ordered_products(
            self.ordered_product_ids)
        ordered_product_labels = self.repository.get_ordered_product_labels_by_id(self.label_ids) \
            .only('id', 'name').prefetch_related('ordered_product__product_order',
                                                 'ordered_product__product_order__order')
        data = []
        # DATA FOR ORDERED PRODUCTS
        if ordered_products:
            data += get_labels_data_for_ordered_products(ordered_products, self.repository)
        # DATA FOR LABELS
        if ordered_product_labels:
            data += get_labels_data_for_ordered_product_labels(ordered_product_labels)

        return self.Response(
            context={
                'labels': data
            }
        )
