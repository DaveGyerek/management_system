import qrcode
import base64
from io import BytesIO


class GetQr:
    class Request:
        def __init__(self, identifier: str, output: str = None):
            """
            :param identifier: In the format of 1234 or 1234/567
            :param output: Optional, if the image should be saved to file
            """
            self.identifier = identifier
            self.output = output

    class Response:
        def __init__(self, base64_qr_content):
            self.base64_qr_content = base64_qr_content

    qr_engine = qrcode

    def execute(self, request: Request) -> Response:
        content = request.identifier
        image = self.qr_engine.make(content)
        if request.output:
            with open(request.output, 'wb') as outfile:
                image.save(outfile, format='JPEG')

        with BytesIO() as buffered:
            image.save(buffered, format='JPEG')
            buffer_value = buffered.getvalue()
            image_str = base64.b64encode(buffer_value)

        print(str(image_str, 'utf-8'))
        return self.Response(
            base64_qr_content=str(image_str, 'utf-8')
        )


def shortcut(identifier):
    use_case = GetQr()
    request = GetQr.Request(identifier)
    response = use_case.execute(request)
    return response.base64_qr_content
