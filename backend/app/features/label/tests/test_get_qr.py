import os
from ..use_cases.get_qr import GetQr


def test_get_qr():
    use_case = GetQr()
    request = GetQr.Request('1234')
    response = use_case.execute(request)
    assert response.base64_qr_content


def test_get_qr_keep_image():
    use_case = GetQr()
    output = 'test-qr.jpeg'
    request = GetQr.Request('1234/567', output)
    response = use_case.execute(request)
    assert response.base64_qr_content
    assert os.path.isfile(output)
    os.remove(output)
