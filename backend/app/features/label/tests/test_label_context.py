import pytest

from backend.app.tests.app_test import BaseTestOrderAddedManufacturePlan
from ..use_cases.label_context import LabelContext, LabelContextForOrderedProductsAndLabels


def get_qr_str_mock(*args):
    return 'a'


class TestLabelContext(BaseTestOrderAddedManufacturePlan):
    def test_label_context(self):
        use_case = LabelContext()
        use_case.get_qr_str = get_qr_str_mock
        request = LabelContext.Request(1)
        response = use_case.execute(request)
        assert response.context
        assert response.context['labels']
        label = response.context['labels'][0]
        assert label['product_name']
        assert label['product_part_name']
        assert label['order_date']
        assert label['client_name']
        assert label['client_store_name']
        assert label['identifier']
        assert label['qr_content']
        assert label['site_url']

    def test_label_context_with_ids(self):
        request = LabelContextForOrderedProductsAndLabels.Request(ordered_product_ids='1',
                                                                  label_ids='1')
        use_case = LabelContextForOrderedProductsAndLabels(request)
        use_case.get_qr_str = get_qr_str_mock
        response = use_case.execute()
        assert response.context
        assert response.context['labels']
        label = response.context['labels'][0]
        assert label['product_name']
        assert label['product_part_name']
        assert label['order_date']
        assert label['client_name']
        assert label['client_store_name']
        assert label['identifier']
        assert label['qr_content']
        assert label['site_url']

    def test_label_context_just_product(self):
        request = LabelContextForOrderedProductsAndLabels.Request(ordered_product_ids='1', label_ids=None)
        use_case = LabelContextForOrderedProductsAndLabels(request)
        use_case.get_qr_str = get_qr_str_mock
        response = use_case.execute()
        assert response.context
        assert response.context['labels']
        label = response.context['labels'][0]
        assert label['product_name']
        assert label['product_part_name']
        assert label['order_date']
        assert label['client_name']
        assert label['client_store_name']
        assert label['identifier']
        assert label['qr_content']
        assert label['site_url']

    def test_label_context_just_label(self):
        request = LabelContextForOrderedProductsAndLabels.Request(ordered_product_ids=None, label_ids='1')
        use_case = LabelContextForOrderedProductsAndLabels(request)
        use_case.get_qr_str = get_qr_str_mock
        response = use_case.execute()
        assert response.context
        assert response.context['labels']
        label = response.context['labels'][0]
        assert label['product_name']
        assert label['product_part_name']
        assert label['order_date']
        assert label['client_name']
        assert label['client_store_name']
        assert label['identifier']
        assert label['qr_content']
        assert label['site_url']

    def test_label_context_multiple_ids(self):
        request = LabelContextForOrderedProductsAndLabels.Request(ordered_product_ids='1,2,3', label_ids='1,2')
        use_case = LabelContextForOrderedProductsAndLabels(request)
        use_case.get_qr_str = get_qr_str_mock
        response = use_case.execute()
        assert response.context
        assert response.context['labels']
        label = response.context['labels'][0]
        assert label['product_name']
        assert label['product_part_name']
        assert label['order_date']
        assert label['client_name']
        assert label['client_store_name']
        assert label['identifier']
        assert label['qr_content']
        assert label['site_url']
