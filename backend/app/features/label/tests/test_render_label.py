import os
from ..use_cases.render_label import RenderLabel


def test_render_label():
    output = 'label.pdf'
    use_case = RenderLabel()
    request = RenderLabel.Request(output, {})
    use_case.execute(request)
    assert os.path.isfile(output)
    os.remove(output)
