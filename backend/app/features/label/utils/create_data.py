from ...ordered_product.repository import OrderedProductRepository
from ..use_cases.get_qr import \
    shortcut  # use this shortcut to get the base64 qr code which should go to template context


def get_labels_data_for_ordered_products(ordered_products, repository: OrderedProductRepository):
    data = []
    ordered_product_labels = repository.get_all_labels_for_ordered_products(ordered_products)

    for ordered_product in ordered_products.iterator():
        order = ordered_product.product_order.order

        labels = ordered_product_labels.filter(ordered_product=ordered_product).values('id', 'name')

        if not labels.exists():
            data.append({
                'product_name': ordered_product.product_order.product.name,
                'product_part_name': '-',
                'order_date': order.delivery_timestamp,
                'client_name': order.client.name,
                'client_store_name': '{} ({})'.format(order.store.name if order.store else '-',
                                                      order.store.delivery_address if order.store else '-'),
                'identifier': ordered_product.id,
                'qr_content': shortcut(ordered_product.id),
                'site_url': 'www.examplestore.com',
            })

        for label in labels:
            data.append({
                'product_name': ordered_product.product_order.product.name,
                'product_part_name': label['name'],
                'order_date': ordered_product.product_order.custom_client_po or order.delivery_timestamp,
                'client_name': order.client.name,
                'client_store_name': '{} ({})'.format(order.store.name if order.store else '-',
                                                      order.store.delivery_address if order.store else '-'),
                'identifier': '{0}/{1}'.format(ordered_product.id, label['id']),
                'qr_content': shortcut('{0}/{1}'.format(ordered_product.id, label['id'])),
                'site_url': 'www.examplestore.com',
            })

    return data


def get_labels_data_for_ordered_product_labels(ordered_product_labels):
    data = []

    for label in ordered_product_labels:
        order = label.ordered_product.product_order.order
        ordered_product = label.ordered_product
        data.append({
            'product_name': ordered_product.product_order.product.name,
            'product_part_name': label.name,
            'order_date': order.delivery_timestamp,
            'client_name': order.client.name,
            'client_store_name': '{} ({})'.format(order.store.name if order.store else '-',
                                                  order.store.delivery_address if order.store else '-'),
            'identifier': '{0}/{1}'.format(ordered_product.id, label.id),
            'qr_content': shortcut('{0}/{1}'.format(ordered_product.id, label.id)),
            'site_url': 'www.examplestore.com',
        })

    return data
