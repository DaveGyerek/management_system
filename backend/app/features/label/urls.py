from django.conf.urls import url

from .views import LabelPdfView, ApiLabelPdfView

urlpatterns = [
    url(r'^label/(?P<manufacture_plan_id>\d+)$',
        LabelPdfView.as_view(), name='label-view'),

    url(r'^api/1/generate-label/',
        ApiLabelPdfView.as_view(), name='api-label-view'),
]
