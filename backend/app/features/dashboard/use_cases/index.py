import json

from django.utils import timezone

from ...product.models import ProductType
from ...ordered_product.models import OrderedProduct


class DashboardContext:
    def get_last_days_data(self):
        data = []
        now = timezone.now()

        for i in range(0, 10):
            date = now - timezone.timedelta(days=i)
            op_count = OrderedProduct.objects.filter(
                product_order__order__order_timestamp__date=date
            ).count()

            data.append({
                'date': date.strftime('%m.%d'),
                'count': op_count,
            })

        return data

    def get_last_months_data(self):
        data = []
        now = timezone.now()

        for i in range(0, 10):
            year = now.year if now.month - i > 0 else now.year - 1
            month = now.month - i if (now.month - i) > 0 else 12 + (now.month - i)
            date = timezone.datetime(year, month, 1)
            op_count = OrderedProduct.objects.filter(
                product_order__order__order_timestamp__date__year=year,
                product_order__order__order_timestamp__date__month=month,
            ).count()

            data.append({
                'date': date.strftime('%B'),
                'count': op_count,
            })

        return data

    def get_product_types_data(self):
        data = []

        product_types = ProductType.objects.all()
        ordered_products = OrderedProduct.objects.all()
        for product_type in product_types:
            data.append({
                'name': product_type.name,
                'count': ordered_products.filter(
                    product_order__product__product_type=product_type
                ).count()
            })

        return data

    def execute(self):
        self.get_last_days_data()
        return {
            'data_json': json.dumps({
                'last_days': self.get_last_days_data(),
                'last_months': self.get_last_months_data(),
                'product_types': self.get_product_types_data(),
            })
        }
