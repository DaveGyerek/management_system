from django.templatetags.static import static
from django.views.generic import TemplateView

from ..use_cases.index import DashboardContext
from backend.app.features.user.permissions import ActivatedUserMixin


class DashboardView(ActivatedUserMixin, TemplateView):
    template_name = 'app/dashboard/index.html'

    def get_js(self):
        return static('app/scripts/dashboard/index.min.js')

    def get_context_data(self, **kwargs):
        context = DashboardContext().execute()

        return {
            **super().get_context_data(**kwargs),
            **context,
        }