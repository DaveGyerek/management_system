from django.utils.translation import ugettext as _
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, FieldRowPanel
from django.templatetags.static import static
from wagtail.contrib.modeladmin.options import modeladmin_register, ModelAdminGroup

from ..ordered_product.wagtail_hook import FinishedProductAdmin
from .models import MaterialCategory, Material
from ..transaction.wagtail_hook import TransactionAdmin
from backend.custom_wagtail.modeladmin import ModelAdmin

MaterialCategory.panels = [
    MultiFieldPanel((
        FieldPanel('name'),
    ), _('Category')),
]

Material.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('name', classname='col6'),
            FieldPanel('category', classname='col6'),
            FieldPanel('unit', classname='col6'),
            FieldPanel('vat', classname='col6'),
            FieldPanel('price', classname='col6'),
            FieldPanel('net_price', classname='col6'),
        )),
    ), _('Material'))
]


class MaterialAdmin(ModelAdmin):
    model = Material
    menu_icon = 'fa-cubes'
    menu_label = _('Materials')

    list_display = (
        'category',
        'name',
        'quantity',
        'unit',
        'formatted_price',
    )
    list_filter = (
        'category',
    )
    search_fields = (
        'name',
    )

    def formatted_price(self, obj):
        return '{0:n} Ft'.format(float(obj.price))

    formatted_price.short_description = 'Price'
    formatted_price.admin_order_field = 'price'

    form_view_extra_js = [static("app/scripts/material/form.js")]

    row_as_link = 'app_material_modeladmin_edit'


class MaterialCategoryAdmin(ModelAdmin):
    model = MaterialCategory
    menu_icon = 'fa-tags'
    menu_label = _('Categories')

    list_display = (
        'name',
    )
    search_fields = (
        'name',
    )

    row_as_link = 'app_materialcategory_modeladmin_edit'


class MaterialAdminGroup(ModelAdminGroup):
    menu_icon = 'fa-cubes'
    menu_label = _('Storage')
    items = [
        MaterialAdmin,
        MaterialCategoryAdmin,
        FinishedProductAdmin,
        TransactionAdmin,
    ]


modeladmin_register(MaterialAdminGroup)
