from rest_framework import serializers

from .models import Material


class MaterialSerializer(serializers.ModelSerializer):
    category__name = serializers.SerializerMethodField()

    class Meta:
        model = Material
        fields = (
            'id',
            'name',
            'unit',
            'category',
            'category__name',
            'price',
            'net_price',
            'vat'
        )

    def get_category__name(self, obj):
        return obj.category.name
