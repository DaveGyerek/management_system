from decimal import Decimal
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import ugettext as _
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.core.models import Orderable

from .constants import UNITS, UNIT_PIECE


class MaterialCategory(ClusterableModel):
    """
    Represent a material category model which can help to
    organize the :model:`app.Material` objects.
    """

    class Meta:
        verbose_name = _('Material Category')
        verbose_name_plural = _('Material Categories')
        ordering = ['name']

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=50,
        unique=True,
    )

    def __str__(self):
        return self.name


class Material(Orderable):
    """
    Represents the smallest building blocks of the app.
    Related to :model:`app.MaterialCategory`.
    With this we can build up the :model:`app.Product` objects.
    """

    class Meta:
        verbose_name = _('Material')
        verbose_name_plural = _('Materials')
        ordering = ['name']

    category = ParentalKey(
        'app.MaterialCategory',
        verbose_name=_('Category'),
        related_name='materials',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=50,
        unique=True,
    )
    unit = models.SmallIntegerField(
        verbose_name=_('Unit'),
        choices=UNITS,
        default=UNIT_PIECE,
    )
    price = models.DecimalField(
        verbose_name=_('Unit Price (gr.)'),
        max_digits=10,
        decimal_places=4,
        validators=[MinValueValidator(Decimal('0.0001'))],
        default=1,
    )
    net_price = models.DecimalField(
        verbose_name=_('Unit Price'),
        max_digits=10,
        decimal_places=4,
        validators=[MinValueValidator(Decimal('0.0001'))],
        default=1,
    )
    vat = models.PositiveIntegerField(
        verbose_name=_('VAT'),
        validators=[MaxValueValidator(100)],
        default=27,
        blank=True,
        null=True,
    )
    quantity = models.IntegerField(
        verbose_name=_('Quantity'),
        default=0,
    )

    def __str__(self):
        return '{} ({})'.format(self.name, self.get_unit_display())
