from django.contrib import admin

from .models import MaterialCategory, Material

admin.site.register([MaterialCategory, Material])
