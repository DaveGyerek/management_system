from django.utils.translation import ugettext as _

UNIT_KG = 0
UNIT_M = 1
UNIT_PIECE = 2
UNIT_M2 = 3

UNITS = (
    (UNIT_KG, _('kg')),
    (UNIT_M, _('m')),
    (UNIT_PIECE, _('db')),
    (UNIT_M2, _('m2')),
)
