from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db import models

from .constants import STATUSES, OPEN, PAYED, TRANSFER


def payment_date():
    return timezone.now() + timezone.timedelta(days=30)
    

class DeliveryNote(models.Model):
    """
    With this model we can create delivery notes for 
    various :model:`app.Client` which can contain 
    :model:`app.OrderedProduct` -s.
    """

    class Meta:
        verbose_name = _('Delivery note')
        verbose_name_plural = _('Delivery notes')
        ordering = [
            '-delivery_timestamp',
        ]

    client = models.ForeignKey(
        'app.Client',
        verbose_name=_('Client'),
        related_name='delivery_notes',
        on_delete=models.CASCADE,
    )
    delivery_timestamp = models.DateField(
        verbose_name=_('Date of delivery'),
        default=timezone.now,
    )
    status = models.SmallIntegerField(
        verbose_name=_('State'),
        choices=STATUSES,
        default=OPEN,
    )
    voucher_number = models.CharField(
        verbose_name=_('Voucher number'),
        max_length=50,
    )
    delivery_address = models.CharField(
        verbose_name=_('Delivery address'),
        max_length=300,
        blank=True,
        null=True,
    )
    delivery_price = models.DecimalField(
        verbose_name=_('Delivery price'),
        max_digits=10,
        decimal_places=2,
        default=0,
    )
    delivery_percent = models.DecimalField(
        verbose_name=_('Delivery percent'),
        decimal_places=0,
        max_digits=3,
        default=8,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        blank=True,
        null=True,
    )
    fulfillment_timestamp = models.DateField(
        verbose_name=_('Date of fulfillment'),
        default=payment_date,
    )
    created_timestamp = models.DateField(
        verbose_name=_('Date of creation'),
        default=timezone.now,
    )
    payed_with = models.SmallIntegerField(
        choices=PAYED,
        default=TRANSFER,
        verbose_name=_('Payment method'),
        blank=True,
        null=True,
    )

    def __str__(self):
        return f'{self.client}-{self.delivery_timestamp} ({self.voucher_number})'
