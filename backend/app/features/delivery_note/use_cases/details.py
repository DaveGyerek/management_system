import json

from django.utils.translation import ugettext_lazy as _

from ...ordered_product.use_cases.common import OrderedProductAndLabelsContext
from ...order.repository import OrderRepository
from ...delivery_note.serializers import DeliveryNoteDetailsSerializer
from ..repository import DeliveryNoteRepository


class DeliveryNoteDetailsTemplateContext(OrderedProductAndLabelsContext):
    class Request:
        def __init__(self, dn_id: int):
            self.dn_id = dn_id

    def __init__(self, request: Request, delivery_note_repository: DeliveryNoteRepository = None,
                 order_repository: OrderRepository = None):
        super().__init__(delivery_note_repository, order_repository)
        self.request = request

    def execute(self) -> dict:
        dn = self.delivery_note_repository.get_delivery_note(self.request.dn_id)
        product_orders = self.order_repository.get_ordered_products_groups_for_delivery_note(dn) \
            .values('id', 'not_in_delivery_count', 'product__name', 'delivery_count', 'ready_count',
                    'price', 'note')
        products_data = []

        sum_price = 0
        for product_order in product_orders:
            sum_price += int(product_order['delivery_count'] * product_order['price'])
            products_data.append([
                [product_order['id'], product_order['not_in_delivery_count']],

                product_order['product__name'],
                '{0}/{1}'.format(product_order['delivery_count'], product_order['ready_count']),
                product_order['id'],
                '{0:n} Ft'.format(int(product_order['price'])),
                '{0:n} Ft'.format(int(product_order['delivery_count'] * product_order['price'])),
                product_order['note'] or ''])

        delivery_price = ""
        if dn.delivery_price:
            delivery_price = '{} Ft'.format(int(dn.delivery_price))
        elif dn.delivery_percent:
            delivery_price = '{0} % ({1:n} Ft)'.format(dn.delivery_percent, int(sum_price * (dn.delivery_percent / 100)))

        delivery_note_data = (
            (_('Client'), dn.client.name),
            (_('Voucher number'), dn.voucher_number),
            (_('Delivery address'), dn.delivery_address),
            (_('Delivery timestamp'), dn.delivery_timestamp),
            (_('Delivery Price'), delivery_price),
            (_('State'), dn.get_status_display()),
        )

        headers = (
            _('Name'), _('Quantity'), _('Identifier'),
            _('Price'), _('Value (br.)'), _('Notes'))
        context = {
            'headers': headers,
            'delivery_note': dn,
            'delivery_note_data': delivery_note_data,
            'ordered_products': products_data,
        }

        context.update(self.get_manufacture_plans())
        context.update(self.get_delivery_notes())
        context.update(self.get_orders())

        return context

    def create_product_order_details(self, dn):
        serializer = DeliveryNoteDetailsSerializer(dn)
        data = serializer.data
        return json.dumps(data)
