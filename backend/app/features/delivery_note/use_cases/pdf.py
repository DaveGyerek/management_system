# from constance import config
from django.utils.translation import ugettext_lazy as _

from .details import DeliveryNoteDetailsTemplateContext


class DeliveryNotePdfTemplateContext(DeliveryNoteDetailsTemplateContext):
    def execute(self) -> dict:
        delivery_note = self.delivery_note_repository.get_delivery_note(self.request.dn_id)
        client = delivery_note.client
        product_orders = self.order_repository.get_ordered_products_groups_for_delivery_note(delivery_note)
        # try:
        #     vat = float(delivery_note.vat) / 100
        # except ValueError:
        vat = 0.27

        sum_price = 0
        sum_net_price = 0

        header_details = [
            [_("Cím"), "Example company address",
             _("Cím"), client.delivery_address or '-'],
            [_("Bank account"), "2312123 13123123 1232",
             "", ""],
            [_("Vat number"), "12312312312",
             _("Vat number"), client.tax_id or '']
        ]

        delivery_note_details = [
            [_("Voucher number"), delivery_note.voucher_number or ''],

            [_("Fulfillment date"), _("Created date"), _("Payed with")],
            [delivery_note.fulfillment_timestamp or '', delivery_note.created_timestamp or '',
             delivery_note.get_payed_with_display() or ''],
        ]

        products_data = []

        products_headers = (
            _("Id"),
            _("Name"),
            _("Qty."),
            _("Price"),
            _("Disc."),
            _("Net pr."),
            _("VAT %"),
            _("VAT"),
            _("Gr. pr."),
        )

        number = 0
        for product_order in product_orders:
            number += 1
            original_price = product_order.product.price
            net_price = float(product_order.price)
            price = float(product_order.price) * (1 + vat)
            discount = 100 - int((product_order.price / original_price) * 100)
            vat_value = int(price * product_order.delivery_count - net_price * product_order.delivery_count)

            products_data.append(['{0}{1}'.format('0' * (3 - len(str(number))), number),
                                  product_order.product.name,
                                  '{} db'.format(product_order.delivery_count),
                                  '{0:n}'.format(int(original_price)),
                                  '{} %'.format(discount) if discount > 0 else '-',
                                  '{0:n}'.format(int(net_price * product_order.delivery_count)),
                                  # '{} %'.format(float(vat * 100)),
                                  '{} %'.format(str(round((vat * 100), 1) if (vat * 100) % 1 else int(vat * 100))),
                                  '{0:n}'.format(vat_value),
                                  '{0:n}'.format(int(price * product_order.delivery_count)),

                                  'Order: {}'.format(product_order.order.delivery_timestamp),
                                  ])

            sum_price += price * product_order.delivery_count
            sum_net_price += net_price * product_order.delivery_count

        # GET DELIVERY PRICE
        delivery_net_price = delivery_note.delivery_price
        if not delivery_note.delivery_price:
            delivery_net_price = int(sum_net_price) * (delivery_note.delivery_percent / 100) \
                if delivery_note.delivery_percent else 0
        number += 1
        delivery_price = int(float(delivery_net_price) * (1 + vat))
        delivery_price_data = ['{0}{1}'.format('0' * (3 - len(str(number))), number),
                               _("Delivery price"),
                               '1 db',
                               '{0:n}'.format(int(delivery_net_price)),
                               '-',
                               '{0:n}'.format(int(delivery_net_price)),
                               # '{} %'.format(float(vat * 100)),
                               '{} %'.format(str(round((vat * 100), 1) if (vat * 100) % 1 else int(vat * 100))),
                               '{0:n}'.format(int(delivery_price - delivery_net_price)),
                               '{0:n}'.format(int(delivery_price))]

        if delivery_price:
            sum_price += int(delivery_price)
            sum_net_price += int(delivery_net_price)

        summaries = [
            [_("VAT %"), _("Net price"), _("VAT"), _("Gr. price")],
            [str(round((vat * 100), 1) if (vat * 100) % 1 else int(vat * 100)),
             "{0:n}".format(int(sum_net_price)),
             "{0:n}".format(int(sum_price - sum_net_price)), "{0:n}".format(int(sum_price))]
        ]

        return {
            'delivery_note': delivery_note,

            'company_name': 'Example company',
            'client': delivery_note.client,

            'header_details': header_details,

            'delivery_note_details': delivery_note_details,

            'products_headers': products_headers,
            'product_orders': products_data,

            'summaries': summaries,

            'delivery_net_price': "{0:n}".format(int(delivery_net_price)),
            'delivery_price_data': delivery_price_data,

            'sum_price': "{0:n}".format(int(sum_price))
        }
