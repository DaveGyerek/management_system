import os

from django.conf import settings
from django.conf.urls import url

from .views.pdf import DeliveryNotePdfView
from .views.details import DeliveryNoteDetailsView, DeliveryNoteOrderedProductDetailsApiView

urlpatterns = [
    url(r'^deliverynote/(?P<pk>\d+)/$', DeliveryNoteDetailsView.as_view(),
        name='delivery-note-details'),

    url(r'^api/1/delivery-note/(?P<dn_id>\d+)/ordered-product/(?P<po_id>\d+)/$',
        DeliveryNoteOrderedProductDetailsApiView.as_view(),
        name='delivery-note-product-order-data'),

    # url(r'^deliverynote/pdf/(?P<delivery_note_id>\d+)/$',
    #     DeliveryNotePdfView.as_view(), name='delivery-note-pdf-export'),
    url(r'^deliverynote/pdf/(?P<delivery_note_id>\d+)/$',
        DeliveryNotePdfView.as_view(
            path=os.path.join(str(settings.APPS_DIR), 'tmp.pdf')
        ), name='delivery-note-pdf-export'),
]
