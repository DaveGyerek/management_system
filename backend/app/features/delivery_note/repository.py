from django.shortcuts import get_object_or_404

from .constants import OPEN
from .models import DeliveryNote


class DeliveryNoteRepository:
    def get_open_delivery_notes(self):
        return DeliveryNote.objects.filter(status=OPEN).only('id', 'voucher_number', 'delivery_timestamp')

    def get_delivery_note(self, delivery_note_id):
        queryset = DeliveryNote.objects.all().prefetch_related('ordered_products')
        delivery_note = get_object_or_404(queryset, id=delivery_note_id)
        return delivery_note
