from django.contrib.staticfiles.templatetags.staticfiles import static
from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import modeladmin_register
from wagtail.admin.edit_handlers import FieldRowPanel, FieldPanel, MultiFieldPanel

from .models import DeliveryNote
from backend.custom_wagtail.modeladmin import ModelAdmin

DeliveryNote.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('client', classname="col6"),
            FieldPanel('voucher_number', classname="col6"),
            FieldPanel('delivery_timestamp', classname="col6"),
        )),
    ), _('Basic details')),
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('delivery_address', classname="col6"),
            FieldPanel('fulfillment_timestamp', classname="col6"),
            FieldPanel('created_timestamp', classname="col6"),
            FieldPanel('payed_with', classname="col6"),
        )),
    ), _('Delivery details')),
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('delivery_price', classname="col6"),
            FieldPanel('delivery_percent', classname="col6"),
        )),
    ), _('Delivery price')),
]


class DeliveryNoteAdmin(ModelAdmin):
    model = DeliveryNote
    menu_icon = 'fa-envelope'
    menu_label = _('Delivery notes')
    list_display = (
        'client',
        'voucher_number',
        'status',
        'delivery_timestamp',
    )

    list_filter = (
        'status',
    )
    search_fields = (
        'client__name',
        'voucher_number',
    )

    row_as_link = 'delivery-note-details'


modeladmin_register(DeliveryNoteAdmin)
