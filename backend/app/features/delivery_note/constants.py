from django.utils.translation import ugettext as _

OPEN = 0
READY = 1
STATUSES = (
    (OPEN, _('Open')),
    (READY, _('Delivered')),
)

TRANSFER = 0
CASH = 1
COMMISSION = 2
PAYED = (
    (CASH, _('Cash')),
    (TRANSFER, _('Transfer')),
    (COMMISSION, _('Commission')),
)
