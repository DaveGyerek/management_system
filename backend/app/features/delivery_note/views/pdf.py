import os

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.conf import settings
from django.views.generic import TemplateView
from easy_pdf.views import PDFTemplateResponseMixin

from ..use_cases.pdf import DeliveryNotePdfTemplateContext
from ....utils.pdf import RenderPdf


# class DeliveryNotePdfView(PDFTemplateResponseMixin, TemplateView):
#     def get(self, request, *args, **kwargs):
#         if not request.user.is_authenticated:
#             return HttpResponseRedirect(reverse('login'))
#         return super().get(request, *args, **kwargs)
#
#     def get_template_names(self):
#         return [
#             'app/delivery_note/easy_pdf.html',
#         ]
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         request = DeliveryNotePdfTemplateContext.Request(kwargs['delivery_note_id'])
#         use_case = DeliveryNotePdfTemplateContext(request)
#         context.update(**use_case.execute())
#         return context


class DeliveryNotePdfView(View):
    path = None

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))
        filename = self.create_pdf(request, *args, **kwargs)
        try:
            with open(filename, 'rb') as pdf:
                response = HttpResponse(pdf.read(), content_type='application/pdf')
                response['Content-Disposition'] = 'inline;filename={}'.format(os.path.basename(filename))
                return response
        except FileNotFoundError:
            return HttpResponseBadRequest(
                'Your url did not return anything'.format(get_str(request, *args, **kwargs)))

    def create_pdf(self, request, *args, **kwargs):
        abs_path = get_str(self.path, request, *args, **kwargs)
        path = os.path.relpath(abs_path, settings.BASE_DIR).replace('\\', '/').replace('dev-data/', 'backend/dev-data/')
        filename = os.path.abspath(path)

        options = {
            'margin-left': "5mm",
            'margin-right': "5mm",
            'margin-bottom': "8mm",
            'footer-center': 'Page [topage]/[page]',
            'footer-spacing': '3',
            'footer-font-size': '7'
        }

        request = DeliveryNotePdfTemplateContext.Request(kwargs['delivery_note_id'])
        use_case = DeliveryNotePdfTemplateContext(request)
        context = use_case.execute()
        pdf_use_case = RenderPdf()
        pdf_request = RenderPdf.Request(
            output_path=filename,
            template_name='app/delivery_note/pdf.html',
            context=context,
            options=options
        )
        pdf_use_case.execute(pdf_request)
        return filename


def get_str(original, request, *args, **kwargs):
    if callable(original):
        return original(request, *args, **kwargs)
    if isinstance(original, str):
        return original
    raise Exception('Not valid url')
