from django.views.generic import TemplateView
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ...order.serializers import ProductOrderForDeliveryNoteSerializer
from ...order.repository import OrderRepository
from ....utils.mixins import SendToAnotherDeliveryNoteMixin
from ..use_cases.details import DeliveryNoteDetailsTemplateContext
from ...user.permissions import ActivatedUserMixin


class DeliveryNoteDetailsView(SendToAnotherDeliveryNoteMixin, ActivatedUserMixin, TemplateView):
    template_name = 'app/delivery_note/details.html'

    def get_context_data(self, **kwargs):
        request = DeliveryNoteDetailsTemplateContext.Request(kwargs['pk'])
        use_case = DeliveryNoteDetailsTemplateContext(request)
        context = use_case.execute()

        context.update(self.delivery_details_context)

        return context


class DeliveryNoteOrderedProductDetailsApiView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, *args, **kwargs):
        queryset = OrderRepository().get_product_order_details_for_delivery_note(kwargs['dn_id'],
                                                                                 kwargs['po_id'])
        serializer_class = ProductOrderForDeliveryNoteSerializer(queryset)
        return Response(serializer_class.data)
