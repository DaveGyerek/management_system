from rest_framework import serializers

from ..order.repository import OrderRepository
from ..order.serializers import ProductOrderForDeliveryNoteSerializer
from .models import DeliveryNote


class DeliveryNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeliveryNote
        fields = ['id', 'voucher_number', 'delivery_timestamp']


class DeliveryNoteDetailsSerializer(serializers.ModelSerializer):
    product_orders = serializers.SerializerMethodField()

    class Meta:
        model = DeliveryNote
        fields = ['product_orders']

    def get_product_orders(self, delivery_note):
        repository = OrderRepository()
        queryset = repository.get_ordered_products_groups_for_delivery_note(dn=delivery_note)
        serializer = ProductOrderForDeliveryNoteSerializer(instance=queryset, many=True)
        return serializer.data
