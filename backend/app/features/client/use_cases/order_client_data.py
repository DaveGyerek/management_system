from backend.app.models import Product, UniquePricing
from ..models import Client


class OrderClientData:
    class Request:
        def __init__(self, client: Client):
            self.client = client

    def __init__(self, request: Request):
        self.request = request

    def execute(self) -> dict:
        products = Product.objects.prefetch_related('product_type')
        discount = self.request.client.discount
        unique_pricings = UniquePricing.objects.filter(client=self.request.client)
        unique_pricings = {x.product_id: x.price for x in unique_pricings}

        prices = {}
        for product in products:
            price = float(product.price)

            if product.id in unique_pricings:
                price = float(unique_pricings[product.id])
            elif discount:
                price = float(price) * float(1 - (discount / 100))

            prices.update({product.id: price})
        return prices
