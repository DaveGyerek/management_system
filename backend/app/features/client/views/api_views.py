from rest_framework.generics import RetrieveAPIView

from backend.app.features.client.models import Client
from ..serializers import ClientSerializer


class ClientRetrieveView(RetrieveAPIView):
    queryset = Client.objects.prefetch_related('stores', 'unique_pricings', 'unique_pricings__product')
    serializer_class = ClientSerializer
