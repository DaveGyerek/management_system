from django.utils.translation import ugettext as _
from wagtail.contrib.modeladmin.options import modeladmin_register
from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, MultiFieldPanel, InlinePanel

from .models import Client, Store, UniquePricing
from backend.custom_wagtail.modeladmin import ModelAdmin

Store.panels = [
    MultiFieldPanel((
        FieldPanel('name'),
        FieldPanel('delivery_address'),
    )),
]

UniquePricing.panels = [
    MultiFieldPanel((
        FieldPanel('product'),
        FieldPanel('price'),
    )),
]

Client.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('name', classname='col6'),
            FieldPanel('email', classname='col6'),
            FieldPanel('billing_address', classname='col6'),
            FieldPanel('delivery_address', classname='col6'),
            FieldPanel('phone_number', classname='col6'),
        )),
    ), _('Personal Information')),

    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('short_name', classname='col6'),
            FieldPanel('company_identifier', classname='col6'),
            FieldPanel('tax_id', classname='col6'),
        )),
    ), _('Company Information')),

    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('discount', classname='col6'),
        )),
    ), _('Discount')),

    MultiFieldPanel((
        InlinePanel('stores', label=_('Store'), ),
    ), _('Stores')),
    MultiFieldPanel((
        InlinePanel('unique_pricings'),
    ), _('Unique Pricings'))
]


class ClientAdmin(ModelAdmin):
    model = Client
    menu_icon = 'fa-user'
    list_display = (
        'name',
        'short_name',
        'email',
        'phone_number',
        'billing_address',
    )
    search_fields = (
        'name',
        'short_name',
        'email',
        'phone_number',
        'billing_address',
    )

    row_as_link = 'app_client_modeladmin_edit'


modeladmin_register(ClientAdmin)
