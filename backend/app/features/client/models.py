from decimal import Decimal

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext as _
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.core.models import Orderable


class Client(ClusterableModel):
    """
    Represents the real life clients with whom the company has contact with
    """
    class Meta:
        verbose_name = _('Client')
        verbose_name_plural = _('Clients')
        ordering = [
            'name',
            'billing_address',
        ]

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=200,
        unique=True,
    )
    short_name = models.CharField(
        verbose_name=_('Short name'),
        max_length=50,
        default='',
        blank=True,
        null=True,
    )
    phone_number = models.CharField(
        verbose_name=_('Phone number'),
        max_length=20,
        blank=True,
        null=True
    )
    email = models.EmailField(
        verbose_name=_('Email'),
    )
    billing_address = models.CharField(
        verbose_name=_('Billing address'),
        max_length=300,
    )
    delivery_address = models.CharField(
        verbose_name=_('Delivery address'),
        max_length=300,
        blank=True,
        null=True,
    )
    company_identifier = models.CharField(
        verbose_name=_('Company identifier'),
        max_length=300,
        blank=True,
        null=True,
    )
    tax_id = models.CharField(
        verbose_name=_('Tax number'),
        max_length=300,
    )
    discount = models.DecimalField(
        verbose_name=_('Discount'),
        decimal_places=0,
        max_digits=3,
        validators=[MinValueValidator(1), MaxValueValidator(100)],
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name


class Store(Orderable, models.Model):
    """
    Every :model:`app.Client` object can have multiple stores to be stored with
    """

    class Meta:
        verbose_name = _('Store')
        verbose_name_plural = _('Stores')
        ordering = [
            'sort_order',
            'client',
            'name',
        ]
        unique_together = [
            'client',
            'name',
        ]

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=300,
    )
    delivery_address = models.CharField(
        verbose_name=_('Delivery address'),
        max_length=300,
        blank=True,
        null=True,
    )
    client = ParentalKey(
        'app.Client',
        related_name='stores',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.client} - {self.name} ({self.delivery_address})'


class UniquePricing(Orderable):
    """
    Connects :model:`app.Client` with :model:`app.Product` , so each :model:`app.Client`
    can have unique pricings with every product if them need to
    """
    class Meta:
        verbose_name = _('Unique pricing')
        verbose_name_plural = _('Unique pricings')
        unique_together = [
            'client',
            'product',
        ]
        ordering = (
            'sort_order',
        )

    client = ParentalKey(
        'app.Client',
        related_name='unique_pricings',
        verbose_name=_('Client'),
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        'app.Product',
        related_name='unique_pricings',
        verbose_name=_('Product'),
        on_delete=models.CASCADE,
    )
    price = models.DecimalField(
        verbose_name=_('Price'),
        max_digits=10,
        decimal_places=2,
        validators=[MinValueValidator(Decimal('0.01'))],
    )

    def __str__(self):
        return f'{self.client.name} - {self.product.name} - {self.price}'
