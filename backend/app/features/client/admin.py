from django.contrib import admin

from .models import Client, Store, UniquePricing

admin.site.register([
    Client,
    Store,
    UniquePricing,
])
