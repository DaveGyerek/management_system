from django.conf.urls import url

from .views.api_views import ClientRetrieveView

urlpatterns = [
    url(r'^api/1/client/(?P<pk>\d+)/$', ClientRetrieveView.as_view(), name='get-client'),
]
