from rest_framework import serializers

from .use_cases.order_client_data import OrderClientData
from .models import Client, Store


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = [
            'id',
            'name',
            'delivery_address',
        ]


class ClientSerializer(serializers.ModelSerializer):
    stores = StoreSerializer(many=True, read_only=True)
    prices = serializers.SerializerMethodField()

    class Meta:
        model = Client
        fields = [
            'name',
            'short_name',
            'phone_number',
            'email',
            'billing_address',
            'delivery_address',
            'company_identifier',
            'tax_id',
            'stores',
            'prices',
        ]

    def get_prices(self, obj):
        request = OrderClientData.Request(obj)
        use_case = OrderClientData(request)
        return use_case.execute()


class ClientSerializerWithoutPrice(ClientSerializer):
    class Meta:
        model = Client
        fields = [
            'id',
            'name',
            'short_name',
            'phone_number',
            'email',
            'billing_address',
            'delivery_address',
            'company_identifier',
            'tax_id',
            'stores',
        ]
