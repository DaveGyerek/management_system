import pytest

from ...product.models import Product, ProductType
from ...client.models import Client, UniquePricing
from ..use_cases.order_client_data import OrderClientData


def _create_product_and_client():
    product_type = ProductType.objects.create(name='asdf')
    product = Product.objects.create(name='asdf', price=1000, product_type=product_type)
    client = Client.objects.create(name='asdf', email='asdf@asdf.asdf', billing_address='asdf')

    return product, client


def _execute_use_case(client: Client):
    request = OrderClientData.Request(client=client)
    use_case = OrderClientData(request)
    return use_case.execute()

@pytest.mark.django_db
def test_order_client_data():
    product, client = _create_product_and_client()
    result = _execute_use_case(client)

    assert isinstance(result, dict)
    assert product.id in result
    assert result[product.id] == 1000


@pytest.mark.django_db
def test_order_client_data_with_unique_pricing():
    _unique_price = 33333

    product, client = _create_product_and_client()
    UniquePricing.objects.create(client=client, product=product, price=_unique_price)
    result = _execute_use_case(client)

    assert result[product.id] == _unique_price


@pytest.mark.django_db
def test_order_client_data_with_discount():
    _discount_percent = 30

    product, client = _create_product_and_client()
    client.discount = _discount_percent
    client.save()

    result = _execute_use_case(client)

    assert result[product.id] == product.price * (1 - (_discount_percent/100))
