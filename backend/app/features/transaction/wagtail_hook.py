from django.db.models import Case, When, Q, CharField
from django.utils.translation import ugettext_lazy as _
from wagtail.admin.edit_handlers import MultiFieldPanel, FieldRowPanel, FieldPanel

from backend.custom_wagtail.modeladmin import ModelAdmin
from .models import Transaction

Transaction.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('material', classname='col6'),
            FieldPanel('quantity', classname='col6'),
            FieldPanel('vat', classname='col6'),
            FieldPanel('net_price', classname='col6'),
        )),
    ), _('Transaction'))
]


class TransactionAdmin(ModelAdmin):
    model = Transaction
    menu_icon = 'fa-refresh'
    menu_label = _('Transactions')
    list_display = (
        '_material',
        'type',
        '_quantity',
        '_unit',
        'formatted_net_price',
        'formatted_price',
        'timestamp',
        'user'
    )

    list_per_page = 20

    search_fields = (
        'material__name',
        'ordered_product__product_order__product__name',
        'user__first_name',
        'user__last_name'
    )
    list_filter = (
        'timestamp',
    )

    def type(self, obj):
        return {
            1: 'Out',
            2: 'Storage -> Making',
            3: 'In',
        }.get(obj.type, "Out")

    type.short_description = _('Type')
    type.admin_order_field = 'type'

    def _material(self, obj):
        if obj.material:
            return obj.material.name
        return obj.ordered_product.product_order.product.name

    _material.short_description = _('Material/Product')
    _material.admin_order_field = 'material__name'

    def _unit(self, obj):
        if obj.material:
            return obj.material.get_unit_display()
        return "db"

    _unit.short_description = _('Unit')
    _unit.admin_order_field = 'material__unit'

    def user(self, obj):
        if obj.user:
            return obj.user.name
        return "-"

    user.short_description = _('User')
    user.admin_order_field = 'user__name'

    def _quantity(self, obj):
        return '{0:n}'.format(float(abs(obj.quantity)))

    _quantity.short_description = _('Quantity')
    _quantity.admin_order_field = 'quantity'

    def formatted_net_price(self, obj):
        if obj.net_price:
            return '{0:n} Ft'.format(float(obj.net_price))
        return '-'

    formatted_net_price.short_description = _('Net Price')
    formatted_net_price.admin_order_field = 'net_price'

    def formatted_price(self, obj):
        if obj.price:
            return '{0:n} Ft'.format(float(obj.price))
        return '-'

    formatted_price.short_description = _('Price')
    formatted_price.admin_order_field = 'price'

    def get_extra_attrs_for_row(self, obj, context):
        if obj.ordered_product and obj.quantity < 0:
            class_name = 'automatic-out'
        elif obj.quantity < 0:
            class_name = 'trans-out'
        else:
            class_name = 'trans-in'
        return {
            'class': class_name,
        }

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(type=Case(
            When(Q(ordered_product__isnull=False) & Q(quantity__lt=0), then=1),
            When(Q(ordered_product__isnull=False) & Q(quantity__gt=0), then=2),
            When(Q(ordered_product__isnull=True) & Q(quantity__gt=0), then=3),
            default=4,
            output_field=CharField(),
        ))
