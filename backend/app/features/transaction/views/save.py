from ..use_cases.save import SaveTransaction


def save_transaction(transaction):
    request = SaveTransaction.Request(transaction)
    use_case = SaveTransaction(request=request)
    use_case.execute()
