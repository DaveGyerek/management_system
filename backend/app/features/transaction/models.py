from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .views.save import save_transaction


class Transaction(models.Model):
    """
    Represents every transaction associated to the :model:`app.Material` -s
    of the company. Either if its done by a user increasing or decreasing
    the quantity of a :model:`app.Material` or automatically happened by
    an ordered product completion.
    """

    class Meta:
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')
        ordering = [
            '-timestamp',
        ]

    material = models.ForeignKey(
        'app.Material',
        verbose_name=_('Material'),
        related_name='transactions',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    quantity = models.FloatField(
        verbose_name=_('Quantity'),
        default=1.0,
    )
    vat = models.PositiveIntegerField(
        verbose_name=_('VAT'),
        validators=[MaxValueValidator(100)],
        default=27,
        blank=True,
        null=True,
    )
    net_price = models.FloatField(
        verbose_name=_('Net price'),
        validators=[MinValueValidator(0.0)],
        blank=True,
        null=True,
    )
    price = models.FloatField(
        verbose_name=_('Gross price'),
        validators=[MinValueValidator(0.0)],
        blank=True,
        null=True,
    )
    timestamp = models.DateTimeField(
        verbose_name=_('Date'),
        auto_now=True,
    )
    ordered_product = models.ForeignKey(
        'app.OrderedProduct',
        verbose_name=_('Ordered product'),
        related_name='transactions',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    user = models.ForeignKey(
        'app.User',
        verbose_name=_('User'),
        related_name='transactions',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f'{self.material.name if self.material else self.ordered_product.product.name} ' \
               f'{self.quantity} ({self.timestamp})'

    def save(self, *args, **kwargs):
        save_transaction(self)
        super(Transaction, self).save(*args, **kwargs)
