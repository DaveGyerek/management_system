from ....tests.app_test import BaseTestOrderAddedManufacturePlan
from ..models import Transaction
from ...material.models import MaterialCategory, Material
from ...ordered_product.models import OrderedProduct
from ...order.models import ProductOrderPart


class TestTransactionSave(BaseTestOrderAddedManufacturePlan):
    def setUp(self):
        super().setUp()
        mc = MaterialCategory.objects.create(name='mc')
        self.material_1 = Material.objects.create(category=mc, name="mat_1")
        self.material_2 = Material.objects.create(category=mc, name="mat_2")
        self.ordered_product = OrderedProduct.objects.get(pk=1)
        self.product_order_part_1 = ProductOrderPart.objects.create(product_order=self.ordered_product.product_order,
                                                                    name='part', material=self.material_1, price=2000,
                                                                    material_quantity=2)
        self.product_order_part_1 = ProductOrderPart.objects.create(product_order=self.ordered_product.product_order,
                                                                    name='part', material=self.material_2, price=2000,
                                                                    material_quantity=5)

    def test_transaction_with_material(self):
        assert self.material_1.quantity == 0
        Transaction.objects.create(material=self.material_1, quantity=5, price=1270, net_price=1000)
        assert self.material_1.quantity == 5
        Transaction.objects.create(material=self.material_1, quantity=-3, price=1270, net_price=1000)
        assert self.material_1.quantity == 2

    def test_transaction_with_ordered_product(self):
        assert self.material_1.quantity == 0
        assert self.material_2.quantity == 0
        Transaction.objects.create(material=self.material_1, quantity=30, price=1270, net_price=1000)
        Transaction.objects.create(material=self.material_2, quantity=50, price=1270, net_price=1000)
        assert self.material_1.quantity == 30
        assert self.material_2.quantity == 50
        Transaction.objects.create(ordered_product=self.ordered_product, quantity=-5)
        assert Material.objects.get(pk=1).quantity == 20
        assert Material.objects.get(pk=2).quantity == 25


_in_data = {
    "material_id": 1,
    "quantity": 10,
    "vat": 27,
    "net_price": 1000,
    "price": 1270
}
_in_data_2 = {
    "material_id": 1,
    "quantity": 10,
    "vat": 27,
    "net_price": 1000,
    "price": 1270
}
_out_data = {
    "material_id": 1,
    "quantity": -5,
    "vat": 27,
    "net_price": 1000,
    "price": 1270
}


def _execute_use_case(data):
    Transaction.objects.create(
        **data,
    )


class TestTransactionInOut(TestTransactionSave):

    def test_transaction_in(self):
        assert self.material_1.quantity == 0
        _execute_use_case(_in_data)
        material = Material.objects.get(pk=1)
        assert material.quantity == _in_data['quantity']

    def test_transaction_out(self):
        assert self.material_1.quantity == 0
        _execute_use_case(_in_data)
        _execute_use_case(_out_data)
        material = Material.objects.get(pk=1)
        assert material.quantity == _in_data['quantity'] + _out_data['quantity']
