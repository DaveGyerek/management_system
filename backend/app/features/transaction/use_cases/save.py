class SaveTransaction:
    class Request:
        def __init__(self, transaction):
            self.transaction = transaction

    def __init__(self, request: Request):
        self.request = request

    def execute(self):
        transaction = self.request.transaction
        if transaction.ordered_product:
            for product_part in transaction.ordered_product.product_order.parts.all():
                quantity = product_part.material_quantity * transaction.quantity
                product_part.material.quantity += quantity
                product_part.material.save()
        else:
            transaction.material.quantity += transaction.quantity
            transaction.material.save()
