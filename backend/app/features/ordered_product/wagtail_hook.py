from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from .views.index import FinishedProductIndexView
from ..order.repository import OrderRepository
from ..order.models import ProductOrder

from backend.custom_wagtail.modeladmin import ModelAdmin
from ...utils.helpers import UrlHelper


class FinishedProductAdmin(ModelAdmin):
    model = ProductOrder
    menu_icon = 'fa-check'
    menu_label = _('Finished Products')

    url_helper_class = UrlHelper(model, "finished-product")

    list_display = ('checkbox', 'order', 'finished_quantity', 'product', 'formatted_price', 'order__note')

    search_fields = ('product__name', 'order__client__name')

    def checkbox(self, obj):
        return format_html(
            '<input class="send-checkbox" type="checkbox" id="{}"',
            obj.id
        )

    checkbox.short_description = format_html(
        '<input class="send-checkbox" type="checkbox" id="select-all"'
    )

    def finished_quantity(self, obj):
        return obj.finished_count

    finished_quantity.short_description = _('Mennyiség')
    finished_quantity.admin_order_field = 'finished_count'

    def formatted_price(self, obj):
        return '{0:n} Ft'.format(float(obj.price))

    formatted_price.short_description = _('Ár')
    formatted_price.admin_order_field = 'price'

    def order__note(self, obj):
        return obj.order.note or ''

    order__note.short_description = _('Megjegyzés')

    def get_extra_attrs_for_row(self, obj, context):
        style = 'clickable-row'
        if obj.not_ready_count > 0:
            style += ' diff-row'
        return {
            'class': style,
        }

    def get_queryset(self, request):
        repository = OrderRepository()
        return repository.get_ordered_product_groups_for_finished_products_admin()

    index_view_class = FinishedProductIndexView
