import json

from backend.app.features.client.models import Client
from .common import OrderedProductAndLabelsContext
from ...manufacture_plan.repository import ManufacturePlanRepository
from ...client.serializers import ClientSerializerWithoutPrice
from ...order.repository import OrderRepository
from ..serializers import ProductOrderForFinishedProductsSerializer
from ...delivery_note.repository import DeliveryNoteRepository


class FinishedProductContext(OrderedProductAndLabelsContext):
    def __init__(self, delivery_note_repository: DeliveryNoteRepository = None,
                 manufacture_plan_repository: ManufacturePlanRepository = None,
                 order_repository: OrderRepository = None):
        super().__init__(delivery_note_repository, manufacture_plan_repository, order_repository)

    def get_product_order_details(self) -> dict:
        queryset = self.order_repository.get_ordered_product_groups_for_finished_products_admin() \
            .prefetch_related('ordered_products')
        serializer = ProductOrderForFinishedProductsSerializer(queryset, many=True)
        data = {'product_orders': serializer.data}
        json_data = json.dumps(data)

        return {
            'details_json': json_data
        }

    def get_client_details(self) -> dict:
        queryset = Client.objects.all().prefetch_related('stores')
        serializer = ClientSerializerWithoutPrice(queryset, many=True)
        data = {'clients': serializer.data}
        json_data = json.dumps(data)

        return {
            'clients_json': json_data
        }
