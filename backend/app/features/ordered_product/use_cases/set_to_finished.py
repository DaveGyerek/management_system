from ..constants import LABEL_READY, READY
from .common import OrderedProductsAndLabels


class SetOrderedProductsAndLabelsToFinished(OrderedProductsAndLabels):
    def execute(self):
        labels = self.repository.get_ordered_product_labels_by_id(self.label_ids)
        for label_id in self.label_ids:
            label = labels.get(id=label_id)
            ordered_product = label.ordered_product
            label.status = LABEL_READY
            label.save(update_fields=['status'])
            ready_labels = ordered_product.labels.filter(status=LABEL_READY)
            # If all parts of the ordered product is ready, set the whole product to ready
            if ready_labels.exists() and ready_labels.count() == ordered_product.labels.all().count():
                ordered_product.status = READY
                ordered_product.save(update_fields=['status'])
        self.repository.set_ordered_products_to_finished_by_id(self.ordered_product_ids)
