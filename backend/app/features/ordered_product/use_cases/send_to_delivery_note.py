from django.shortcuts import get_object_or_404

from .common import OrderedProductsAndLabels
from ..constants import ON_DELIVERY_NOTE, LABEL_ON_DELIVERY_NOTE
from ...delivery_note.repository import DeliveryNoteRepository
from ...order.repository import OrderRepository
from ...ordered_product.repository import OrderedProductRepository


class SendToDeliveryNote:
    class Request:
        def __init__(self, data: dict):
            self.data = data

    def __init__(self, request: Request,
                 order_repository: OrderRepository = None,
                 ordered_product_repository: OrderedProductRepository = None,
                 delivery_note_repository: DeliveryNoteRepository = None):
        self.request = request
        self.order_repository = order_repository or OrderRepository()
        self.ordered_product_repository = ordered_product_repository or OrderedProductRepository()
        self.delivery_note_repository = delivery_note_repository or DeliveryNoteRepository()

    def execute(self):
        ids = [product_order['id'] for product_order in self.request.data['product_orders']]
        delivery_note = self.delivery_note_repository.get_delivery_note(self.request.data['id'])
        product_orders = self.order_repository.get_product_orders(ids)

        for data in self.request.data['product_orders']:
            product_order = get_object_or_404(product_orders, id=data["id"])
            quantity = data['quantity']

            ordered_products = self.order_repository.get_ready_ordered_products(product_order)
            ordered_product_ids = ordered_products.values('pk')[:quantity]
            queryset = ordered_products.filter(pk__in=ordered_product_ids)
            self.ordered_product_repository.update_ordered_product_labels_for_ordered_product(ordered_products=queryset,
                                                                                              status=LABEL_ON_DELIVERY_NOTE)

            self.ordered_product_repository.update_ordered_products_to_delivery(ordered_products=queryset,
                                                                                delivery_note=delivery_note)


class MoveToAnotherDeliveryNote(SendToDeliveryNote):
    def execute(self):
        ids = [product_order['id'] for product_order in self.request.data['product_orders']]
        origin_delivery_note = self.delivery_note_repository.get_delivery_note(self.request.data['origin'])
        if 'duplicate' in self.request.data:
            new_voucher_number = origin_delivery_note.voucher_number + "/A"
            new_delivery_note = self.delivery_note_repository.duplicate_delivery_note(origin_delivery_note.id,
                                                                                      new_voucher_number)
        else:
            new_delivery_note = self.delivery_note_repository.get_delivery_note(self.request.data['new'])
        product_orders = self.order_repository.get_product_orders(ids)

        for data in self.request.data['product_orders']:
            product_order = get_object_or_404(product_orders, id=data["id"])
            quantity = data['quantity']

            ordered_products = self.order_repository.get_on_delivery_note_ordered_products(product_order,
                                                                                           origin_delivery_note)
            ordered_product_ids = ordered_products.values('pk')[:quantity]
            queryset = ordered_products.filter(pk__in=ordered_product_ids)
            self.ordered_product_repository.send_ordered_products_to_another_delivery(queryset, new_delivery_note)


class SentOrderedProductsAndLabelsToDelivery(OrderedProductsAndLabels):
    def __init__(self, request: OrderedProductsAndLabels.Request, repository: OrderedProductRepository = None,
                 delivery_note_repository: DeliveryNoteRepository = None):
        super().__init__(request, repository)
        self.delivery_note_repository = delivery_note_repository or DeliveryNoteRepository()

    def execute(self):
        delivery_note = self.delivery_note_repository.get_delivery_note(self.request.data["delivery_id"])
        labels = self.repository.get_ordered_product_labels_by_id(self.label_ids)
        for label_id in self.label_ids:
            label = labels.get(id=label_id)
            ordered_product = label.ordered_product
            label.status = LABEL_ON_DELIVERY_NOTE
            label.save()
            ordered_product.delivery_note = delivery_note
            ordered_product.status = ON_DELIVERY_NOTE
            ordered_product.save()
        self.repository.send_ordered_products_to_delivery_by_ids(self.ordered_product_ids,
                                                                 delivery_note)
