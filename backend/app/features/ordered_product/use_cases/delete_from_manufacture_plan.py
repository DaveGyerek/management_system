from ..use_cases.add_to_manufacture_plan import AddOrderedProductsToManufacturePlan


class DeleteOrderedProductsFromManufacturePlan(AddOrderedProductsToManufacturePlan):
    def execute(self):
        plan = self.manufacture_plan_repository.get_manufacture_plan(self.request.data['id'])

        for data in self.request.data['product_orders']:
            product_order = self.order_repository.get_product_order(data['id'])
            quantity = data['quantity']
            ordered_products = self.order_repository.get_in_work_ordered_products_for_manufacture_plan(product_order,
                                                                                                       plan)
            ordered_product_ids = ordered_products.values('pk')[:quantity]
            queryset = ordered_products.filter(pk__in=ordered_product_ids)
            self.ordered_product_repository.update_ordered_products_to_open(ordered_products=queryset)
        return True
