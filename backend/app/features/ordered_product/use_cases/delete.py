from ..repository import OrderedProductRepository
from ...delivery_note.repository import DeliveryNoteRepository


class DeleteOrderedProduct:
    class Request:
        def __init__(self, ordered_product_id):
            self.ordered_product_id = ordered_product_id

    def __init__(self, request: Request,
                 repository: OrderedProductRepository = None):
        self.request = request
        self.repository = repository or OrderedProductRepository()

    def execute(self):
        ordered_product = self.repository.get_ordered_product(self.request.ordered_product_id)
        delete_ordered_product(ordered_product)


def delete_ordered_product(ordered_product):
    product_order = ordered_product.product_order
    if product_order.ordered_products.all().count() == 1:
        if product_order.order.product_orders.all().count() == 1:
            product_order.order.delete()
        else:
            product_order.delete()
    else:
        ordered_product.delete()
        product_order.quantity = product_order.quantity - 1
        product_order.save(update_fields=['quantity'])


class DeleteOrderedProductsFromDeliveryNote:
    class Request:
        def __init__(self, delivery_note_id):
            self.delivery_note_id = delivery_note_id

    def __init__(self, request: Request,
                 repository: DeliveryNoteRepository = None):
        self.request = request
        self.repository = repository or DeliveryNoteRepository()

    def execute(self):
        delivery_note = self.repository.get_delivery_note(self.request.delivery_note_id)
        ordered_products = delivery_note.ordered_products.all()
        for ordered_product in ordered_products:
            delete_ordered_product(ordered_product)
        delivery_note.delete(empty=True)

