import datetime
from django.shortcuts import get_object_or_404

from backend.app.models import ManufacturePlan
from .common import OrderedProductsAndLabels
from ...manufacture_plan.repository import ManufacturePlanRepository
from ...order.repository import OrderRepository
from ..repository import OrderedProductRepository
from ..constants import WAITING, LABEL_WAITING


class AddOrderedProductsToManufacturePlan:
    class Request:
        def __init__(self, data: dict):
            self.data = data

    def __init__(self, request: Request,
                 order_repository: OrderRepository = None,
                 ordered_product_repository: OrderedProductRepository = None,
                 manufacture_plan_repository: ManufacturePlanRepository = None):
        self.request = request
        self.order_repository = order_repository or OrderRepository()
        self.ordered_product_repository = ordered_product_repository or OrderedProductRepository()
        self.manufacture_plan_repository = manufacture_plan_repository or ManufacturePlanRepository()

    def execute(self):
        ids = [product_order['id'] for product_order in self.request.data['product_orders']]
        if 'new_manufacture_plan' in self.request.data:
            date = datetime.datetime.strptime(self.request.data['new_manufacture_plan']['date'], '%Y-%m-%d').date()
            number = self.request.data['new_manufacture_plan']['number']
            plan = ManufacturePlan.objects.create(identification_number=number,
                                                  date=date)
        else:
            plan = self.manufacture_plan_repository.get_manufacture_plan(self.request.data['id'])
        product_orders = self.order_repository.get_product_orders(ids)

        for data in self.request.data['product_orders']:
            product_order = get_object_or_404(product_orders, id=data['id'])

            quantity = data['quantity']
            ordered_products = self.ordered_product_repository.get_open_ordered_products_for_product_order(
                product_order)

            ordered_product_ids = ordered_products.values('pk')[:quantity]

            self.ordered_product_repository.send_ordered_products_to_manufacture_plan(ordered_product_ids, mp=plan)


class AddOrderedProductsAndLabelsToManufacturePlan(OrderedProductsAndLabels):
    def __init__(self, request: OrderedProductsAndLabels.Request, repository: OrderedProductRepository = None,
                 manufacture_plan_repository: ManufacturePlanRepository = None):
        super().__init__(request, repository)
        self.manufacture_plan_repository = manufacture_plan_repository or ManufacturePlanRepository()

    def execute(self):
        mp = self.manufacture_plan_repository.get_manufacture_plan(self.request.data['manufacture_id'])
        for label_id in self.label_ids:
            label = self.repository.get_ordered_product_label(label_id)
            ordered_product = label.ordered_product
            label.status = LABEL_WAITING
            label.save()
            ordered_product.manufacture_plan = mp
            ordered_product.status = WAITING
            ordered_product.save()
        self.repository.send_ordered_products_to_manufacture_plan(self.ordered_product_ids,
                                                                  mp)
