from ...order.repository import OrderRepository
from ...ordered_product.repository import OrderedProductRepository
from .common import OrderedProductsAndLabels


class TranspositionOrderedProductsAndLabels(OrderedProductsAndLabels):
    def __init__(self, request: OrderedProductsAndLabels.Request,
                 repository: OrderedProductRepository = None,
                 order_repository: OrderRepository = None):
        super().__init__(request, repository)
        self.order_repository = order_repository or OrderRepository()

    def execute(self):
        new_order = self.order_repository.get_order(self.request.data['order'])
        ordered_products = self.repository.get_ordered_products(self.ordered_product_ids, self.label_ids)

        product_order = ordered_products.first().product_order
        product_order_parts = product_order.parts.all()

        if new_order == product_order.order:
            return

        if product_order.quantity == ordered_products.count():
            product_order.order = new_order
            product_order.save()
        else:
            # DECREASE QUANTITY IN THE ORIGINAL
            product_order.quantity -= ordered_products.count()
            product_order.save()

            # DUPLICATION
            product_order.id = None
            product_order.pk = None
            product_order.quantity = ordered_products.count()
            product_order.order = new_order
            product_order.duplication = True
            product_order.save()
            ordered_products.update(product_order=product_order)
            for part in product_order_parts:
                # DUPLICATE PARTS
                part.id = None
                part.pk = None
                part.product_order = product_order
                part.save()
