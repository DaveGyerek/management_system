from django.shortcuts import get_object_or_404

from .send_to_delivery_note import SendToDeliveryNote


class DeleteFromDeliveryNote(SendToDeliveryNote):
    def execute(self):
        ids = [product_order['id'] for product_order in self.request.data['product_orders']]
        delivery_note = self.delivery_note_repository.get_delivery_note(self.request.data['id'])
        product_orders = self.order_repository.get_product_orders(ids)

        for data in self.request.data['product_orders']:
            product_order = get_object_or_404(product_orders, id=data["id"])
            quantity = data['quantity']

            ordered_products = self.order_repository.get_on_delivery_note_ordered_products(product_order,
                                                                                           delivery_note)
            ordered_product_ids = ordered_products.values('pk')[:quantity]
            queryset = ordered_products.filter(pk__in=ordered_product_ids)
            self.ordered_product_repository.set_ordered_products_to_finished(queryset)
