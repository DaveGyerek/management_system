from ..repository import OrderedProductRepository


class CreateOrderedProducts:
    class Request:
        def __init__(self, product_order):
            self.product_order = product_order

    def __init__(self, request: Request,
                 ordered_product_repository: OrderedProductRepository = None):
        self.request = request
        self.ordered_product_repository = ordered_product_repository or OrderedProductRepository()

    def execute(self):
        po = self.request.product_order
        ordered_products = []
        for i in range(0, po.quantity):
            ordered_products.append(
                self.ordered_product_repository.create_ordered_product(product_order=po,
                                                                       price=po.price)
            )
        ordered_product_labels = []
        for op in ordered_products:
            labels = op.product_order.product.labels.all()
            if labels:
                for label in labels:
                    ordered_product_labels.append(
                        self.ordered_product_repository.create_label_object_for_bulk(name=label.name,
                                                                                     ordered_product=op)
                    )
        return self.ordered_product_repository.create_bulk_ordered_product_label(ordered_product_labels)
