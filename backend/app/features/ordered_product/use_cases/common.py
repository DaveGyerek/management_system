import json

from ...manufacture_plan.serializers import ManufacturePlanForSendingSerializer
from ...order.serializers import SimpleOrderSerializer
from ...delivery_note.serializers import DeliveryNoteSerializer
from ...order.repository import OrderRepository
from ...manufacture_plan.repository import ManufacturePlanRepository
from ...delivery_note.repository import DeliveryNoteRepository
from ..repository import OrderedProductRepository


class OrderedProductAndLabelsContext:
    def __init__(self,
                 delivery_note_repository: DeliveryNoteRepository = None,
                 manufacture_plan_repository: ManufacturePlanRepository = None,
                 order_repository: OrderRepository = None):
        self.delivery_note_repository = delivery_note_repository or DeliveryNoteRepository()
        self.manufacture_plan_repository = manufacture_plan_repository or ManufacturePlanRepository()
        self.order_repository = order_repository or OrderRepository()

    def get_delivery_notes(self) -> dict:
        delivery_notes = self.delivery_note_repository.get_open_delivery_notes()
        serializer = DeliveryNoteSerializer(delivery_notes, many=True)
        delivery_notes_for_sending = serializer.data
        delivery_notes_json = json.dumps(delivery_notes_for_sending)

        return {
            'delivery_json': delivery_notes_json
        }

    def get_manufacture_plans(self) -> dict:
        manufacture_plans = self.manufacture_plan_repository.get_manufacture_plans_for_sending()
        serializer = ManufacturePlanForSendingSerializer(manufacture_plans, many=True)
        manufacture_plans_for_sending = serializer.data
        manufacture_plans_json = json.dumps(manufacture_plans_for_sending)

        return {
            'manufacture_json': manufacture_plans_json
        }

    def get_orders(self) -> dict:
        orders = self.order_repository.get_simple_orders()
        serializer = SimpleOrderSerializer(orders, many=True)
        order_for_sending = serializer.data
        order_json = json.dumps(order_for_sending)

        return {
            'orders_json': order_json
        }


class OrderedProductsAndLabels:
    class Request:
        def __init__(self, data: dict):
            self.data = data

    def __init__(self, request: Request,
                 repository: OrderedProductRepository = None):
        self.request = request
        self.repository = repository or OrderedProductRepository()

        self.ordered_product_ids = request.data['ordered_products']
        self.label_ids = request.data['labels']
        # Delete the unnecessary ids
        labels = self.repository.get_all_labels_for_ordered_product_ids(self.ordered_product_ids)
        self.label_ids = [item for item in self.label_ids if item not in labels]
