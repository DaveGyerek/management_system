from ...transaction.models import Transaction


class SaveOrderedProduct:
    class Request:
        def __init__(self, ordered_product, is_new):
            self.ordered_product = ordered_product
            self.is_new = is_new

    def __init__(self, request: Request):
        self.request = request

    def execute(self):
        ordered_product = self.request.ordered_product
        if self.request.is_new or (ordered_product.tracker.saved_data['status'] < 2 and ordered_product.status > 1):
            Transaction.objects.create(ordered_product=ordered_product, quantity=-1)
        elif ordered_product.tracker.saved_data['status'] > 1 and ordered_product.status < 2:
            Transaction.objects.create(ordered_product=ordered_product)
