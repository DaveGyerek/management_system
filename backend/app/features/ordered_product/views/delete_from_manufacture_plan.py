from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..use_cases.delete_from_manufacture_plan import DeleteOrderedProductsFromManufacturePlan
from ..serializers import SendToManufactureSerializer as DeleteOrderedProductsSerializer


class DeleteOrderedProductsFromManufacturePlanView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        serializer = DeleteOrderedProductsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request = DeleteOrderedProductsFromManufacturePlan.Request(serializer.validated_data)
        use_case = DeleteOrderedProductsFromManufacturePlan(request)
        use_case.execute()
        return Response(status=201)
