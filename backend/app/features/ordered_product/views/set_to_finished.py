from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..use_cases.set_to_finished import SetOrderedProductsAndLabelsToFinished


class SetOrderedProductsAndLabelsToFinishedView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        request = SetOrderedProductsAndLabelsToFinished.Request(request.data)
        use_case = SetOrderedProductsAndLabelsToFinished(request)
        use_case.execute()
        return Response(status=201)
