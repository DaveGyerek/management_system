from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..serializers import SendToDeliveryNoteSerializer, SendToAnotherDeliveryNoteSerializer
from ..use_cases.send_to_delivery_note import SendToDeliveryNote, MoveToAnotherDeliveryNote, \
    SentOrderedProductsAndLabelsToDelivery


class SendOrderedProductsToDeliveryNoteView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        serializer = SendToDeliveryNoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request = SendToDeliveryNote.Request(serializer.validated_data)
        use_case = SendToDeliveryNote(request)
        use_case.execute()
        return Response(status=201)


class SendOrderedProductsToAnotherDeliveryNoteView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        serializer = SendToAnotherDeliveryNoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request = MoveToAnotherDeliveryNote.Request(serializer.validated_data)
        use_case = MoveToAnotherDeliveryNote(request)
        use_case.execute()
        return Response(status=201)


class SendOrderedProductsAndLabelsToDeliveryView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        request = SentOrderedProductsAndLabelsToDelivery.Request(request.data)
        use_case = SentOrderedProductsAndLabelsToDelivery(request)
        use_case.execute()
        return Response(status=201)
