from ..use_cases.save import SaveOrderedProduct


def save_ordered_product(ordered_product, is_new=False):
    request = SaveOrderedProduct.Request(ordered_product, is_new)
    use_case = SaveOrderedProduct(request=request)
    use_case.execute()
