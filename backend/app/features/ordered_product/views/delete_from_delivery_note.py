from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..use_cases.delete_from_delivery_note import DeleteFromDeliveryNote
from ..serializers import DeleteFromDeliveryNoteSerializer


class DeleteFromDeliveryNoteView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        serializer = DeleteFromDeliveryNoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request = DeleteFromDeliveryNote.Request(serializer.validated_data)
        use_case = DeleteFromDeliveryNote(request)
        use_case.execute()
        return Response(status=201)
