from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..serializers import SendToManufactureSerializer
from ..use_cases.add_to_manufacture_plan import AddOrderedProductsToManufacturePlan, \
    AddOrderedProductsAndLabelsToManufacturePlan


class AddOrderedProductsToManufacturePlanView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        serializer = SendToManufactureSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request = AddOrderedProductsToManufacturePlan.Request(serializer.validated_data)
        use_case = AddOrderedProductsToManufacturePlan(request)
        use_case.execute()
        return Response(status=201)


class AddOrderedProductsAndLabelsToManufacturePlanView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        request = AddOrderedProductsAndLabelsToManufacturePlan.Request(request.data)
        use_case = AddOrderedProductsAndLabelsToManufacturePlan(request)
        use_case.execute()
        return Response(status=201)
