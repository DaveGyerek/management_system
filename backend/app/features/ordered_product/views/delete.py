from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..use_cases.delete import DeleteOrderedProduct, DeleteOrderedProductsFromDeliveryNote


class DeleteOrderedProductView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        request = DeleteOrderedProduct.Request(kwargs['ordered_product_id'])
        use_case = DeleteOrderedProduct(request)
        use_case.execute()
        return Response(status=201)


class DeleteOrderedProductsFromDeliveryNoteView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        request = DeleteOrderedProductsFromDeliveryNote.Request(kwargs['delivery_note_id'])
        use_case = DeleteOrderedProductsFromDeliveryNote(request)
        use_case.execute()
        return HttpResponseRedirect(reverse('home'))

