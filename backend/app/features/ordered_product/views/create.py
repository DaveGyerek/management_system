from ..use_cases.create import CreateOrderedProducts


def create_ordered_products(order):
    request = CreateOrderedProducts.Request(order)
    use_case = CreateOrderedProducts(request)
    use_case.execute()
