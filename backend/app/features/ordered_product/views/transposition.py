from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ..use_cases.transposition import TranspositionOrderedProductsAndLabels


class TranspositionOrderedProductsView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        request = TranspositionOrderedProductsAndLabels.Request(request.data)
        use_case = TranspositionOrderedProductsAndLabels(request)
        use_case.execute()
        return Response(status=201)
