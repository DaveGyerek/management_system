from django.utils.translation import ugettext_lazy as _

from backend.custom_wagtail.views import IndexView


class FinishedProductIndexView(IndexView):
    def get_page_title(self):
        return _('Finished Products')

    def get_buttons_for_obj(self, obj):
        return []

    def get_template_names(self):
        return [
            'app/finished_product/index.html'
        ]
