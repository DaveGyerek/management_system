from django.conf.urls import url

from .views.delete_from_delivery_note import DeleteFromDeliveryNoteView
from .views.delete import DeleteOrderedProductView, DeleteOrderedProductsFromDeliveryNoteView
from .views.transposition import TranspositionOrderedProductsView
from .views.send_to_delivery_note import SendOrderedProductsToAnotherDeliveryNoteView, \
    SendOrderedProductsAndLabelsToDeliveryView
from .views.delete_from_manufacture_plan import DeleteOrderedProductsFromManufacturePlanView
from .views.set_to_finished import SetOrderedProductsAndLabelsToFinishedView
from .views.add_to_manufacture_plan import AddOrderedProductsToManufacturePlanView, \
    AddOrderedProductsAndLabelsToManufacturePlanView

urlpatterns = [
    url(r'^api/1/ordered-products/send-to-manufacture/$', AddOrderedProductsToManufacturePlanView.as_view(),
        name='send-to-manufacture'),
    url(r'^api/1/add-ordered-products-and-labels-to-manufacture-plan/$',
        AddOrderedProductsAndLabelsToManufacturePlanView.as_view(),
        name='add-ordered-products-and-labels-to-manufacture-plan'),
    url(r'^api/1/set-ordered-products-and-labels-to-finished/$',
        SetOrderedProductsAndLabelsToFinishedView.as_view(),
        name='set-ordered-products-and-labels-to-finished'),
    url(r'^api/1/ordered-products/delete/$', DeleteOrderedProductsFromManufacturePlanView.as_view(),
        name='delete-ordered-products-from-manufacture-plan'),

    url(r'^ordered-products/move-to-another-delivery-note/$',
        SendOrderedProductsToAnotherDeliveryNoteView.as_view(),
        name='move-to-another-delivery-note'),
    url(r'^api/1/send-ordered-products-and-labels-to-delivery/$',
        SendOrderedProductsAndLabelsToDeliveryView.as_view(),
        name='send-ordered-products-and-labels-to-delivery'),

    url(r'^api/1/transposition-ordered-products/$',
        TranspositionOrderedProductsView.as_view(),
        name='transposition-ordered-products'),

    url(r'^api/1/ordered-product/delete/(?P<ordered_product_id>\d+)[/]?$',
        DeleteOrderedProductView.as_view(),
        name='delete-ordered-product'),
    url(r'^api/1/ordered-product/delete-from-delivery-note/(?P<delivery_note_id>\d+)[/]?$',
        DeleteOrderedProductsFromDeliveryNoteView.as_view(),
        name='delete-ordered-products-from-delivery-note'),

    url(r'^ordered-products/delete-from-delivery-note/$',
        DeleteFromDeliveryNoteView.as_view(),
        name='delete-from-delivery-note'),
]
