from django.contrib import admin

from .models import OrderedProduct, OrderedProductLabel

admin.site.register([
    OrderedProduct,
    OrderedProductLabel,
])
