from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models, transaction
from django.utils.translation import ugettext as _
from model_utils import FieldTracker

from .constants import STATUSES, OPEN, LABEL_STATUSES, LABEL_OPEN, READY
from .views.save import save_ordered_product


class OrderedProduct(models.Model):
    """
    Represents the real objects which have to be manufactured for a
    :model:`app.Client` in a certain :model:`app.Order` .
    """

    class Meta:
        verbose_name = _('Ordered product')
        verbose_name_plural = _('Ordered products')
        ordering = [
            'product_order__product__name',
            'price',
        ]

    product_order = models.ForeignKey(
        'app.ProductOrder',
        verbose_name=_('Product order'),
        related_name='ordered_products',
        on_delete=models.CASCADE,
    )
    status = models.SmallIntegerField(
        verbose_name=_('State'),
        choices=STATUSES,
        default=OPEN,
    )
    price = models.DecimalField(
        verbose_name=_('Price'),
        max_digits=10,
        decimal_places=2,
        validators=[MinValueValidator(Decimal('0.01'))],
    )
    manufacture_plan = models.ForeignKey(
        'app.ManufacturePlan',
        verbose_name=_('Manufacture plan'),
        related_name='ordered_products',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    delivery_note = models.ForeignKey(
        'app.DeliveryNote',
        verbose_name=_('Delivery note'),
        related_name='ordered_products',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    tracker = FieldTracker()

    def __str__(self):
        return f'{self.product_order.order} - {self.product_order.product.name}'

    @transaction.atomic
    def save(self, *args, **kwargs):
        if self.pk:
            save_ordered_product(self)
        elif self.product_order.product.ready_made:
            self.status = READY
        super(OrderedProduct, self).save(*args, **kwargs)


class OrderedProductLabel(models.Model):
    """
    Related to :model:`app.OrderedProduct` and :model:`app.ProductLabel` generates the
    labels for every :model:`app.OrderedProduct` and can be treat as a separeta object.
    """

    class Meta:
        verbose_name = _('Ordered product label')
        verbose_name_plural = _('Ordered product labels')
        ordering = [
            'name',
        ]

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=100,
    )
    ordered_product = models.ForeignKey(
        'app.OrderedProduct',
        related_name='labels',
        on_delete=models.CASCADE,
    )
    status = models.SmallIntegerField(
        verbose_name=_('State'),
        choices=LABEL_STATUSES,
        default=LABEL_OPEN,
    )

    def __str__(self):
        return f'{self.ordered_product.product_order.product.name} - {self.name}'
