from datetime import datetime

from backend.app.features.manufacture_plan.models import ManufacturePlan
from ..constants import WAITING, LABEL_WAITING
from ..models import OrderedProduct, OrderedProductLabel
from ..use_cases.add_to_manufacture_plan import AddOrderedProductsAndLabelsToManufacturePlan
from ....tests.app_test import BaseTestWithOrder

_valid_data = {
    "ordered_products": [2, 6],
    "labels": [1],
    "manufacture_id": 1
}


def _execute_use_case(data):
    request = AddOrderedProductsAndLabelsToManufacturePlan.Request(data)
    use_case = AddOrderedProductsAndLabelsToManufacturePlan(request=request)
    return use_case.execute()


class TestProductOrdersSendToManufacturePlan(BaseTestWithOrder):
    def test_ordered_products_and_labels_send_to_manufacture_plan(self):
        ManufacturePlan.objects.create(identification_number='asd', date=datetime.now())
        _execute_use_case(_valid_data)
        ordered_products = OrderedProduct.objects.only('status')
        in_manufacture_count = ordered_products.filter(status=WAITING).count()
        assert in_manufacture_count == 3
        assert OrderedProductLabel.objects.get(pk=3).status == LABEL_WAITING
        assert OrderedProductLabel.objects.get(pk=4).status == LABEL_WAITING
