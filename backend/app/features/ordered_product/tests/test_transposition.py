from backend.app.features.client.models import Client
from backend.app.features.order.models import Order
from backend.app.features.ordered_product.models import OrderedProduct
from ..use_cases.transposition import TranspositionOrderedProductsAndLabels
from ....tests.app_test import BaseTestWithOrder

_valid_data = {
    "ordered_products": [1, 3],
    "labels": [],
    "order": 2
}
_valid_data_full = {
    "ordered_products": [1, 2, 3, 4, 5],
    "labels": [],
    "order": 2
}


def _execute_use_case(data):
    request = TranspositionOrderedProductsAndLabels.Request(data)
    use_case = TranspositionOrderedProductsAndLabels(request=request)
    return use_case.execute()


class TestProductOrdersTranspositioned(BaseTestWithOrder):
    def test_ordered_products_and_labels_transpositioned(self):
        client = Client.objects.first()
        old_order = Order.objects.first()
        order = Order.objects.create(client=client, billing_address="asdasd", delivery_address="asdasd")
        _execute_use_case(_valid_data)
        ordered_products = OrderedProduct.objects.all()
        assert ordered_products.get(pk=1).product_order.order == order
        assert ordered_products.get(pk=3).product_order.order == order
        assert ordered_products.get(pk=2).product_order.order == old_order

        assert ordered_products.get(pk=1).product_order.quantity == 2
        assert ordered_products.get(pk=2).product_order.quantity == 3

        assert old_order.product_orders.all().count() == 2
        assert order.product_orders.all().count() == 1

        assert ordered_products.filter(product_order__in=order.product_orders.all()).count() == 2
        assert ordered_products.filter(product_order__in=old_order.product_orders.all()).count() == 8

    def test_ordered_products_and_labels_transpositioned_with_full_product_order(self):
        client = Client.objects.first()
        old_order = Order.objects.first()
        order = Order.objects.create(client=client, billing_address="asdasd", delivery_address="asdasd")
        _execute_use_case(_valid_data_full)
        ordered_products = OrderedProduct.objects.all()
        new_product_order = ordered_products.get(pk=2).product_order
        assert old_order.product_orders.all().count() == 1
        assert order.product_orders.all().count() == 1
