from ...delivery_note.models import DeliveryNote
from ...order.models import ProductOrder, Order
from ..models import OrderedProduct
from ..use_cases.delete import DeleteOrderedProduct, DeleteOrderedProductsFromDeliveryNote
from ....tests.app_test import BaseTestWithOrder, BaseTestOrderAddedToDeliveryNote, \
    _execute_send_to_delivery_note

_send_to_delivery_note_data = {
    "ordered_products": [6, 7, 8, 9, 10],
    "labels": [],
    "delivery_id": 1
}


def _execute_use_case(_id):
    request = DeleteOrderedProduct.Request(_id)
    use_case = DeleteOrderedProduct(request)
    use_case.execute()


def _execute_delivery_use_case(_id):
    request = DeleteOrderedProductsFromDeliveryNote.Request(_id)
    use_case = DeleteOrderedProductsFromDeliveryNote(request)
    use_case.execute()


class TestDeleteOrderedProduct(BaseTestWithOrder):
    def test_delete_ordered_product(self):
        _id = 1
        ordered_product = OrderedProduct.objects.get(id=_id)
        product_order_id = ordered_product.product_order.id
        _execute_use_case(_id)
        assert ProductOrder.objects.get(id=product_order_id).quantity == 4
        assert OrderedProduct.objects.filter(id=_id).count() == 0

    def test_delete_all_ordered_product_in_po(self):
        ordered_product = OrderedProduct.objects.get(id=1)
        product_order_id = ordered_product.product_order.id
        _execute_use_case(1)
        _execute_use_case(2)
        _execute_use_case(3)
        _execute_use_case(4)
        _execute_use_case(5)
        assert ProductOrder.objects.filter(id=product_order_id).count() == 0
        assert Order.objects.count() == 1
        assert OrderedProduct.objects.count() == 5

    def test_delete_all_ordered_product_in_order(self):
        ordered_product = OrderedProduct.objects.get(id=1)
        product_order_id = ordered_product.product_order.id
        order_id = ordered_product.product_order.order.id
        for i in range(1, 11):
            _execute_use_case(i)
        assert ProductOrder.objects.filter(id=product_order_id).count() == 0
        assert Order.objects.filter(id=order_id).count() == 0


class TestDeleteOrderedProductsFromDeliveryNote(BaseTestOrderAddedToDeliveryNote):
    def test_delete_all_from_delivery_note(self):
        _id = 1
        ordered_product_count = OrderedProduct.objects.count()
        assert ProductOrder.objects.get(id=_id).ordered_products.all().count() == 5
        _execute_delivery_use_case(_id)
        assert ProductOrder.objects.filter(id=_id).count() == 0
        assert OrderedProduct.objects.count() == ordered_product_count - 5
        assert DeliveryNote.objects.filter(id=_id).count() == 0

    def test_delete_all_from_delivery_note_and_order(self):
        # add the remaining ordered products to the delivery note
        _execute_send_to_delivery_note(_send_to_delivery_note_data)

        _id = 1
        _execute_delivery_use_case(_id)
        assert Order.objects.count() == 0
        assert OrderedProduct.objects.count() == 0
