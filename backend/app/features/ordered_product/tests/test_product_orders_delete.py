import pytest

from ....tests.app_test import BaseTestOrderAddedManufacturePlan
from ...order.repository import OrderRepository
from ..models import OrderedProduct
from ..use_cases.delete_from_manufacture_plan import DeleteOrderedProductsFromManufacturePlan

_valid_data = {
    "id": 1,
    "product_orders":
        [
            {
                "id": "1",
                "quantity": 1
            },
            {
                "id": "2",
                "quantity": 2
            }
        ]
}


def _execute_use_case_for_delete(data):
    request = DeleteOrderedProductsFromManufacturePlan.Request(data=data)
    use_case = DeleteOrderedProductsFromManufacturePlan(request=request)
    return use_case.execute()


class TestProductOrdersDelete(BaseTestOrderAddedManufacturePlan):
    @pytest.mark.django_db
    def test_product_orders_delete(self):
        _execute_use_case_for_delete(_valid_data)
        ordered_products = OrderedProduct.objects.only('status')
        product_orders = OrderRepository().get_product_orders_with_open_quantity()
        ordered_products_count = ordered_products.count()
        assert product_orders[0].open_quantity == 4
        assert product_orders[1].open_quantity == 4
        assert ordered_products_count == 10
