from ..constants import READY, ON_DELIVERY_NOTE, LABEL_ON_DELIVERY_NOTE
from ..models import OrderedProduct, OrderedProductLabel
from ..use_cases.send_to_delivery_note import SentOrderedProductsAndLabelsToDelivery
from ...client.models import Client
from ...delivery_note.models import DeliveryNote
from ....tests.app_test import BaseTestOrderAddedManufacturePlan

_valid_data_with_label = {
    "ordered_product_id": 1,
    "label_id": 1
}

_valid_data_with_label_2 = {
    "ordered_product_id": 1,
    "label_id": 2
}

_valid_data_without_label = {
    "ordered_product_id": 1
}

_valid_group_data = {
    "ordered_products": [2, 6],
    "labels": [1],
    "delivery_id": 1
}


def _execute_group_use_case(data):
    request = SentOrderedProductsAndLabelsToDelivery.Request(data)
    use_case = SentOrderedProductsAndLabelsToDelivery(request=request)
    return use_case.execute()


def set_ordered_products_to_finished():
    return OrderedProduct.objects.update(status=READY)


def create_delivery_note():
    client = Client.objects.first()
    return DeliveryNote.objects.create(client=client, delivery_address="asd", voucher_number="ASD123")


class TestProductOrdersSendToDeliveryNoteByID(BaseTestOrderAddedManufacturePlan):
    def setUp(self):
        super().setUp()
        self.delivery_note = create_delivery_note()
        set_ordered_products_to_finished()

    def test_ordered_products_and_labels_send_to_delivery_note(self):
        _execute_group_use_case(_valid_group_data)
        ordered_products = OrderedProduct.objects.only('status')
        in_delivery_note_count = ordered_products.filter(status=ON_DELIVERY_NOTE).count()
        assert in_delivery_note_count == 3
        assert OrderedProductLabel.objects.get(pk=3).status == LABEL_ON_DELIVERY_NOTE
        assert OrderedProductLabel.objects.get(pk=4).status == LABEL_ON_DELIVERY_NOTE
