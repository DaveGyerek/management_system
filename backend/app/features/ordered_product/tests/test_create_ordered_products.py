import pytest
from django.utils import timezone

from backend.app.features.material.models import Material
from backend.app.features.order.models import Order
from backend.app.features.ordered_product.constants import READY, OPEN
from ....tests.app_test import BaseTestWithOrder, BaseTestWithOrderReadyMade
from ...order.repository import OrderRepository
from ..models import OrderedProduct
from ...manufacture_plan.models import ManufacturePlan
from ..use_cases.add_to_manufacture_plan import AddOrderedProductsToManufacturePlan

_valid_data = {
    "id": 1,
    "product_orders":
        [
            {
                "id": "1",
                "quantity": 1
            },
            {
                "id": "2",
                "quantity": 2
            }
        ]
}

_valid_data_with_new = {
    "product_orders":
        [
            {
                "id": "1",
                "quantity": 1
            },
            {
                "id": "2",
                "quantity": 2
            }
        ],
    "new_manufacture_plan": {
        "number": "456ASD",
        "date": "2018-4-20"
    }
}


def _create_manufacture_plan():
    ManufacturePlan.objects.create(identification_number="123", date=timezone.now())


def _execute_use_case(data):
    request = AddOrderedProductsToManufacturePlan.Request(data=data)
    use_case = AddOrderedProductsToManufacturePlan(request=request)
    return use_case.execute()


class TestCreateOrderedProducts(BaseTestWithOrder):
    def setUp(self):
        super().setUp()
        _create_manufacture_plan()

    @pytest.mark.django_db
    def test_create_ordered_products_on_order_creation(self):
        product_orders = OrderRepository().get_product_orders_with_open_quantity()
        p1_ordered_product = OrderedProduct.objects.filter(product_order__product__name='product1').first()
        assert p1_ordered_product.labels.count() == 2
        assert OrderedProduct.objects.count() == 10
        assert product_orders[0].open_quantity == 5
        assert product_orders[1].open_quantity == 5

    @pytest.mark.django_db
    def test_add_ordered_products_to_manufactured_plan(self):
        _execute_use_case(_valid_data)
        product_orders = OrderRepository().get_product_orders_with_open_quantity()
        assert OrderedProduct.objects.filter(status=OPEN).count() == 7
        assert product_orders[0].open_quantity == 4
        assert product_orders[1].open_quantity == 3

    @pytest.mark.django_db
    def test_add_ordered_products_to__new_manufacture_plan(self):
        _execute_use_case(_valid_data_with_new)
        product_orders = OrderRepository().get_product_orders_with_open_quantity()
        mp = ManufacturePlan.objects.get(identification_number='456ASD')
        assert OrderedProduct.objects.filter(status=OPEN).count() == 7
        assert product_orders[0].open_quantity == 4
        assert product_orders[1].open_quantity == 3
        assert mp.ordered_products.count() == 3


class TestCreateReadyMadeOrderedProducts(BaseTestWithOrderReadyMade):
    def test_ready_made_ordered_product_creation(self):
        assert OrderedProduct.objects.filter(status=READY).count() == 5
        material = Material.objects.first()
        assert material.quantity == -5

    def test_ready_made_order_deletion(self):
        Order.objects.first().delete()
        material = Material.objects.first()
        assert material.quantity == 0

    def test_ready_made_ordered_product_deletion(self):
        OrderedProduct.objects.first().delete()
        assert OrderedProduct.objects.filter(status=READY).count() == 4
        material = Material.objects.first()
        assert material.quantity == -4
