from unittest import TestCase

import pytest

from ..models import OrderedProduct
from ...material.models import Material, MaterialCategory
from ...order.models import Order, ProductOrder, ProductOrderPart
from ....tests.app_test import _create_products_and_client
from ..constants import READY, WAITING


def create_material():
    category = MaterialCategory.objects.create(name='a')
    return Material.objects.create(category=category, name='a', price=123)


@pytest.mark.django_db
class TestSaveOrderedProduct(TestCase):
    def setUp(self):
        product1, product2, client = _create_products_and_client()
        material = create_material()
        order = Order.objects.create(client=client, billing_address="asd", delivery_address="asd")
        po = ProductOrder.objects.create(order=order, product=product1, quantity=5, price=1000)
        ProductOrderPart.objects.create(product_order=po, name="a", material=material, material_quantity=2,
                                        price=200)

    def test_save_ordered_product(self):
        material = Material.objects.first()
        material.quantity = 10
        material.save()
        assert material.quantity == 10
        ordered_product = OrderedProduct.objects.first()
        ordered_product.status = READY
        ordered_product.save()
        material = Material.objects.first()
        assert material.quantity == 8
        ordered_product.status = WAITING
        ordered_product.save()
        material = Material.objects.first()
        assert material.quantity == 10
