import pytest

from ...client.models import Client
from ...delivery_note.models import DeliveryNote
from ..constants import READY, ON_DELIVERY_NOTE, LABEL_ON_DELIVERY_NOTE, LABEL_READY
from ..models import OrderedProduct
from ..use_cases.delete_from_delivery_note import DeleteFromDeliveryNote
from ..use_cases.send_to_delivery_note import SendToDeliveryNote, MoveToAnotherDeliveryNote
from ....tests.app_test import BaseTestOrderAddedManufacturePlan

_valid_data = {
    "id": 1,
    "product_orders":
        [
            {
                "id": "1",
                "quantity": 1
            },
            {
                "id": "2",
                "quantity": 2
            }
        ]
}

_move_to_another_data = {
    "origin": 1,
    "new": 2,
    "product_orders":
        [
            {
                "id": "2",
                "quantity": 2
            }
        ]
}

_move_to_duplication_data = {
    "origin": 1,
    "duplicate": True,
    "product_orders":
        [
            {
                "id": "2",
                "quantity": 2
            }
        ]
}

_delete_from_delivery_note_data = {
    "id": 1,
    "product_orders":
        [
            {
                "id": 1,
                "quantity": 1
            }
        ]
}


def _execute_use_case(data):
    request = SendToDeliveryNote.Request(data=data)
    use_case = SendToDeliveryNote(request=request)
    return use_case.execute()


def _execute_move_use_case(data):
    request = MoveToAnotherDeliveryNote.Request(data=data)
    use_case = MoveToAnotherDeliveryNote(request=request)
    return use_case.execute()


def _execute_delete_use_case(data):
    request = DeleteFromDeliveryNote.Request(data=data)
    use_case = DeleteFromDeliveryNote(request=request)
    return use_case.execute()


def set_ordered_products_to_finished():
    return OrderedProduct.objects.update(status=READY)


def create_delivery_note():
    client = Client.objects.first()
    return DeliveryNote.objects.create(client=client, delivery_address="asd", voucher_number="ASD123")


class TestProductOrdersSendToDeliveryNote(BaseTestOrderAddedManufacturePlan):
    def setUp(self):
        super().setUp()
        self.delivery_note = create_delivery_note()
        self.delivery_note_2 = create_delivery_note()
        set_ordered_products_to_finished()

    def test_product_orders_send_to_delivery_note_valid_data(self):
        _execute_use_case(_valid_data)
        ordered_products = OrderedProduct.objects.only('status')
        in_delivery_ordered_products = ordered_products.filter(status=ON_DELIVERY_NOTE)
        finished_count = ordered_products.filter(status=READY).count()
        in_delivery_note_count = in_delivery_ordered_products.count()
        assert in_delivery_note_count == self.delivery_note.ordered_products.count()
        assert finished_count == ordered_products.count() - in_delivery_note_count
        assert in_delivery_note_count == 3
        assert in_delivery_ordered_products.first().labels.all().first().status == LABEL_ON_DELIVERY_NOTE

    def test_product_orders_send_to_another_delivery_note(self):
        _execute_use_case(_valid_data)
        _execute_move_use_case(_move_to_another_data)
        ordered_products = OrderedProduct.objects.only('status')
        in_delivery_note_count = ordered_products.filter(status=ON_DELIVERY_NOTE).count()
        assert self.delivery_note.ordered_products.count() == 1
        assert self.delivery_note_2.ordered_products.count() == 2
        assert in_delivery_note_count == 3

    def test_product_orders_send_to_duplicated_delivery_note(self):
        _execute_use_case(_valid_data)
        _execute_move_use_case(_move_to_duplication_data)
        ordered_products = OrderedProduct.objects.only('status')
        in_delivery_note_count = ordered_products.filter(status=ON_DELIVERY_NOTE).count()
        duplicated_dn = DeliveryNote.objects.get(pk=3)
        assert self.delivery_note.client == duplicated_dn.client
        assert self.delivery_note.voucher_number + "/A" == duplicated_dn.voucher_number
        assert self.delivery_note.delivery_address == duplicated_dn.delivery_address
        assert self.delivery_note.ordered_products.count() == 1
        assert duplicated_dn.ordered_products.count() == 2
        assert in_delivery_note_count == 3

    def test_product_orders_delete_from_delivery_note(self):
        _execute_use_case(_valid_data)
        _execute_delete_use_case(_delete_from_delivery_note_data)
        ordered_products = OrderedProduct.objects.only('status')
        in_delivery_note_count = ordered_products.filter(status=ON_DELIVERY_NOTE).count()
        ready_ordered_products = ordered_products.filter(status=READY)
        finished_count = ready_ordered_products.count()
        assert in_delivery_note_count == 2
        assert finished_count == ordered_products.count() - in_delivery_note_count
        assert ready_ordered_products.first().labels.all().first().status == LABEL_READY
