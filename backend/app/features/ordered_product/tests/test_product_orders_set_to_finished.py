import pytest

from ..constants import READY, LABEL_READY
from ..models import OrderedProduct, OrderedProductLabel
from ..use_cases.set_to_finished import SetOrderedProductsAndLabelsToFinished
from ....tests.app_test import BaseTestOrderAddedManufacturePlan

_valid_data = {
    "id": 1,
    "product_orders":
        [
            {
                "id": "1",
                "quantity": 1
            },
            {
                "id": "2",
                "quantity": 2
            }
        ]
}

_valid_group_data = {
    "ordered_products": [2, 6],
    "labels": [1, 2],
}


def _execute_group_use_case(data):
    request = SetOrderedProductsAndLabelsToFinished.Request(data=data)
    use_case = SetOrderedProductsAndLabelsToFinished(request=request)
    return use_case.execute()


class TestProductOrdersSetToFinished(BaseTestOrderAddedManufacturePlan):

    def test_product_orders_and_labels_set_to_finished(self):
        _execute_group_use_case(_valid_group_data)
        ordered_products = OrderedProduct.objects.only('status')
        ready_ordered_products = ordered_products.filter(status=READY)
        finished_count = ready_ordered_products.count()
        assert finished_count == 3
        assert OrderedProductLabel.objects.get(pk=2).status == LABEL_READY
        assert OrderedProductLabel.objects.get(pk=3).status == LABEL_READY
