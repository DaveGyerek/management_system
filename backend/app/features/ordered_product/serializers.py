from rest_framework import serializers

from backend.app.features.order.models import ProductOrder
from backend.app.features.ordered_product.constants import OPEN
from .models import (
    OrderedProductLabel,
    OrderedProduct,
)


class OrderedProductLabelSerializer(serializers.ModelSerializer):
    parent = serializers.CharField(source='ordered_product.id')

    class Meta:
        model = OrderedProductLabel
        fields = [
            'parent',
            'id',
            'name',
            'status'
        ]


class OrderedProductSerializer(serializers.ModelSerializer):
    labels = OrderedProductLabelSerializer(many=True)
    name = serializers.CharField(source='product_order.product.name')

    class Meta:
        model = OrderedProduct
        fields = [
            'id',
            'name',
            'status',
            'labels'
        ]


class SendToManufactureSerializer(serializers.Serializer):
    class ProductOrdersSerializer(serializers.Serializer):
        id = serializers.CharField(required=True)
        quantity = serializers.IntegerField(required=True)

    class NewManufacturePlanSerializer(serializers.Serializer):
        number = serializers.CharField(required=True)
        date = serializers.CharField(required=True)

    id = serializers.CharField(required=False)
    product_orders = ProductOrdersSerializer(many=True)
    new_manufacture_plan = NewManufacturePlanSerializer(required=False)


class OrderedProductLabelForManufacturePlanSerializer(serializers.ModelSerializer):
    parent = serializers.CharField(source='ordered_product.id')

    class Meta:
        model = OrderedProductLabel
        fields = [
            'parent',
            'id',
            'name',
            'status',
        ]


class OrderedProductForManufacturePlanSerializer(serializers.ModelSerializer):
    labels = OrderedProductLabelForManufacturePlanSerializer(many=True)
    name = serializers.CharField(source='product_order.product.name')

    class Meta:
        model = OrderedProduct
        fields = [
            'id',
            'name',
            'status',
            'labels'
        ]


class SendToDeliveryNoteSerializer(SendToManufactureSerializer):
    pass


class SendToAnotherDeliveryNoteSerializer(serializers.Serializer):
    product_orders = SendToManufactureSerializer().ProductOrdersSerializer(many=True)
    origin = serializers.CharField(required=True)
    new = serializers.CharField(required=False)
    duplicate = serializers.BooleanField(required=False)

    def validate(self, attrs):
        super().validate(attrs)
        if 'new' not in attrs and 'duplicate' not in attrs:
            raise serializers.ValidationError("Either new or duplicate had been set")
        return attrs


class DeleteFromDeliveryNoteSerializer(SendToManufactureSerializer):
    pass


class ProductOrderForFinishedProductsSerializer(serializers.ModelSerializer):
    ordered_products = serializers.SerializerMethodField()

    class Meta:
        model = ProductOrder
        fields = [
            'id',
            'ordered_products'
        ]

    def get_ordered_products(self, product_order):
        items = sorted(set(product_order.ordered_products.all()), key=lambda x: x.id)
        serializer = OrderedProductForManufacturePlanSerializer(instance=items, many=True)
        return serializer.data


class ProductOrderForOpenOrderSerializer(ProductOrderForFinishedProductsSerializer):
    def get_ordered_products(self, product_order):
        items = set(filter(lambda x: x.status == OPEN, product_order.ordered_products.all()))
        serializer = OrderedProductForManufacturePlanSerializer(instance=items, many=True)
        return serializer.data
