from django.utils.translation import ugettext as _

OPEN = 0
WAITING = 1
READY = 2
ON_DELIVERY_NOTE = 3
DELIVERED = 4
STATUSES = (
    (OPEN, _('Open')),
    (WAITING, _('Waiting for manufacture')),
    (READY, _('Ready')),
    (ON_DELIVERY_NOTE, _('On delivery note')),
    (DELIVERED, _('Delivered')),
)

LABEL_OPEN = 0
LABEL_WAITING = 1
LABEL_READY = 2
LABEL_ON_DELIVERY_NOTE = 3
LABEL_DELIVERED = 4

LABEL_STATUSES = {
    (LABEL_OPEN, _('Open')),
    (LABEL_WAITING, _('Waiting for manufacture')),
    (LABEL_READY, _('Ready')),
    (LABEL_ON_DELIVERY_NOTE, _('On delivery note')),
    (LABEL_DELIVERED, _('Delivered')),
}
