from django.db.models import QuerySet, Q
from django.shortcuts import get_object_or_404

from .constants import WAITING, LABEL_WAITING, LABEL_READY, READY, LABEL_OPEN, OPEN, ON_DELIVERY_NOTE, \
    LABEL_ON_DELIVERY_NOTE
from .models import OrderedProduct, OrderedProductLabel


class OrderedProductRepository:
    def create_ordered_product(self, **kwargs):
        return OrderedProduct.objects.create(**kwargs)

    def create_bulk_ordered_product_label(self, ordered_product_labels: []):
        return OrderedProductLabel.objects.bulk_create(ordered_product_labels)

    def create_label_object_for_bulk(self, **kwargs):
        return OrderedProductLabel(**kwargs)

    def get_ordered_products(self, ids: [], label_ids: [] = None):
        queryset = OrderedProduct.objects.all()
        if label_ids:
            label_queryset = OrderedProductLabel.objects.filter(pk__in=label_ids)
            return queryset.filter(Q(pk__in=ids) | Q(labels__in=label_queryset))

        return queryset.filter(Q(pk__in=ids))

    def get_open_ordered_products_for_product_order(self, product_order):
        queryset = product_order.ordered_products.filter(status=OPEN)
        return queryset

    def get_ordered_products_for_manufacture_plan(self, mp_id):
        queryset = OrderedProduct.objects.filter(manufacture_plan__id=mp_id,
                                                 status__lte=WAITING) \
            .prefetch_related('product_order__parts', 'product_order__order') \
            .only('id', 'product_order__product__name') \
            .order_by('product_order__product__product_type', 'product_order__product__name')
        return queryset

    def get_all_labels_for_ordered_product_ids(self, ids: []):
        labels = OrderedProductLabel.objects.filter(ordered_product__id__in=ids)
        return labels

    def get_ordered_product_label(self, label_id):
        queryset = OrderedProductLabel.objects.all()
        return get_object_or_404(queryset, id=label_id)

    def update_ordered_product_labels_for_ordered_product(self, ordered_products: QuerySet, **kwargs):
        labels = OrderedProductLabel.objects.filter(ordered_product__in=ordered_products)
        if labels.exists():
            labels.update(**kwargs)

    def send_ordered_products_to_manufacture_plan(self, ids: [], mp):
        ordered_products = OrderedProduct.objects.filter(id__in=ids)
        self.update_ordered_product_labels_for_ordered_product(ordered_products, status=LABEL_WAITING)
        for ordered_product in ordered_products.iterator():
            ordered_product.status = WAITING
            ordered_product.delivery_note = None
            ordered_product.manufacture_plan = mp
            ordered_product.save(update_fields=['status', 'delivery_note', 'manufacture_plan'])

    def get_ordered_product_labels_by_id(self, label_ids: []):
        queryset = OrderedProductLabel.objects.all().filter(id__in=label_ids) \
            .prefetch_related('ordered_product')
        return queryset

    def set_ordered_products_to_finished(self, ordered_products: QuerySet):
        labels = OrderedProductLabel.objects.filter(ordered_product__in=ordered_products)
        if labels.exists():
            labels.update(status=LABEL_READY)
        for ordered_product in ordered_products.iterator():
            ordered_product.status = READY
            ordered_product.delivery_note = None
            ordered_product.save(update_fields=['status', 'delivery_note'])

    def set_ordered_products_to_finished_by_id(self, ids: []):
        ordered_products = OrderedProduct.objects.filter(id__in=ids)
        self.set_ordered_products_to_finished(ordered_products)

    def update_ordered_products_to_open(self, ordered_products: QuerySet):
        labels = OrderedProductLabel.objects.filter(ordered_product__in=ordered_products)
        if labels.exists():
            labels.update(status=LABEL_OPEN)
        ordered_products.update(manufacture_plan=None, status=OPEN)

    def send_ordered_products_to_delivery_by_ids(self, ids: [], dn):
        ordered_products = OrderedProduct.objects.filter(id__in=ids)
        for ordered_product in ordered_products.iterator():
            ordered_product.status = ON_DELIVERY_NOTE
            ordered_product.delivery_note = dn
            ordered_product.save(update_fields=['status', 'delivery_note'])
        self.update_ordered_product_labels_for_ordered_product(ordered_products, status=LABEL_ON_DELIVERY_NOTE)

    def get_all_labels_for_ordered_products(self, ordered_products):
        labels = OrderedProductLabel.objects.filter(ordered_product__in=ordered_products).only('name', 'id')
        return labels
