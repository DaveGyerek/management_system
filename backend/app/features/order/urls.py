from django.conf.urls import url

from .views.details import (
    OrderDetailsView,
    OrderDetailProductDetailsApiView,
    OpenOrderProductDetailsApiView)

urlpatterns = [
    url(r'^order/(?P<pk>\d+)/$', OrderDetailsView.as_view(), name='order-details'),

    url(r'^api/1/order/(?P<order_id>\d+)/ordered-product/(?P<po_id>\d+)/$',
        OrderDetailProductDetailsApiView.as_view(),
        name='order-detail-product-order-data'),

    url(r'^api/1/open-order/ordered-product/(?P<po_id>\d+)/$',
        OpenOrderProductDetailsApiView.as_view(),
        name='open-order-product-order-data'),
]
