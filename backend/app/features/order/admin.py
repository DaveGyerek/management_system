from django.contrib import admin

from .models import Order, ProductOrder, ProductOrderPart

admin.site.register([
    Order,
    ProductOrder,
    ProductOrderPart,
])
