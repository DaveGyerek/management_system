from django.db import models
from django.db.models import Sum, Prefetch, Q, Count
from django.db.models.expressions import RawSQL, Case, When, F
from django.shortcuts import get_object_or_404

from backend.app.models import Order, ProductOrder
from ..ordered_product.constants import (
    OPEN,
    LABEL_ON_DELIVERY_NOTE,
    ON_DELIVERY_NOTE,
    LABEL_WAITING,
    WAITING,
    READY, LABEL_READY, LABEL_OPEN)
from ..ordered_product.models import OrderedProduct, OrderedProductLabel


class OrderRepository:
    def get_order(self, order_id):
        queryset = Order.objects.all()
        return get_object_or_404(queryset, id=order_id)

    def get_product_order(self, po_id):
        queryset = ProductOrder.objects.prefetch_related('ordered_products')
        po = get_object_or_404(queryset, id=po_id)
        return po

    def get_product_orders(self, ids=None):
        if not ids:
            return ProductOrder.objects.prefetch_related('ordered_products')
        return ProductOrder.objects.prefetch_related('ordered_products').filter(pk__in=ids)

    def get_open_ordered_products_for_product_order(self, product_order):
        queryset = product_order.ordered_products.filter(status=OPEN)
        return queryset

    def get_orders_with_sum_price(self):
        queryset = Order.objects.prefetch_related('product_orders', 'client')
        queryset = queryset.annotate(sum_price=Sum(
            RawSQL("""(SELECT
                                                SUM(price * quantity)
                                                FROM app_productorder
                                                WHERE order_id = app_order.id)""",
                   params=[])))
        return queryset

    def get_order_for_details(self, order_id):
        queryset = Order.objects.prefetch_related(
            'client',
            'store',
            'product_orders',
            'product_orders__product',
            'product_orders__parts',
            'product_orders__parts__material',
        )
        order = get_object_or_404(queryset, id=order_id)
        return order

    def get_product_orders_with_open_quantity(self, order: Order = None):
        if not order:
            return ProductOrder.objects.annotate(
                open_quantity=Sum(Case(
                    When(ordered_products__status=OPEN, then=1),
                    default=0, output_field=models.IntegerField()))) \
                .only('id', 'product__name', 'quantity', 'price', 'note')
        return order.product_orders.annotate(
            open_quantity=Sum(Case(
                When(ordered_products__status=OPEN, then=1),
                default=0, output_field=models.IntegerField()))) \
            .only(
            'id', 'product__name', 'quantity', 'price', 'note')

    def get_product_order_details_for_manufacture_plan(self, mp_id, po_id):
        ordered_products = OrderedProduct.objects.filter(manufacture_plan__id=mp_id)
        return ProductOrder.objects.prefetch_related(Prefetch('ordered_products', queryset=ordered_products)).get(
            id=po_id)

    def get_in_work_ordered_products_for_manufacture_plan(self, po, mp):
        queryset = po.ordered_products.all().filter(manufacture_plan=mp, status=WAITING)
        return queryset

    def get_product_order_details_for_order(self, order_id, po_id):
        ordered_products = OrderedProduct.objects.filter(product_order__order__id=order_id)
        return ProductOrder.objects.prefetch_related(Prefetch('ordered_products', queryset=ordered_products)).get(
            id=po_id)

    def get_simple_orders(self):
        return Order.objects.only('id', 'client__name', 'delivery_address', 'delivery_timestamp') \
            .order_by('-delivery_timestamp')

    def get_product_order_details_for_delivery_note(self, dn_id, po_id):
        ordered_products = OrderedProduct.objects.filter(delivery_note__id=dn_id)
        return ProductOrder.objects.prefetch_related(Prefetch('ordered_products', queryset=ordered_products)).get(
            id=po_id)

    def get_ordered_products_groups_for_delivery_note(self, dn):
        ordered_products = OrderedProduct.objects.filter(delivery_note=dn)
        not_in_delivery_labels = OrderedProductLabel.objects.filter(status__lt=LABEL_ON_DELIVERY_NOTE).values('id')
        not_in_delivery_ordered_products = ordered_products.filter(
            Q(status__lt=ON_DELIVERY_NOTE) | Q(labels__id__in=not_in_delivery_labels)).values('id')

        queryset = ProductOrder.objects \
            .filter(ordered_products__in=ordered_products) \
            .annotate(
            not_in_delivery_count=Count(Case(When(ordered_products__id__in=not_in_delivery_ordered_products, then=1))),
            ready_count=RawSQL('''(SELECT
                        COUNT(app_orderedproduct.id)
                        FROM app_orderedproduct
                        WHERE app_orderedproduct.product_order_id = app_productorder.id 
                        AND app_orderedproduct.status = 2)''', params=[]),
            delivery_count=RawSQL('''(SELECT
                        COUNT(app_orderedproduct.id)
                        FROM app_orderedproduct
                        WHERE app_orderedproduct.product_order_id = app_productorder.id 
                        AND app_orderedproduct.status = 3 
                        AND app_orderedproduct.delivery_note_id = %s)''', params=[dn.id])) \
            .prefetch_related(Prefetch('ordered_products', queryset=ordered_products)).distinct()

        return queryset

    def get_ordered_product_groups_for_manufacture_plan(self, mp):
        not_ready_labels = OrderedProductLabel.objects.filter(status=LABEL_WAITING).only('id')
        ordered_products = OrderedProduct.objects.filter(manufacture_plan=mp) \
            .only('id', 'status', 'product_order__product__name').prefetch_related('labels', 'product_order__parts')
        not_ready_ordered_products = ordered_products.filter(Q(labels__in=not_ready_labels) | Q(status=WAITING)) \
            .only('id')
        manufactured_queryset = ordered_products.filter(Q(manufacture_plan=mp) & ~Q(status=WAITING) & ~Q(status=OPEN)) \
            .only('id')

        queryset = ProductOrder.objects.filter(ordered_products__in=ordered_products) \
            .annotate(manufactured_count=Count(Case(When(ordered_products__in=manufactured_queryset, then=1))),
                      ordered_product_count=Count('ordered_products'),
                      not_ready_count=Count(Case(When(ordered_products__in=not_ready_ordered_products, then=1)))) \
            .prefetch_related(
            Prefetch('ordered_products', queryset=ordered_products), 'parts') \
            .only('id', 'order', 'product__name',
                  'order__client__name', 'note')
        return queryset

    def get_ordered_product_groups_for_manufacture_plan_pdf(self, mp):
        queryset = self.get_ordered_product_groups_for_manufacture_plan(mp)
        queryset = queryset.annotate(in_progress_count=F('ordered_product_count') - F('manufactured_count')) \
            .filter(in_progress_count__gt=0)
        return queryset

    def get_ordered_product_groups_for_finished_products_admin(self):
        not_ready_labels = OrderedProductLabel.objects.filter(status=LABEL_WAITING).values('id')
        not_ready_ordered_products = OrderedProduct.objects \
            .filter(Q(labels__id__in=not_ready_labels) | Q(status=WAITING)).values('id')
        ready_labels = OrderedProductLabel.objects.filter(status=LABEL_READY).only('id')
        finished_queryset = OrderedProduct.objects \
            .filter(Q(status=READY) | Q(labels__in=ready_labels)).values('id')
        queryset = ProductOrder.objects.filter(ordered_products__id__in=finished_queryset) \
            .annotate(finished_count=Count(Case(When(ordered_products__id__in=finished_queryset, then=1))),
                      not_ready_count=Count(Case(When(ordered_products__id__in=not_ready_ordered_products, then=1))))
        return queryset

    def get_product_orders_for_open_orders(self):
        queryset = ProductOrder.objects.prefetch_related(
            'parts',
            'parts__material',
            'parts__material__category',
            'ordered_products',
            'order',
            'order__client',
            'order__store',
            'product',
        )
        queryset = queryset.filter(ordered_products__status=OPEN).annotate(
            open_quantity=Count('ordered_products'))
        return queryset.filter(open_quantity__gt=0)

    def get_product_order_details_for_open_orders(self, po_id):
        open_labels = OrderedProductLabel.objects.filter(status=LABEL_OPEN) \
            .values('id')
        ordered_products = OrderedProduct.objects.filter(
            Q(status=OPEN) |
            Q(labels__id__in=open_labels)
        )
        return ProductOrder.objects \
            .prefetch_related(Prefetch('ordered_products', queryset=ordered_products)) \
            .get(id=po_id)
