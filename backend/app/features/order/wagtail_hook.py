from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.templatetags.static import static
from wagtail.admin.edit_handlers import MultiFieldPanel, FieldRowPanel, FieldPanel, InlinePanel
from wagtail.contrib.modeladmin.options import modeladmin_register

from .repository import OrderRepository
from .views.index import OpenOrderIndexView
from .views.create import CreateOrderView
from .views.edit import EditOrderView
from backend.app.models import Order, ProductOrder
from backend.app.utils.helpers import UrlHelper
from backend.custom_wagtail.modeladmin import ModelAdmin, ModelAdminGroup

ProductOrder.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('product', classname='col6'),
            FieldPanel('quantity', classname='col6'),
        )),
    ), _('Product')),
]

Order.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('client', classname="col6"),
            FieldPanel('store', classname="col6"),
            FieldPanel('billing_address', classname="col6"),
            FieldPanel('delivery_address', classname="col6"),
            FieldPanel('order_timestamp', classname="col6"),
            FieldPanel('delivery_timestamp', classname="col6"),
            FieldPanel('note', classname="col12",
                       widget=forms.Textarea(attrs={'rows': 5})),
        )),
    ), _('Client'), classname='collapsible'),
    MultiFieldPanel((
        InlinePanel('product_orders', classname='react-root'),
    ), _('Products'), classname="full order-root"),
]


class OrderAdmin(ModelAdmin):
    ORDER_FORM_DEV_URL = 'http://localhost:3000/static/js/bundle.js'
    # _order_js = ORDER_FORM_DEV_URL
    _order_js = static('app/scripts/order/create.min.js')

    # _order_js = get_resource(ORDER_FORM_DEV_URL, static('app/scripts/order/create.min.js'))

    form_view_extra_js = [_order_js, ]
    form_view_extra_css = [] if _order_js == ORDER_FORM_DEV_URL else [static('app/style/order/create.min.css')]

    model = Order
    menu_icon = 'fa-shopping-cart'

    list_display = ('client', 'delivery_timestamp', 'order_timestamp', 'sum_price', 'note')
    list_filter = ('delivery_timestamp',)
    search_fields = ('client__name',)

    row_as_link = 'order-details'

    create_view_class = CreateOrderView
    edit_view_class = EditOrderView

    def sum_price(self, obj):
        if obj.sum_price:
            return '{0:n} Ft'.format(int(obj.sum_price))
        return '-'

    sum_price.short_description = _('Sum price')
    sum_price.admin_order_field = 'sum_price'

    def get_queryset(self, request):
        repository = OrderRepository()
        qs = repository.get_orders_with_sum_price()
        return qs


class OpenOrderAdmin(ModelAdmin):
    model = ProductOrder
    menu_label = _('Open orders')
    menu_icon = 'fa-calendar'

    url_helper_class = UrlHelper(model, "open-order")

    list_display = (
        'checkbox',
        'product__name',
        'open_quantity',
        'product__price',
        'note',
        'order__client',
        'order__store',
        'order__timestamp',
    )

    search_fields = (
        'order__client__name',
        'product__name',
        'order__store__name',
    )
    index_view_class = OpenOrderIndexView

    index_template_name = 'app/order/open_index.html'

    ordering = ['-order__order_timestamp']

    def product__name(self, obj):
        return obj.product.name

    product__name.short_description = _('Name')
    product__name.admin_order_field = 'product__name'

    def open_quantity(self, obj):
        return obj.open_quantity

    open_quantity.short_description = _('Quantity')
    open_quantity.admin_order_field = 'open_quantity'

    def product__price(self, obj):
        return '{0:n} Ft'.format(float(obj.price))

    product__price.short_description = _('Price')
    product__price.admin_order_field = 'price'

    def order__client(self, obj):
        return obj.order.client

    order__client.short_description = _('Client')
    order__client.admin_order_field = 'order__client__name'

    def order__store(self, obj):
        if obj.order.store:
            return obj.order.store.name
        return _('Not specified')

    order__store.short_description = _('Store')
    order__store.admin_order_field = 'order__store__name'

    def order__timestamp(self, obj):
        return obj.order.order_timestamp

    order__timestamp.short_description = _('Order timestamp')
    order__timestamp.admin_order_field = 'order__order_timestamp'

    def checkbox(self, obj):
        return format_html(
            '<input class="send-checkbox" type="checkbox" id="{}"',
            obj.id
        )

    checkbox.short_description = format_html(
        '<input class="send-checkbox" type="checkbox" id="select-all"'
    )

    def get_queryset(self, request):
        return OrderRepository().get_product_orders_for_open_orders()

    def get_extra_attrs_for_row(self, obj, context):
        style = 'clickable-row'
        return {
            'class': style,
        }


class OrderAdminGroup(ModelAdminGroup):
    menu_icon = 'fa-shopping-cart'
    menu_label = _('Orders')
    items = [OpenOrderAdmin, OrderAdmin]


modeladmin_register(OrderAdminGroup)
