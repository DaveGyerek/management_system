from django.utils.translation import ugettext as _

CONDITIONAL = 0
ACCEPTED = 1
DONE = 2
STATUSES = (
    (CONDITIONAL, _('Conditional')),
    (ACCEPTED, _('Accepted')),
    (DONE, _('Done')),
)
