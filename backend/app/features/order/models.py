from decimal import Decimal
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils import timezone
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from django.utils.translation import gettext_lazy as _
from wagtail.core.models import Orderable

from ..ordered_product.views.create import create_ordered_products
from .constants import STATUSES, CONDITIONAL


def delivery_next_week():
    return timezone.now() + timezone.timedelta(weeks=1)


class Order(ClusterableModel):
    """
    Represent an order for a :model:`app.Client` and one of its
    :model:`app.Store` .
    This objects can be built up by various :model:`app.ProductOrder` -s.
    """

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')
        ordering = [
            '-delivery_timestamp',
        ]

    client = models.ForeignKey(
        'app.Client',
        verbose_name=_('Client'),
        on_delete=models.PROTECT,
        related_name='orders',
    )
    store = models.ForeignKey(
        'app.Store',
        verbose_name=_('Store'),
        on_delete=models.PROTECT,
        related_name='orders',
        blank=True,
        null=True,
    )
    billing_address = models.CharField(
        verbose_name=_('Billing address'),
        max_length=300,
    )
    delivery_address = models.CharField(
        verbose_name=_('Delivery address'),
        max_length=300,
    )
    order_timestamp = models.DateTimeField(
        verbose_name=_('Time of order'),
        default=timezone.now,
    )
    delivery_timestamp = models.DateField(
        verbose_name=_('Time of delivery'),
        default=delivery_next_week,
    )
    status = models.SmallIntegerField(
        verbose_name=_('State'),
        choices=STATUSES,
        default=CONDITIONAL,
    )
    note = models.TextField(
        verbose_name=_('Note'),
        blank=True,
    )

    def __str__(self):
        return f'{self.client} ({self.delivery_timestamp})'


class ProductOrder(ClusterableModel, Orderable):
    """
    Connects the :model:`app.Order` with :model:`app.Product` .
    We can add multiple of there objects to on :model:`app.Order`
    to store what the client ordered.
    """

    class Meta:
        verbose_name = _('Product order')
        verbose_name_plural = _('Product orders')

    order = ParentalKey(
        'app.Order',
        verbose_name=_('Order'),
        related_name='product_orders',
        on_delete=models.CASCADE,
    )
    product = models.ForeignKey(
        'app.Product',
        verbose_name=_('Product'),
        on_delete=models.PROTECT,
    )
    quantity = models.PositiveIntegerField(
        verbose_name=_('Quantity'),
        default=1,
    )
    price = models.DecimalField(
        verbose_name=_('Price'),
        max_digits=10,
        decimal_places=2,
        validators=[MinValueValidator(Decimal('0'))],
    )
    note = models.CharField(
        verbose_name=_('Note'),
        blank=True,
        max_length=1000,
        null=True,
    )

    def __str__(self):
        return f'{self.order} - {self.product}'

    def save(self, **kwargs):
        created_now = not self.pk
        super(ProductOrder, self).save(**kwargs)
        if created_now:
            create_ordered_products(self)


class ProductOrderPart(Orderable):
    """
    Related to :model:`app.ProductOrder` these objects customizable define
    the :models:`app.Material` and the quantity for each.
    """

    class Meta:
        verbose_name = _('Product order part')
        verbose_name_plural = _('Product order parts')

    product_order = ParentalKey(
        'app.ProductOrder',
        verbose_name=_('Product order'),
        related_name='parts',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=100,
    )
    short_name = models.CharField(
        verbose_name=_('Short name'),
        max_length=100,
        blank=True,
    )
    material = models.ForeignKey(
        'app.Material',
        verbose_name=_('Material'),
        related_name='product_order_parts',
        on_delete=models.CASCADE,
    )
    material_quantity = models.FloatField(
        verbose_name=_('Material quantity'),
        default=1,
    )
    price = models.DecimalField(
        verbose_name=_('Price of unit (gross)'),
        max_digits=10,
        decimal_places=2,
    )
    net_price = models.DecimalField(
        verbose_name=_('Price of unit'),
        max_digits=10,
        decimal_places=4,
        default=1,
    )
    vat = models.PositiveIntegerField(
        verbose_name=_('VAT'),
        validators=[MaxValueValidator(100)],
        default=27,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f'{self.product_order} - {self.name}'
