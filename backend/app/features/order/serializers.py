from django.db.models import Q
from rest_framework import serializers

from ..ordered_product.constants import OPEN
from ..ordered_product.serializers import OrderedProductSerializer, OrderedProductForManufacturePlanSerializer
from .models import Order, ProductOrder, ProductOrderPart


class SimpleOrderSerializer(serializers.ModelSerializer):
    client = serializers.CharField(source='client.name')

    class Meta:
        model = Order
        fields = [
            'id',
            'client',
            'delivery_address',
            'delivery_timestamp'
        ]


class ProductOrderPartSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductOrderPart
        fields = [
            'id',
            'name',
            'material',
            'material_quantity',
            'price',
            'net_price',
            'vat',
            'sort_order'
        ]


class ProductOrderSerializer(serializers.ModelSerializer):
    parts = ProductOrderPartSerializer(many=True)

    class Meta:
        model = ProductOrder
        fields = [
            'id',
            'product',
            'quantity',
            'price',
            'parts',
            'note',
        ]


class ProductOrderEditSerializer(ProductOrderSerializer):
    quantity = serializers.SerializerMethodField()

    def get_quantity(self, product_order):
        not_open_quantity = product_order.ordered_products.all().filter(~Q(status=OPEN)).count()
        return product_order.quantity - not_open_quantity


class OrderSerializer(serializers.ModelSerializer):
    product_orders = ProductOrderSerializer(many=True)

    class Meta:
        model = Order
        fields = [
            'id',
            'client',
            'store',
            'billing_address',
            'delivery_address',
            'order_timestamp',
            'delivery_timestamp',
            'status',
            'note',
            'product_orders'
        ]


class ProductOrderDetailsSerializer(serializers.ModelSerializer):
    ordered_products = serializers.SerializerMethodField()

    class Meta:
        model = ProductOrder
        fields = [
            'id',
            'ordered_products'
        ]

    def get_ordered_products(self, product_order):
        items = set(product_order.ordered_products.all())
        serializer = OrderedProductSerializer(instance=items, many=True)
        return serializer.data


class ProductOrderForDeliveryNoteSerializer(ProductOrderDetailsSerializer):
    def get_ordered_products(self, product_order):
        items = product_order.ordered_products.filter(delivery_note__isnull=False) \
            .only('id', 'product_order__product__name', 'status').prefetch_related('labels')
        serializer = OrderedProductSerializer(instance=items, many=True)
        return serializer.data


class ProductOrderForManufacturePlanSerializer(ProductOrderDetailsSerializer):
    def get_ordered_products(self, product_order):
        items = product_order.ordered_products.all()
        serializer = OrderedProductSerializer(instance=items, many=True)
        return serializer.data


class ProductOrderForOrderSerializer(ProductOrderForDeliveryNoteSerializer):
    def get_ordered_products(self, product_order):
        items = set(product_order.ordered_products.all())
        serializer = OrderedProductForManufacturePlanSerializer(instance=items, many=True)
        return serializer.data
