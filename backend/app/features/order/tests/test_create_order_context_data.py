import json

import pytest
from django.urls import reverse

from backend.app.features.order.use_cases.create_context import CreateOrderContextData


@pytest.mark.django_db
def test_order_initial_data():
    request = CreateOrderContextData.Request(None)
    use_case = CreateOrderContextData(request)
    response = use_case.execute()
    result = json.loads(response.initial_data)
    assert 'products' in result
    assert len(result['products']) == 0
    assert 'materials' in result
    assert len(result['materials']) == 0


@pytest.mark.django_db
def test_call_create_order_return_200(admin_client, admin_user):
    admin_user.activated = True
    admin_user.save()
    response = admin_client.get(reverse('app_order_modeladmin_create'))
    assert response.status_code == 200
