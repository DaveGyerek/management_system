from django.urls import reverse
from django.utils import timezone

from ...order.models import Order
from ...client.models import Client, Store


def _create_order():
    client = Client.objects.create(name='a', billing_address='b', email='c')
    store = Store.objects.create(name="a", client=client)
    Order.objects.create(client=client, store=store, billing_address="a", delivery_address='b',
                         delivery_timestamp=timezone.now())


def test_order_details_exist(admin_client, admin_user):
    admin_user.activated = True
    admin_user.save()
    _create_order()
    response = admin_client.get(reverse('app_order_modeladmin_edit', kwargs={"instance_pk": 1}))
    assert response.status_code == 200


def test_order_details_not_exist(admin_client, admin_user):
    admin_user.activated = True
    admin_user.save()
    response = admin_client.get(reverse('app_order_modeladmin_edit', kwargs={"instance_pk": 1}))
    assert response.status_code == 404
