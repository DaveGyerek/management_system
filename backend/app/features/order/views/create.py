from django.db import transaction
from django.shortcuts import redirect
from wagtail.admin import messages

from backend.app.features.order.use_cases.create import CreateOrder
from backend.custom_wagtail.views import CreateView
from ..use_cases.create_context import CreateOrderContextData
from ..use_cases.process_form import OrderProcessForm


class OrderViewFormMixin:
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        """BASED ON wagtail\contrib\modeladmin\views.py"""
        form = super().get_form()

        request = OrderProcessForm.Request(form)
        use_case = OrderProcessForm(request)
        response = use_case.execute_validate()

        if response.is_valid:
            return self.form_valid(form, response.product_orders_serializer)
        use_case.execute_invalid()
        return self.form_invalid(form)


class CreateOrderView(OrderViewFormMixin, CreateView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = context['form']

        request = CreateOrderContextData.Request(form)
        use_case = CreateOrderContextData(request)
        response = use_case.execute()

        response.append_to_form(form)
        return context

    def form_valid(self, form, product_orders_serializer):
        request = CreateOrder.Request(form, product_orders_serializer)
        use_case = CreateOrder(request)
        response = use_case.execute()

        """COPY OF wagtail\contrib\modeladmin\views.py"""
        messages.success(
            self.request, self.get_success_message(response.order),
            buttons=self.get_success_message_buttons(response.order)
        )
        return redirect(self.get_success_url())
