from django.views.generic import TemplateView
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ...user.permissions import ActivatedUserMixin
from ...ordered_product.use_cases.common import OrderedProductAndLabelsContext
from ..repository import OrderRepository
from ..serializers import ProductOrderDetailsSerializer, ProductOrderForOrderSerializer
from ..use_cases.details import OrderDetailsTemplateContext


class OrderDetailsView(OrderedProductAndLabelsContext, ActivatedUserMixin, TemplateView):
    template_name = 'app/order/details.html'

    def get_context_data(self, **kwargs):
        request = OrderDetailsTemplateContext.Request(kwargs['pk'])
        use_case = OrderDetailsTemplateContext(request)
        context = use_case.execute()

        context.update(self.get_delivery_notes())
        context.update(self.get_orders())
        context.update(self.get_manufacture_plans())

        return context


class OrderDetailProductDetailsApiView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, *args, **kwargs):
        queryset = OrderRepository().get_product_order_details_for_order(kwargs['order_id'],
                                                                         kwargs['po_id'])
        serializer_class = ProductOrderDetailsSerializer(queryset)
        return Response(serializer_class.data)


class OpenOrderProductDetailsApiView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, *args, **kwargs):
        queryset = OrderRepository().get_product_order_details_for_open_orders(kwargs['po_id'])
        serializer_class = ProductOrderForOrderSerializer(queryset)
        return Response(serializer_class.data)
