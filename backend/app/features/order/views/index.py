from django.utils.translation import ugettext as _

from ..use_cases.open_order_context import OpenOrderContext
from backend.app.utils.mixins import SendToManufactMixin
from backend.custom_wagtail.views import IndexView


class OpenOrderIndexView(SendToManufactMixin, IndexView):
    def get_page_title(self):
        return _('Open orders')

    def get_template_names(self):
        return [
            'app/order/open_index.html'
        ]

    def get_buttons_for_obj(self, obj):
        return []

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.order_details_context)
        context_class = OpenOrderContext(context['view'].queryset)

        delivery_notes = context_class.get_delivery_notes()
        manufacture_plans = context_class.get_manufacture_plans()
        clients = context_class.get_client_details()
        orders = context_class.get_orders()

        context.update(delivery_notes)
        context.update(clients)
        context.update(manufacture_plans)
        context.update(orders)
        return context
