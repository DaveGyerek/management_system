from django.shortcuts import redirect
from wagtail.admin import messages

from .create import OrderViewFormMixin
from ..use_cases.edit import EditOrder
from ..use_cases.edit_context import EditOrderContextData
from backend.custom_wagtail.views import EditView


class EditOrderView(OrderViewFormMixin, EditView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = context['form']

        request = EditOrderContextData.Request(form)
        use_case = EditOrderContextData(request)
        response = use_case.execute()

        response.append_to_form(form)
        return context

    def form_valid(self, form, product_orders_serializer):

        request = EditOrder.Request(form, product_orders_serializer)
        use_case = EditOrder(request)
        response = use_case.execute()

        """COPY OF wagtail\contrib\modeladmin\views.py"""
        messages.success(
            self.request, self.get_success_message(response.order),
            buttons=self.get_success_message_buttons(response.order)
        )
        return redirect(self.get_success_url())
