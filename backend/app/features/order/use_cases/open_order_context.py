import json

from ...ordered_product.serializers import ProductOrderForOpenOrderSerializer
from ...ordered_product.use_cases.finished_product_context import FinishedProductContext


class OpenOrderContext(FinishedProductContext):
    def __init__(self, queryset, *args, **kwargs):
        self.queryset = queryset
        super().__init__(*args, **kwargs)

    def get_product_order_details(self) -> dict:
        queryset = self.order_repository.get_ordered_product_groups_for_open_order(self.queryset)
        serializer = ProductOrderForOpenOrderSerializer(queryset, many=True)
        data = {'product_orders': serializer.data}
        json_data = json.dumps(data)

        return {
            'details_json': json_data
        }
