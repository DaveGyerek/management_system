from django.utils.translation import ugettext_lazy as _

from ..repository import OrderRepository


class OrderDetailsTemplateContext:
    class Request:
        def __init__(self, order_id: int):
            self.order_id = order_id

    def __init__(self, request: Request, order_repository: OrderRepository = None):
        super().__init__()
        self.request = request
        self.order_repository = order_repository or OrderRepository()

    def execute(self) -> dict:
        order = self.order_repository.get_order_for_details(order_id=self.request.order_id)
        order_data = (
            (_('Client'), order.client.name),
            (_('Shop'), order.store.name if order.store else ''),
            (_('Delivery address'), order.delivery_address),
            (_('Scheduled delivery'), order.delivery_timestamp),
            (_('Note'), order.note),
        )
        headers = (_('Name'), _('Quantity (Open/All)'), _('Price'), _('Note'),)
        products_data = []
        for product_order in self.order_repository.get_product_orders_with_open_quantity(order).iterator():
            products_data.append(
                [(product_order.id, product_order.open_quantity),
                 product_order.product.name,
                 '{}/{}'.format(product_order.open_quantity, product_order.quantity),
                 '{0:n} Ft'.format(float(product_order.price)),
                 product_order.note or '',
                 product_order.id]
            )

        context = {
            'order': order,
            'order_data': order_data,
            'headers': headers,
            'product_orders': products_data,
        }

        return context
