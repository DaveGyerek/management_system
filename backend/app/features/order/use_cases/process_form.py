import json

from ..serializers import ProductOrderSerializer


class OrderProcessForm:
    class Request:
        def __init__(self, form):
            self.form = form

    class Response:
        def __init__(self, product_orders_serializer: ProductOrderSerializer, is_valid: bool):
            self.product_orders_serializer = product_orders_serializer
            self.is_valid = is_valid

    def __init__(self, request: Request):
        self.request = request
        self.formsets_backup = None
        self.response = None

    def execute_validate(self) -> Response:
        self.formsets_backup = self.request.form.formsets
        self.request.form.formsets = {}
        product_orders_json = self.request.form.data['product_orders']
        product_orders_data = json.loads(product_orders_json)
        product_orders_serializer = ProductOrderSerializer(data=product_orders_data, many=True)
        is_valid = product_orders_serializer.is_valid() and self.request.form.is_valid()
        self.response = OrderProcessForm.Response(product_orders_serializer, is_valid)
        return self.response

    def execute_invalid(self):
        for error in self.response.product_orders_serializer.errors:
            self.request.form.errors.update(error)
        self.request.form.formsets = self.formsets_backup
