import json

from backend.app.models import Product, Material
from .create_context import CreateOrderContextData
from ..serializers import ProductOrderEditSerializer
from ...client.use_cases.order_client_data import OrderClientData
from ...material.serializers import MaterialSerializer
from ...product.serializers import ProductDataSerializer


class EditOrderContextData(CreateOrderContextData):
    def _get_initial_data(self):
        products = Product.objects.prefetch_related('product_type')
        products_serializer = ProductDataSerializer(products, many=True)

        materials = Material.objects.all()
        material_serializer = MaterialSerializer(materials, many=True)

        client = self.request.form.instance.client
        prices_request = OrderClientData.Request(client)
        prices_use_case = OrderClientData(prices_request)
        prices = prices_use_case.execute()

        response = {
            'products': products_serializer.data,
            'materials': material_serializer.data,
            'prices': prices
        }
        return json.dumps(response)

    def _get_product_orders(self):
        order = self.request.form.instance
        product_orders = order.product_orders.all()
        product_orders_serializer = ProductOrderEditSerializer(product_orders, many=True)
        product_orders_data = product_orders_serializer.data
        return json.dumps(product_orders_data)
