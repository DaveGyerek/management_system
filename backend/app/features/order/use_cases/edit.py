from backend.app.models import UniquePricing, ProductOrder, ProductOrderPart, OrderedProduct
from ...ordered_product.constants import OPEN
from ...ordered_product.repository import OrderedProductRepository
from .create import CreateOrder


class EditOrder(CreateOrder):
    def __init__(self, request: CreateOrder.Request,
                 ordered_product_repository: OrderedProductRepository = None):
        super().__init__(request)
        self.ordered_product_repository = ordered_product_repository or OrderedProductRepository()

    def execute(self) -> CreateOrder.Response:
        order = self.request.order_form.save()

        unique_pricings = UniquePricing.objects.filter(client=order.client)
        unique_pricings = {x.product_id: x.price for x in unique_pricings}

        new_product_orders = self.request.product_orders_serializer.validated_data
        initial_product_orders = self.request.product_orders_serializer.initial_data

        open_ordered_products = OrderedProduct.objects.filter(status=OPEN, product_order__order=order)
        po_ids = [v['id'] for v in initial_product_orders]
        product_orders = ProductOrder.objects.prefetch_related('ordered_products').filter(pk__in=po_ids)

        # if deleted, delete all open
        delete_product_orders = order.product_orders.all().exclude(id__in=po_ids)
        for delete_po in delete_product_orders:
            if delete_po.ordered_products.all().filter(status__gt=OPEN).count() == 0:
                delete_po.delete()
            # decrease quantity
            else:
                delete_po.quantity = delete_po.ordered_products.all().filter(status__gt=OPEN).count()
                open_ordered_products.filter(product_order__in=delete_product_orders).delete()
                delete_po.save()

        index = -1
        for product_order_data in new_product_orders:
            index = index + 1
            po = product_orders.filter(pk=initial_product_orders[index]['id']).first()
            if product_order_data['quantity'] > 0:
                with_out_parts = {k: v for k, v in product_order_data.items() if k != 'parts'}

                # delete origin po
                # if po.quantity == product_order_data['quantity']:
                if po:
                    if po.ordered_products.all().filter(status__gt=OPEN).count() == 0:
                        po.delete()
                    # decrease quantity
                    else:
                        po.quantity = po.ordered_products.all().filter(status__gt=OPEN).count()
                        open_ordered_products.filter(product_order=po).delete()
                        po.save()

                product_order = ProductOrder.objects.create(order=order, **with_out_parts)

                original_price = float(product_order.product.price)
                if product_order.product.id in unique_pricings:
                    original_price = float(unique_pricings[product_order.product.id])

                product_order.original_price = original_price
                product_order.save()

                for product_order_part_data in product_order_data['parts']:
                    product_order_part = ProductOrderPart.objects.create(product_order=product_order,
                                                                         **product_order_part_data)

                    price = float(product_order_part.material.price) * product_order_part.material_quantity
                    net_price = float(product_order_part.material.net_price) * product_order_part.material_quantity

                    product_order_part.price = price
                    product_order_part.net_price = net_price
                    product_order_part.save()

        return EditOrder.Response(order)
