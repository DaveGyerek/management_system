import json

from django.forms import HiddenInput, CharField
from django.shortcuts import get_object_or_404

from backend.app.models import Product, Material, Client
from ...client.use_cases.order_client_data import OrderClientData
from ...product.serializers import ProductDataSerializer
from ...material.serializers import MaterialSerializer


class CreateOrderContextData:
    class Request:
        def __init__(self, form):
            self.form = form

    class Response:
        def __init__(self, product_orders: str, initial_data: str):
            self.product_orders = product_orders
            self.initial_data = initial_data

        def append_to_form(self, form):
            def append(key, value):
                try:
                    form.data._mutable = True
                    form.data[key] = value
                    form.data._mutable = False
                except AttributeError:
                    form.data[key] = value
                form.fields[key] = CharField(widget=HiddenInput(), initial=value)

            append('initial-data', self.initial_data)
            if self.product_orders:
                append('product_orders', self.product_orders)

    def __init__(self, request: Request):
        self.request = request

    def execute(self) -> Response:
        initial_data = self._get_initial_data()
        product_orders = self._get_product_orders()
        return CreateOrderContextData.Response(product_orders, initial_data)

    def _get_initial_data(self):
        try:
            client_id = self.request.form.data['client']
            queryset = Client.objects.all()
            client = get_object_or_404(queryset, id=client_id)
        except (KeyError, AttributeError, ValueError):
            client = None

        products = Product.objects.prefetch_related('product_type')
        products_serializer = ProductDataSerializer(products, many=True)

        materials = Material.objects.all()
        material_serializer = MaterialSerializer(materials, many=True)
        response = {
            'products': products_serializer.data,
            'materials': material_serializer.data
        }
        if client:
            prices_request = OrderClientData.Request(client)
            prices_use_case = OrderClientData(prices_request)
            prices = prices_use_case.execute()
            response['prices'] = prices
        return json.dumps(response)

    def _get_product_orders(self):
        try:
            return self.request.form.data['product_orders']
        except (KeyError, AttributeError):
            return None
