from ..serializers import ProductOrderSerializer
from backend.app.models import UniquePricing, Order, ProductOrder, ProductOrderPart


class CreateOrder:
    class Request:
        def __init__(self, order_form, product_orders_serializer: ProductOrderSerializer):
            self.order_form = order_form
            self.product_orders_serializer = product_orders_serializer

    class Response:
        def __init__(self, order: Order):
            self.order = order

    def __init__(self, request: Request):
        self.request = request

    def execute(self) -> Response:
        order = self.request.order_form.save()

        unique_pricings = UniquePricing.objects.filter(client=order.client)
        unique_pricings = {x.product_id: x.price for x in unique_pricings}

        product_orders = self.request.product_orders_serializer.validated_data

        for product_order_data in product_orders:
            with_out_parts = {k: v for k, v in product_order_data.items() if k != 'parts'}
            product_order = ProductOrder.objects.create(order=order, **with_out_parts)

            original_price = float(product_order.product.price)
            if product_order.product.id in unique_pricings:
                original_price = float(unique_pricings[product_order.product.id])

            product_order.original_price = original_price
            product_order.save()

            for product_order_part_data in product_order_data['parts']:
                product_order_part = ProductOrderPart.objects.create(product_order=product_order,
                                                                     **product_order_part_data)

                price = float(product_order_part.price) * product_order_part.material_quantity
                net_price = float(product_order_part.net_price) * product_order_part.material_quantity
                product_order_part.price = price
                product_order_part.net_price = net_price
                product_order_part.save()

        return CreateOrder.Response(order)
