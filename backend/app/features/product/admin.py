from django.contrib import admin

from .models import ProductType, Product, ProductPart, ProductLabel

admin.site.register([ProductType, Product, ProductPart, ProductLabel])
