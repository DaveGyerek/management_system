from rest_framework import serializers

from .models import Product, ProductPart


class ProductPartDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductPart
        fields = (
            'id',
            'name',
            'short_name',
            'material',
            'material_quantity',
            'sort_order'
        )


class ProductDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'product_type',
            'price',
            'parts'
        )

    parts = ProductPartDataSerializer(read_only=True, many=True)
    product_type = serializers.SerializerMethodField()

    def get_product_type(self, obj):
        return str(obj.product_type)
