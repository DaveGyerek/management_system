from django.utils.translation import ugettext as _
from wagtail.admin.edit_handlers import MultiFieldPanel, FieldPanel, FieldRowPanel, InlinePanel
from wagtail.contrib.modeladmin.options import modeladmin_register, ModelAdminGroup

from backend.custom_wagtail.modeladmin import ModelAdmin
from .models import Product, ProductType, ProductPart, ProductLabel

ProductLabel.panels = [
    MultiFieldPanel((
        FieldPanel('name'),
    )),
]

Product.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('name', classname='col6'),
            FieldPanel('price', classname='col6'),
            FieldPanel('product_type', classname='col6'),
            FieldPanel('ready_made', classname='col6'),
        )),
    ), _('Basic details')),
    MultiFieldPanel((
        InlinePanel('parts', label=_('Material')),
    ), _('Material Norm')),
    MultiFieldPanel((
        InlinePanel('labels', label=_('Labels')),
    ), _('Labels')),
]

ProductPart.panels = [
    MultiFieldPanel((
        FieldPanel('name'),
        FieldPanel('short_name'),
        FieldPanel('material'),
        FieldPanel('material_quantity'),
    )),
]


class ProductAdmin(ModelAdmin):
    model = Product
    menu_icon = 'fa-database'
    list_display = (
        'name',
        'product_type',
        'formatted_price',
    )
    list_filter = (
        'product_type',
    )
    search_fields = (
        'name',
    )

    def formatted_price(self, obj):
        return '{0:n} Ft'.format(float(obj.price))

    formatted_price.short_description = _('Default price')
    formatted_price.admin_order_field = 'price'

    row_as_link = 'app_product_modeladmin_edit'


class ProductTypeAdmin(ModelAdmin):
    model = ProductType
    menu_icon = 'fa-database'
    list_display = (
        'name',
    )
    search_fields = (
        'name',
    )

    row_as_link = 'app_producttype_modeladmin_edit'


class ProductAdminGroup(ModelAdminGroup):
    menu_icon = 'fa-database'
    menu_label = _('Products')
    items = [
        ProductAdmin,
        ProductTypeAdmin,
    ]


modeladmin_register(ProductAdminGroup)
