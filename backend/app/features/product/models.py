from decimal import Decimal
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext as _
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.core.models import Orderable


class ProductType(models.Model):
    """
    Represent a product category model which can help to
    organize the :model:`app.Product` objects.
    """

    class Meta:
        verbose_name = _('Product Type')
        verbose_name_plural = _('Product Types')
        ordering = ['name']

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=50,
        unique=True,
    )

    def __str__(self):
        return self.name


class Product(ClusterableModel):
    """
    Represents the main blocks of the app.
    Related to :model:`app.ProductCategory`.
    With this we can model and manage the real products.
    """

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        unique_together = [
            'name',
            'product_type',
        ]
        ordering = [
            'product_type',
            'name',
            'price',
        ]

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=150,
        unique=True,
    )
    product_type = models.ForeignKey(
        'app.ProductType',
        verbose_name=_('Product Type'),
        related_name='products',
        on_delete=models.CASCADE,
    )
    price = models.DecimalField(
        verbose_name=_('Default Price'),
        max_digits=10,
        decimal_places=4,
        validators=[MinValueValidator(Decimal('0.0001'))],
    )
    ready_made = models.BooleanField(
        verbose_name=_('Ready-made'),
        default=False,
    )

    def __str__(self):
        return self.name


class ProductPart(Orderable):
    """
    Connects the :model:`app.Product` with :model:`app.Material`
    in various parts. We can create material norms for each products,
    and with this, indicate the structure of an item.
    """

    class Meta:
        verbose_name = _('Product Part')
        verbose_name_plural = _('Product Parts')
        ordering = [
            'sort_order',
            'name',
        ]

    product = ParentalKey(
        'app.Product',
        verbose_name=_('Product'),
        related_name='parts',
        on_delete=models.CASCADE,
    )
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=100,
    )
    short_name = models.CharField(
        verbose_name=_('Short Name'),
        max_length=100,
        blank=True,
    )
    material = models.ForeignKey(
        'app.Material',
        verbose_name=_('Material'),
        related_name='product_parts',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    material_quantity = models.FloatField(
        verbose_name=_('Material Quantity'),
        default=1,
    )

    def __str__(self):
        return '{} - {}'.format(self.product, self.name)


class ProductLabel(Orderable):
    """
    Represent a labeling for the :model:`app.Product` objects.
    With this we can identify the various parts of a product,
    and then create printed labels and manage each of these items
    separately.
    """

    class Meta:
        verbose_name = _('Product Label')
        verbose_name_plural = _('Product Labels')
        ordering = ['name']

    name = models.CharField(
        verbose_name=_('Name'),
        max_length=100,
    )
    product = ParentalKey(
        'app.Product',
        related_name='labels',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return '{} - {}'.format(self.product.name, self.name)
