import os

from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.urls import reverse
from django.views import View
from django.conf import settings
from backend.app.features.manufacture_plan.use_cases.pdf import ManufacturePlanPdfTemplateContext
from backend.app.utils.pdf import RenderPdf


class ManufacturePlanPdfView(View):
    path = None

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))
        filename = self.create_pdf(request, *args, **kwargs)
        try:
            with open(filename, 'rb') as pdf:
                response = HttpResponse(pdf.read(), content_type='application/pdf')
                response['Content-Disposition'] = 'inline;filename={}'.format(os.path.basename(filename))
                return response
        except FileNotFoundError:
            return HttpResponseBadRequest(
                'Your url did not return anything'.format(get_str(request, *args, **kwargs)))

    def create_pdf(self, request, *args, **kwargs):
        abs_path = get_str(self.path, request, *args, **kwargs)
        path = os.path.relpath(abs_path, settings.BASE_DIR).replace('\\', '/')
        filename = os.path.abspath(path)

        options = {
            'margin-left': "5mm",
            'margin-right': "5mm",
            'margin-bottom': "8mm",
            'footer-center': 'Oldal [topage]/[page]',
            'footer-spacing': '3',
            'footer-font-size': '7'
        }

        request = ManufacturePlanPdfTemplateContext.Request(kwargs['manufacture_plan_id'])
        use_case = ManufacturePlanPdfTemplateContext(request)
        context = use_case.execute()
        pdf_use_case = RenderPdf()
        pdf_request = RenderPdf.Request(
            output_path=filename,
            template_name='app/manufacture_plan/pdf.html',
            context=context,
            landscape=True,
            options=options
        )
        pdf_use_case.execute(pdf_request)
        return filename


def get_str(original, request, *args, **kwargs):
    if callable(original):
        return original(request, *args, **kwargs)
    if isinstance(original, str):
        return original
    raise Exception('Not valid url')
