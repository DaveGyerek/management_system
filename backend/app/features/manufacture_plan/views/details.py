from django.views.generic import TemplateView
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from ...order.repository import OrderRepository
from ...order.serializers import ProductOrderForManufacturePlanSerializer
from ....utils.mixins import SetOrderedProductsFinishedMixin
from ..use_cases.details import ManufacturePlanDetailsTemplateContext
from ...user.permissions import ActivatedUserMixin


class ManufacturePlanDetailsView(SetOrderedProductsFinishedMixin, ActivatedUserMixin, TemplateView):
    template_name = 'app/manufacture_plan/details.html'

    def get_context_data(self, **kwargs):
        request = ManufacturePlanDetailsTemplateContext.Request(kwargs['pk'])
        use_case = ManufacturePlanDetailsTemplateContext(request)
        context = use_case.execute()

        context.update(self.manufact_context)

        return context


class ManufacturePlanOrderedProductDetailsApiView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication,)

    def get(self, *args, **kwargs):
        queryset = OrderRepository().get_product_order_details_for_manufacture_plan(kwargs['mp_id'],
                                                                                    kwargs['po_id'])
        serializer_class = ProductOrderForManufacturePlanSerializer(queryset)
        return Response(serializer_class.data)
