from rest_framework import serializers

from ..order.repository import OrderRepository
from ..order.serializers import ProductOrderForManufacturePlanSerializer
from .models import ManufacturePlan


class ManufacturePlanForSendingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ManufacturePlan
        fields = [
            'id',
            'identification_number',
            'date',
        ]


class ManufacturePlanDetailsSerializer(serializers.ModelSerializer):
    product_orders = serializers.SerializerMethodField()

    class Meta:
        model = ManufacturePlan
        fields = ['product_orders']

    def get_product_orders(self, manufacture_plan):
        repository = OrderRepository()
        queryset = repository.get_ordered_product_groups_for_manufacture_plan(mp=manufacture_plan)
        serializer = ProductOrderForManufacturePlanSerializer(instance=queryset, many=True)
        return serializer.data
