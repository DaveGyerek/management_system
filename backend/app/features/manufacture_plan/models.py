from django.utils.translation import ugettext_lazy as _
from django.db import models

from .constants import STATUSES, MAKING


class ManufacturePlan(models.Model):
    """
    Represents a container for :model:`app.OrderedProduct` -s
    to help the company organize their manufacture
    """

    class Meta:
        verbose_name = _('Manufacture plan')
        verbose_name_plural = _('Manufacture plans')
        ordering = [
            '-date',
        ]

    identification_number = models.CharField(
        verbose_name=_('Identification number'),
        max_length=30,
    )
    date = models.DateField(
        verbose_name=_('Date of manufacture'),
    )

    status = models.SmallIntegerField(
        verbose_name=_('State'),
        choices=STATUSES,
        default=MAKING,
    )

    def __str__(self):
        return self.date.strftime('%Y/%m/%d')
