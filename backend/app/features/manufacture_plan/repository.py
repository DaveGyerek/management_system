from django.shortcuts import get_object_or_404

from .models import ManufacturePlan


class ManufacturePlanRepository:
    def get_manufacture_plan(self, mp_id):
        queryset = ManufacturePlan.objects.only('id')
        mp = get_object_or_404(queryset, id=mp_id)
        return mp

    def get_manufacture_plans_for_sending(self):
        return ManufacturePlan.objects.only('id', 'identification_number', 'date')

    def get_manufacture_plan_for_details(self, mp_id):
        queryset = ManufacturePlan.objects.only('id')
        mp = get_object_or_404(queryset, id=mp_id)
        return mp
