from django.utils.translation import ugettext as _

MAKING = 0
READY = 1
STATUSES = (
    (MAKING, _('Under manufacture')),
    (READY, _('Ready')),
)
