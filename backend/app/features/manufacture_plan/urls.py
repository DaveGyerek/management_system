import os

from django.conf import settings
from django.conf.urls import url

from .views.pdf import ManufacturePlanPdfView
from .views.details import ManufacturePlanDetailsView, ManufacturePlanOrderedProductDetailsApiView

urlpatterns = [
    url(r'^manufactureplan/(?P<pk>\d+)/$', ManufacturePlanDetailsView.as_view(),
        name='manufacture-plan-details'),
    url(r'^api/1/manufacture-plan/(?P<mp_id>\d+)/ordered-product/(?P<po_id>\d+)/$',
        ManufacturePlanOrderedProductDetailsApiView.as_view(),
        name='manufacture-plan-product-order-data'),
    url(r'^manufactureplan/pdf/(?P<manufacture_plan_id>\d+)/$',
        ManufacturePlanPdfView.as_view(
            path=os.path.join(str(settings.APPS_DIR), 'tmp.pdf')),
        name='manufacture-plan-pdf-export'
        ),
]
