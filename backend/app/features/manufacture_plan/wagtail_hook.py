from django.utils.translation import ugettext_lazy as _
from wagtail.admin.edit_handlers import FieldRowPanel, FieldPanel, MultiFieldPanel
from wagtail.contrib.modeladmin.options import modeladmin_register

from .models import ManufacturePlan
from backend.custom_wagtail.modeladmin import ModelAdmin

ManufacturePlan.panels = [
    MultiFieldPanel((
        FieldRowPanel((
            FieldPanel('identification_number', classname='col4'),
            FieldPanel('date', classname='col4'),
        )),
    ), _('Manufacture Plan'))
]


class ManufactureAdmin(ModelAdmin):
    model = ManufacturePlan
    menu_icon = 'fa-folder-open'
    menu_label = _('Manufacture Plans')
    list_display = (
        'identification_number',
        'date',
        'status',
    )

    list_filter = (
        'status',
    )
    search_fields = (
        'identification_number',
    )

    row_as_link = 'manufacture-plan-details'


modeladmin_register(ManufactureAdmin)
