import json

from django.utils.translation import ugettext_lazy as _

from ...delivery_note.repository import DeliveryNoteRepository
from ...manufacture_plan.serializers import ManufacturePlanDetailsSerializer
from ...order.repository import OrderRepository
from ..repository import ManufacturePlanRepository
from ...ordered_product.use_cases.common import OrderedProductAndLabelsContext


class ManufacturePlanDetailsTemplateContext(OrderedProductAndLabelsContext):
    class Request:
        def __init__(self, mp_id: int):
            self.mp_id = mp_id

    def __init__(self, request: Request, delivery_note_repository: DeliveryNoteRepository = None,
                 manufacture_plan_repository: ManufacturePlanRepository = None,
                 order_repository: OrderRepository = None):
        super().__init__(delivery_note_repository, manufacture_plan_repository, order_repository)
        self.request = request

    def execute(self) -> dict:
        mp = self.manufacture_plan_repository.get_manufacture_plan_for_details(mp_id=self.request.mp_id)
        product_orders = self.order_repository.get_ordered_product_groups_for_manufacture_plan(mp)
        products_data = []

        for product_order in product_orders.iterator():

            products_data.append([
                [product_order.id, product_order.ordered_product_count - product_order.manufactured_count,
                 product_order.not_ready_count],
                product_order.order,
                '{}/{}'.format(product_order.manufactured_count, product_order.ordered_product_count),
                product_order.product.name,
                product_order.order.client.name,
                product_order.note
            ])

        headers = (
            _('Order'), _('Done'), _('Name'), _('Client'), _('Note'))

        context = {
            'headers': headers,
            'manufacture_plan': mp,
            'ordered_products': products_data,
        }

        context.update(self.get_delivery_notes())
        context.update(self.get_orders())
        context.update(self.get_manufacture_plans())

        return context

    def create_product_order_details(self, mp):
        serializer = ManufacturePlanDetailsSerializer(mp)
        data = serializer.data
        return json.dumps(data)
