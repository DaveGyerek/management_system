from collections import OrderedDict

from django.utils.translation import ugettext_lazy as _

from .details import ManufacturePlanDetailsTemplateContext


class ManufacturePlanPdfTemplateContext(ManufacturePlanDetailsTemplateContext):
    def execute(self) -> dict:
        mp = self.manufacture_plan_repository.get_manufacture_plan_for_details(mp_id=self.request.mp_id)
        product_orders = self.order_repository.get_ordered_product_groups_for_manufacture_plan_pdf(mp)
        products_data = OrderedDict()

        for product_order in product_orders.order_by('product__product_type__name', 'product__name'):
            item = [product_order.product.name,
                    product_order.in_progress_count,
                    product_order.note,
                    product_order.order.client.name,
                    product_order.order.store.name if product_order.order.store else '-']
            if product_order.product.product_type.name in products_data:
                products_data[product_order.product.product_type.name].append(item)
            else:
                products_data[product_order.product.product_type.name] = [item]

        headers = (
            _('Product'), _('Qt.'), _('Note'), _('Buyer'), _('Shop'))
        return {
            'headers': headers,
            'manufacture_plan': mp,
            'ordered_products': products_data.items(),
        }
