from django.contrib import admin

from .models import ManufacturePlan

admin.site.register([
    ManufacturePlan,
])
