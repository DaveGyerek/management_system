from django.conf.urls import include, url
from django.views import defaults as default_views

import debug_toolbar

from ..base.urls import urlpatterns

urlpatterns += [
    url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
    url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
    url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
    url(r'^500/$', default_views.server_error),
    url(r'^__debug__/', include(debug_toolbar.urls)),
]
