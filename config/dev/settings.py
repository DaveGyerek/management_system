from ..base.settings import *

_ENV = 'dev'
DATA_ROOT = ROOT_DIR.path('backend/dev-data')

# --------------------CONFIG--------------------
DEBUG = True
ROOT_URLCONF = 'config.{}.urls'.format(_ENV)
WSGI_APPLICATION = 'config.{}.wsgi.application'.format(_ENV)
MEDIA_ROOT = str(DATA_ROOT.path('media'))
STATIC_ROOT = str(DATA_ROOT.path('static'))
FIXTURE_DIRS = (
    str(DATA_ROOT.path('fixtures')),
)
ALLOWED_HOSTS = ['*']
SECRET_KEY = 'v@@3a&z^6z6dnzo^7_fz18rq(_r#zge5z3zqoe!3#(4h9m-ks='
DEV = True
CORS_ORIGIN_ALLOW_ALL = True
MIDDLEWARE.insert(1, 'corsheaders.middleware.CorsMiddleware')

# --------------------LOCALE--------------------
locale.setlocale(locale.LC_ALL, '')

# --------------------DATABASE--------------------
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': str(DATA_ROOT.path('db.sqlite3'))
#     }
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_DB'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True,
    }
}

# --------------------DEBUG-TOOLBAR--------------------
INSTALLED_APPS += [
    'debug_toolbar',
]
INTERNAL_IPS = ['127.0.0.1']
MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')


def show_toolbar(request):
    return True


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": show_toolbar,
}
