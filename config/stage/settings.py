import os

from ..base.settings import *

DATA_ROOT = os.path.abspath('/var/www/app')
CONF_ROOT = ROOT_DIR.path('config', 'stage')

# --------------------CONFIG--------------------
DEBUG = env.bool('DJANGO_DEBUG', False)
ROOT_URLCONF = 'config.stage.urls'
WSGI_APPLICATION = 'config.wsgi.application'
MEDIA_ROOT = os.path.join(DATA_ROOT, 'media')
STATIC_ROOT = os.path.join(DATA_ROOT, 'static')
FIXTURE_DIRS = (
    os.path.abspath('backend/dev-data/fixtures'),
    str(ROOT_DIR.path('backend', 'app', 'dev-data', 'fixtures')),
)
SECRET_KEY = env('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = ['207.154.192.62']

# --------------------DATABASE--------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_DB'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True,
    }
}
# --------------------SENTRY----------------------
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://cba10c620d5d4c7cbaea45d80df6baac@sentry.io/206352",
    integrations=[DjangoIntegration()]
)