import locale
import os

import environ

env = environ.Env()
ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('backend/app')
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# --------------------CONFIG--------------------
DEBUG = env.bool('DJANGO_DEBUG', False)
ROOT_URLCONF = ''
MEDIA_ROOT = ''
STATIC_ROOT = ''
WSGI_APPLICATION = ''
ALLOWED_HOSTS = []
SECRET_KEY = ''
DEV = False

# --------------------LOCALE--------------------
DATE_INPUT_FORMATS = ['%Y/%m/%d']
DATETIME_INPUT_FORMATS = ['%Y/%m/%d %h:%M']
TIME_ZONE = 'Europe/Budapest'
USE_I18N = True
USE_L10N = True
USE_TZ = True
TRANZ_REPLACE_DJANGO_TRANSLATIONS = True
USE_THOUSAND_SEPARATOR = True
THOUSAND_SEPARATOR = ' '
LOCALE_PATHS = [
    str(APPS_DIR.path('locale'))
]
LANGUAGE_CODE = 'en'
LANGUAGES = [
    ['en', 'English',
     'hu', 'Magyar']
]

# --------------------DJANGO--------------------
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.admin',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# --------------------LOGGING--------------------
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'App': {
            'handlers': ['console'],
            'level': 'DEBUG',
        }
    }
}

# --------------------AUTH--------------------
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]
AUTH_USER_MODEL = 'app.User'

# --------------------URLS--------------------
STATIC_URL = '/static/'
MEDIA_URL = '/media/'
ADMIN_URL = r'^admin/'
LOGIN_URL = r'^login/'

# --------------------STATIC--------------------
STATICFILES_DIRS = [
    str(APPS_DIR.path('static'))
]
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# --------------------REST--------------------
INSTALLED_APPS += [
    'rest_framework',
    'rest_framework_swagger',
    'knox',
]
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (),
    'DEFAULT_AUTHENTICATION_CLASSES': ('knox.auth.TokenAuthentication',),

    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20,
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
}
SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    },
}
# --------------------APP-----------------------
INSTALLED_APPS += [
    'backend.app',
]

# --------------------WAGTAIL--------------------
WAGTAIL_SITE_NAME = 'ManagementSystem'
INSTALLED_APPS += [
    'wagtail.contrib.forms',
    'wagtail.contrib.redirects',
    'wagtail.contrib.modeladmin',
    'wagtail.embeds',
    'wagtail.sites',
    'wagtail.users',
    'wagtail.snippets',
    'wagtail.documents',
    'wagtail.images',
    'wagtail.search',
    'wagtail.admin',
    'wagtail.core',
    'modelcluster',
    'taggit',
    # third party
    'wagtailfontawesome'
]
# --------------------CUSTOM_WAGTAIL--------------------
INSTALLED_APPS += [
    'backend.custom_wagtail',
]
CUSTOM_WAGTAIL = {
    'base-template': 'app/manage/base.html',
}

# --------------------MODEL ADMIN REORDER--------------------
INSTALLED_APPS += ['admin_reorder']
MIDDLEWARE += ['admin_reorder.middleware.ModelAdminReorder']
ADMIN_REORDER = [
    {'app': 'app', 'label': 'User', 'models': (
        {'model': 'app.User', 'label': 'Users'},
    )},
    {'app': 'app', 'label': 'Material', 'models': (
        {'model': 'app.MaterialCategory', 'label': 'Material Categories'},
        {'model': 'app.Material', 'label': 'Materials'},
    )},
    {'app': 'app', 'label': 'Product', 'models': (
        {'model': 'app.ProductType', 'label': 'Product Types'},
        {'model': 'app.Product', 'label': 'Products'},
        {'model': 'app.ProductPart', 'label': 'Product Parts'},
        {'model': 'app.ProductLabel', 'label': 'Product Labels'},
    )},
    {'app': 'app', 'label': 'Client', 'models': (
        {'model': 'app.Client', 'label': 'Clients'},
        {'model': 'app.Store', 'label': 'Stores'},
        {'model': 'app.UniquePricing', 'label': 'Unique pricings'},
    )},
    {'app': 'app', 'label': 'Order', 'models': (
        {'model': 'app.Order', 'label': 'Orders'},
        {'model': 'app.ProductOrder', 'label': 'Product orders'},
        {'model': 'app.ProductOrderPart', 'label': 'Product order parts'},
    )},
    {'app': 'app', 'label': 'Ordered Product', 'models': (
        {'model': 'app.OrderedProduct', 'label': 'Ordered products'},
        {'model': 'app.OrderedProductLabel', 'label': 'Ordered product labels'},
    )},
    {'app': 'app', 'label': 'Delivery note', 'models': (
        {'model': 'app.DeliveryNote', 'label': 'Delivery notes'},
    )},
    {'app': 'app', 'label': 'Manufacture plan', 'models': (
        {'model': 'app.ManufacturePlan', 'label': 'Manufacture plans'},
    )},
    {'app': 'app', 'label': 'Transaction', 'models': (
        {'model': 'app.Transaction', 'label': 'Transactions'},
    )},

]

# --------------------EASY PDF--------------------
INSTALLED_APPS += [
    'easy_pdf',
]