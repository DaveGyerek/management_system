from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from wagtail.admin import urls as wagtailadmin_urls

urlpatterns = [
                  url(settings.ADMIN_URL, admin.site.urls),
                  url(r'^', include('backend.app.urls')),
                  url(r'^', include(wagtailadmin_urls.urlpatterns)),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
