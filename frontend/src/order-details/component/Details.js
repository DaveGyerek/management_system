import React, {Component} from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

import {modalStyle, getCookie} from './utils';
import DeleteModal from './DeleteModal';
import {printLabels} from '../interact';

Array.prototype.groupBy = function (selector) {
    const obj = this.reduce((result, item) => {
        const key = selector(item);
        result[key] = result[key] || [];
        result[key].push(item);
        return result;
    }, {});
    return Object.keys(obj).map(key => ({
        key, items: obj[key]
    }));
};

class Details extends Component {
    initState = {
        id: null,
        ordered_products: [],
        checkedOrderedProducts: new Set(),
        checkedLabels: new Set(),
        modalVisible: false,

        deliveryModalVisible: false,
        selectedDelivery: 0,

        manufactureModalVisible: false,
        selectedManufacture: 0,

        transpositionModalVisible: false,
        selectedOrder: 0,

        isLoading: false,
        error: null,

        deleteModalVisible: false,
        selectedDelete: 0,
    };

    state = this.initState;

    statusesMap = [
        'Open',
        'To be manufactured',
        'Ready',
        'On delivery note',
        'Shipped'
    ];

    labelStatusesMap = [
        'Open',
        'To be manufactured',
        'Ready',
        'On delivery note',
        'Shipped'
    ];

    constructor(props) {
        super(props);
        this.props.initFunction(this.handleOrderedProducts);
    }

    initComponent = () => {
        this.setState({
            modalVisible: false,
            checkedOrderedProducts: new Set(),
            checkedLabels: new Set()
        })
    };

    handleOrderedProducts = (context) => {
        this.setState({
            id: context.id,
            ordered_products: context.ordered_products.sort(),
            modalVisible: true,
        });
    };

    setState(nextState, callback) {
        console.log({
            currentState: this.state,
            nextState
        });
        super.setState(nextState, callback);
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    handleClose = () => {
        this.setState(this.initState);
    };

    selectAll = (e) => {
        let tbodys = Array.from(e.target.closest('table').children);
        tbodys.shift();

        let ids = this.state.checkedOrderedProducts;
        let labelIds = this.state.checkedLabels;

        tbodys.forEach((tbody) => {
            let rows = Array.from(tbody.children);
            rows.pop();
            rows.forEach((row) => {
                if (row.classList.contains("parent-row")) {
                    (e.target.checked) ? ids.add(row.getAttribute('data-id')) : ids.delete(row.getAttribute('data-id'))
                } else {
                    (e.target.checked) ? labelIds.add(row.getAttribute('data-id')) : labelIds.delete(row.getAttribute('data-id'))
                }
            })
        });

        this.setState({
            checkedOrderedProducts: ids,
            checkedLabels: labelIds
        })
    };

    handleCheck = (e) => {
        let children = Array.from(e.target.closest('tbody').children);
        children.shift();
        children.pop();

        let ids = this.state.checkedOrderedProducts;
        let labelIds = this.state.checkedLabels;

        if (e.target.checked) {
            ids.add(e.target.id);
            children.forEach((child) => {
                labelIds.add(child.getAttribute('data-id'));
            })
        }
        else {
            ids.delete(e.target.id);
            children.forEach((child) => {
                labelIds.delete(child.getAttribute('data-id'));
            })
        }

        this.setState({
            checkedOrderedProducts: ids,
            checkedLabels: labelIds
        })

    };

    handleCheckLabel = (e) => {
        let children = Array.from(e.target.closest('tbody').children);
        children.shift();
        children.pop();

        let allLabelIds = children.map(child => child.getAttribute('data-id'));

        let ids = this.state.checkedOrderedProducts;
        let labelIds = this.state.checkedLabels;

        if (e.target.checked) {
            labelIds.add(e.target.id);

            let sum = 0;
            allLabelIds.forEach(labelId => {
                if (labelIds.has(labelId))
                    sum++;
            });

            if (sum === allLabelIds.length)
                ids.add(e.target.getAttribute('data-parent-id'));
        }
        else {
            labelIds.delete(e.target.id);
            ids.delete(e.target.getAttribute('data-parent-id'));
        }

        this.setState({
            checkedOrderedProducts: ids,
            checkedLabels: labelIds
        })
    };

    handleDelete = (e) => {
        this.setState({
            deleteModalVisible: true,
            selectedDelete: parseInt(e.target.id)
        })
    };

    // HANDLE SAVES

    makeApiCall(url, _body = null) {
        let body = !_body ? {
            ordered_products: [...this.state.checkedOrderedProducts],
            labels: [...this.state.checkedLabels],
        } : _body;

        fetch(url, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify(body)
        }).then(response => {
            if (response.ok)
                return window.location.reload();
            throw new Error(response.statusText);
        }).catch(console.error);
    }

    finishSelected = () => {
        this.makeApiCall(window.setToFinishedUrl)
    };

    openSelected = () => {
        this.makeApiCall(window.setToOpenUrl)
    };

    deliverySave = () => {
        const body = {
            ordered_products: [...this.state.checkedOrderedProducts],
            labels: [...this.state.checkedLabels],
            delivery_id: this.state.selectedDelivery
        };
        this.makeApiCall(window.sendToDeliveryUrl, body)
    };

    manufactureSave = () => {
        const body = {
            ordered_products: [...this.state.checkedOrderedProducts],
            labels: [...this.state.checkedLabels],
            manufacture_id: this.state.selectedManufacture
        };
        this.makeApiCall(window.addToManufactureUrl, body)
    };

    transpositionSave = () => {
        const body = {
            ordered_products: [...this.state.checkedOrderedProducts],
            labels: [...this.state.checkedLabels],
            order: this.state.selectedOrder
        };
        this.makeApiCall(window.transpositionUrl, body)
    };

    handlePrint = () => {
        const body = {
            ordered_products: [...this.state.checkedOrderedProducts],
            labels: [...this.state.checkedLabels]
        };

        printLabels(body);
    };

    // RENDERING

    renderTableBody(id, name, status, labels) {
        return (
            <tbody key={id} className="modal-table-body">
            <tr key={id} data-id={id} className=
                {(status === 0 || (labels.length > 0 && labels.filter(e => e.status === 0).length > 0))
                    ? 'border-bottom-bg diff-row parent-row' : 'border-bottom-bg parent-row'}>
                <td className="field-checkbox">
                    <input
                        className="send-checkbox"
                        type="checkbox"
                        checked={this.state.checkedOrderedProducts.has(id.toString())}
                        id={id}
                        onChange={this.handleCheck}
                    />
                </td>
                <td>{name}</td>
                <td>{id}</td>
                <td><span className={"label label-" + status}>{this.statusesMap[status]}</span></td>
                <td>
                    <button
                        id={id}
                        type="button"
                        className="button no icon icon-fa-times modal-delete-btn"
                        onClick={this.handleDelete}
                    />
                </td>
            </tr>
            {labels.length > 0 ?
                <React.Fragment>
                    {labels.map(({id, name, status, parent}) => (
                        <tr key={id} data-id={id}>
                            <td className="field-checkbox">
                                <input
                                    data-parent-id={parent}
                                    checked={this.state.checkedLabels.has(id.toString())}
                                    className="send-checkbox"
                                    type="checkbox"
                                    id={id}
                                    onChange={this.handleCheckLabel}
                                />
                            </td>
                            <td >{name}</td>
                            <td >{id}</td>
                            <td>
                                <span className={"label label-label-" + status}>{this.labelStatusesMap[status]}</span>
                            </td>
                            <td/>
                        </tr>
                    ))}
                </React.Fragment> : null}
            <tr>
                <td className="empty-row" colSpan={3}/>
            </tr>
            </tbody>
        )
    }

    renderOrderedProducts() {
        const {ordered_products} = this.state;
        return (
            <table className="modal-table table inline-table" style={{marginTop: 0}}>
                <thead>
                <tr>
                    <th>
                        <input type="checkbox" onChange={this.selectAll}/>
                    </th>
                    <th style={{textAlign: 'left'}}>Megnevezés</th>
                    <th>Azonosító</th>
                    <th>Státusz</th>
                    <th>Törlés</th>
                </tr>
                </thead>

                {ordered_products.map(({id, name, status, labels}) => (
                    this.renderTableBody(id, name, status, labels)
                ))}

            </table>
        )
    }

    renderDeliveryModal() {
        const {deliveryModalVisible} = this.state;
        const deliveryNotes = window.deliveryNotes;
        return (

            <Modal
                isOpen={deliveryModalVisible}
                style={modalStyle}
                closeTimeoutMS={1000}
                onRequestClose={() => {
                    this.setState({deliveryModalVisible: false});
                }}
            >
                <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
                    <table className="modal-table table inline-table" style={{marginTop: 0}}>
                        <thead>
                        <tr>
                            <th style={{textAlign: 'left'}}>Szállítólevél</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <select name="selectedDelivery" value={this.state.selectedDelivery}
                                        onChange={this.handleChange}>
                                    <option value="">---------</option>
                                    {deliveryNotes.groupBy(x => x.delivery_timestamp).map(({key, items}) => (
                                        <optgroup label={key} key={key}>
                                            {items.map(({id, voucher_number}) => (
                                                <option key={id} value={id + ''}>
                                                    {voucher_number}
                                                </option>
                                            ))}
                                        </optgroup>
                                    ))}
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div className="iziModal-footer" style={{marginTop: 10}}>
                        <button disabled={!this.state.selectedDelivery} type="button"
                                className="button bicolor icon icon-fa-send"
                                onClick={this.deliverySave}>
                            Küldés
                        </button>
                    </div>
                </div>
            </Modal>
        )
    }

    renderManufactureModal() {
        const {manufactureModalVisible} = this.state;
        const manufacturePlans = window.manufacturePlans;
        return (

            <Modal
                isOpen={manufactureModalVisible}
                style={modalStyle}
                closeTimeoutMS={1000}
                onRequestClose={() => {
                    this.setState({manufactureModalVisible: false});
                }}
            >
                <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
                    <table className="modal-table table inline-table" style={{marginTop: 0}}>
                        <thead>
                        <tr>
                            <th style={{textAlign: 'left'}}>Gyártási terv</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <select name="selectedManufacture" value={this.state.selectedManufacture}
                                        onChange={this.handleChange}>
                                    <option value="">---------</option>
                                    {manufacturePlans.groupBy(x => x.date).map(({key, items}) => (
                                        <optgroup label={key} key={key}>
                                            {items.map(({id, identification_number}) => (
                                                <option key={id} value={id + ''}>
                                                    {identification_number}
                                                </option>
                                            ))}
                                        </optgroup>
                                    ))}
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div className="iziModal-footer" style={{marginTop: 10}}>
                        <button disabled={!this.state.selectedManufacture} type="button"
                                className="button bicolor icon icon-fa-send"
                                onClick={this.manufactureSave}>
                            Küldés
                        </button>
                    </div>
                </div>
            </Modal>
        )
    }

    renderTranspositionModal() {
        const {transpositionModalVisible} = this.state;
        const orders = window.orders;
        return (

            <Modal
                isOpen={transpositionModalVisible}
                style={modalStyle}
                closeTimeoutMS={1000}
                onRequestClose={() => {
                    this.setState({transpositionModalVisible: false});
                }}
            >
                <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
                    <table className="modal-table table inline-table" style={{marginTop: 0}}>
                        <thead>
                        <tr>
                            <th style={{textAlign: 'left'}}>Rendelés</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Termékrész választása esetén is átkerül az egész termék!</td>
                        </tr>
                        <tr>
                            <td>
                                <select name="selectedOrder" value={this.state.selectedOrder}
                                        onChange={this.handleChange}>
                                    <option value="">---------</option>
                                    {orders.groupBy(x => x.client).map(({key, items}) => (
                                        <optgroup label={key} key={key}>
                                            {items.map(({id, client, delivery_address, delivery_timestamp}) => (
                                                <option key={id} value={id + ''}>
                                                    {delivery_timestamp} - {delivery_address}
                                                </option>
                                            ))}
                                        </optgroup>
                                    ))}
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div className="iziModal-footer" style={{marginTop: 10}}>
                        <button disabled={!this.state.selectedOrder} type="button"
                                className="button bicolor icon icon-fa-send"
                                onClick={this.transpositionSave}>
                            Küldés
                        </button>
                    </div>
                </div>
            </Modal>
        )
    }

    renderButton(title, onClickFunction, icon, color = "") {
        return (
            <a
                className={color}
                onClick={onClickFunction}>
                <i className={`icon icon-fa-${icon}`}/>
                {title}
            </a>
        )
    }

    render() {
        const {modalVisible} = this.state;
        return (
            <React.Fragment>
                <Modal
                    isOpen={modalVisible}
                    // onRequestClose={this.closeModal}
                    style={modalStyle}
                    closeTimeoutMS={1000}
                    onRequestClose={this.initComponent}
                >
                    <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
                        <div className="">
                            <div className="iziModal-header">
                                Rendelt termékek
                            </div>
                            {this.renderOrderedProducts()}
                        </div>
                    </div>
                    <div className="iziModal-footer">
                        <div className="custom-dropdown up">
                            <button
                                disabled=
                                    {
                                        this.state.checkedLabels.size === 0 &&
                                        this.state.checkedOrderedProducts.size === 0
                                    }
                                className="button dropdown-btn misc bicolor icon icon-fa-caret-up">
                                Lehetőségek
                            </button>
                            <div className="dropdown-content">
                                {this.props.finishedEnabled &&
                                this.renderButton("Készáru raktár", this.finishSelected, "check", "success")}
                                {this.props.openEnabled &&
                                this.renderButton("Nyitott", this.openSelected, "calendar")}
                                <div className="divider"/>
                                {this.renderButton(
                                    "Nyomtatás",
                                    (this.handlePrint),
                                    "qrcode"
                                )}
                                <div className="divider"/>
                                {this.props.deliveryEnabled &&
                                this.renderButton(
                                    "Szállítólevél",
                                    () => {
                                        this.setState({deliveryModalVisible: true});
                                    },
                                    "envelope"
                                )}

                                {this.props.manufactureEnabled &&
                                this.renderButton(
                                    "Gyártási terv",
                                    () => {
                                        this.setState({manufactureModalVisible: true})
                                    },
                                    "folder-open"
                                )}

                                {this.props.transpositionEnabled &&
                                this.renderButton(
                                    "Másik rendelés",
                                    () => {
                                        this.setState({transpositionModalVisible: true})
                                    },
                                    "shopping-cart"
                                )}
                            </div>
                        </div>
                    </div>
                </Modal>
                {this.props.deliveryEnabled && this.renderDeliveryModal()}
                {this.props.manufactureEnabled && this.renderManufactureModal()}
                {this.props.transpositionEnabled && this.renderTranspositionModal()}
                <DeleteModal modalVisible={this.state.deleteModalVisible}
                             close={() => this.setState({
                                 deleteModalVisible: false,
                                 selectedDelete: 0
                             })}
                             title="Törlés" content="Biztosan törölni szeretnéd a terméket?"
                             id={this.state.selectedDelete}/>
            </React.Fragment>
        );
    }
}

Details.propTypes = {
    initFunction: PropTypes.func.isRequired,

    deliveryEnabled: PropTypes.bool,
    finishedEnabled: PropTypes.bool,
    openEnabled: PropTypes.bool,
    manufactureEnabled: PropTypes.bool,
    transpositionEnabled: PropTypes.bool
};

Details.defaultProps = {
    deliveryEnabled: true,
    finishedEnabled: true,
    openEnabled: true,
    manufactureEnabled: true,
    transpositionEnabled: true
};

export default Details
