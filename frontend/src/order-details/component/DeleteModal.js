import React, {Component} from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

import {modalStyle, getCookie} from './utils';

class DeleteModal extends Component {
    state = {
        waiting: false,
    };

    constructor(props) {
        super(props);
    }

    handleApiCall = () => {
        this.setState({waiting: true});

        fetch(`/api/1/ordered-product/delete/${this.props.id}/`, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
        }).then(response => {
            if (response.ok)
                return window.location.reload();
            throw new Error(response.statusText);
        }).catch(console.error);
    };

    render() {
        const {modalVisible} = this.props;
        return (
            <Modal
                isOpen={modalVisible}
                // onRequestClose={this.closeModal}
                style={modalStyle}
                closeTimeoutMS={1000}
                onRequestClose={() => {
                    this.props.close()
                }}
            >
                <div className="dialog">
                    <div className="dialog-header">
                        {this.props.title}
                    </div>
                    <div className="dialog-content">
                        <p>
                            {this.props.content}
                        </p>
                    </div>
                </div>
                <div className="dialog-footer">
                    {this.state.waiting ? (
                        <button className="button icon icon-spinner"/>
                    ) : (
                        <button
                            type="button"
                            className="button bicolor icon icon-fa-check"
                            onClick={this.handleApiCall}
                        >
                            Mehet
                        </button>
                    )}
                </div>
            </Modal>
        );
    }
}

DeleteModal.propTypes = {
    modalVisible: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
};

export default DeleteModal;
