import React, {Component} from 'react';
import DateTimePicker from 'react-xdsoft-datetimepicker';
import DateFormat from 'dateformat';

import {setToReady} from './interact';
import Modal from 'react-modal';

export const modalStyle = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(71, 71, 71, 0.9)',
        zIndex: 100,
    },
    content: {
        position: 'absolute',
        top: '15%',
        left: '25%',
        width: '50%',
        bottom: 'unset',
        right: 'unset',
        border: '1px solid #ccc',
        background: '#fff',
        overflow: 'auto',
        WebkitOverflowScrolling: 'touch',
        borderRadius: '4px',
        outline: 'none',
        padding: '20px',
        maxHeight: '75vh'
    }
};

Array.prototype.updateItem = function (selector, getNewItem) {
    return this.map(x => selector(x) ? getNewItem(x) : x);
};

Array.prototype.groupBy = function (selector) {
    const obj = this.reduce((result, item) => {
        const key = selector(item);
        result[key] = result[key] || [];
        result[key].push(item);
        return result;
    }, {});
    return Object.keys(obj).map(key => ({
        key, items: obj[key]
    }));
};

export function getCookie(name) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}


export default class extends Component {
    initState = {
        products: [],
        modalVisible: false,
        selectPlan: false,
        manufacture_plan: {
            id: null,
            identifictaion_number: null,
            date: null
        },
        isLoading: false,
        error: null,

        newManufacturePlan: {
            visible: true,
            number: "",
            date: DateFormat(new Date(), 'yyyy-m-d')
        }

    };

    state = this.initState;

    constructor(props) {
        super(props);
        setToReady(this.handleProducts);
    }

    setState(nextState, callback) {
        console.log({
            currentState: this.state,
            nextState
        });
        super.setState(nextState, callback);
    }

    handleProducts = (products) => {
        this.setState({
            products: products.filter(({checked}) => checked).map(({quantity, ...rest}) => ({
                ...rest,
                quantity,
                originalQuantity: quantity
            })),
            modalVisible: true
        })
    };

    handleClose = () => {
        this.setState(this.initState);
    };

    handleChangeQuantity = (id, quantity) => {
        this.setState({
            products: this.state.products.updateItem(
                ({id: currentId}) => id === currentId,
                props => ({
                    ...props,
                    quantity
                }))
        })
    };

    handleSave = () => {
        let body = {
            id: this.state.manufacture_plan.id,
            product_orders: this.state.products.map(({id, quantity}) => ({
                id, quantity
            }))
        };
        if (this.state.newManufacturePlan.visible
            && this.state.newManufacturePlan.number.length > 0
            && this.state.newManufacturePlan.date !== null) {
            body['new_manufacture_plan'] = this.state.newManufacturePlan;
            delete body['id'];
        }

        (this.state.manufacture_plan.id > 0 ||
        (this.state.newManufacturePlan.visible
        && this.state.newManufacturePlan.number.length > 0
        && this.state.newManufacturePlan.date !== null))
        && fetch(window.submitUrl, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify(body)
        }).then(response => {
            if (response.ok)
                return window.location.reload();
            throw new Error(response.statusText);
        }).catch(console.error);
    };

    handleSelectManufacturePlan = ({target: {value}}) => {
        this.setState({
            manufacture_plan: {
                ...this.state.manufacture_plan,
                id: parseInt(value)
            }
        })
    };

    switchNew = () => {
        this.setState(prevState => ({
            newManufacturePlan: {
                ...prevState.newManufacturePlan,
                visible: !prevState.newManufacturePlan.visible
            }
        }))
    };

    renderProducts() {
        const {products} = this.state;
        return (
            <table className="modal-table table inline-table" style={{marginTop: 0}}>
                <thead>
                <tr>
                    <th style={{textAlign: 'left'}}>Megnevezés</th>
                    <th>Mennyiség</th>
                </tr>
                </thead>
                <tbody>

                {products.map(({id, name, quantity, originalQuantity}) => (
                    <tr key={id}>
                        <td>{name}</td>
                        <td>
                            <input
                                type="number"
                                value={quantity}
                                max={originalQuantity}
                                min={1}
                                onChange={(e) => this.handleChangeQuantity(id, parseInt(e.target.value))}
                            />
                        </td>
                    </tr>
                ))}

                </tbody>
            </table>
        )
    }

    renderManufacturePlanForm() {
        DateTimePicker.setLocale('hu');
        const manufacturePlans = window.manufacturePlans;
        const {manufacture_plan: {id, identification_number, date}} = this.state;
        // const groupedPlans = manufacturePlans.reduce((result, item) => {
        //     result[item.date] = result[item.date] || [];
        //     result[item.date].push(item);
        //     return result;
        // }, {});

        return (
            <table className="modal-table table inline-table" style={{marginTop: 0}}>
                <thead>
                <tr>
                    <th style={{textAlign: 'left', width: '9%'}}>Gyártási terv</th>
                    <th>
                        { !this.state.newManufacturePlan.visible ?
                            (
                                <button style={{float: 'right'}} type="button"
                                        className="button bicolor icon icon-fa-plus"
                                        onClick={this.switchNew}>
                                    Új
                                </button>
                            ) :
                            (
                                <button style={{float: 'right'}} type="button"
                                        className="button bicolor icon icon-fa-history"
                                        onClick={this.switchNew}>
                                    Meglévő
                                </button>
                            )}
                    </th>
                </tr>
                </thead>
                <tbody>
                { !this.state.newManufacturePlan.visible ?
                    (
                        <tr>
                            <td colSpan={2}>
                                <select name="mp_id"
                                        value={id || ''}
                                        onChange={this.handleSelectManufacturePlan}>
                                    <option value="0">---------------</option>
                                    {manufacturePlans.groupBy(x => x.date).map(({key, items}) => (
                                        <optgroup label={key} key={key}>
                                            {items.map(({id, identification_number}) => (
                                                <option key={id} value={id + ''}>
                                                    {identification_number}
                                                </option>
                                            ))}
                                        </optgroup>
                                    ))}
                                </select>
                            </td>
                        </tr>
                    ) :
                    (
                        <React.Fragment>
                            <tr>
                                <td>Munkaszám</td>
                                <td>Gyártás napja</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text"
                                           value={this.state.newManufacturePlan.number}
                                           onChange={(e) => {
                                               const value = e.target.value;
                                               this.setState(prevState =>
                                                   ({
                                                       newManufacturePlan: {
                                                           ...prevState.newManufacturePlan,
                                                           number: value
                                                       }
                                                   }))
                                           }}/>
                                </td>
                                <td>
                                    <DateTimePicker
                                        displayDateFormat='YYYY-MM-DD'
                                        timepicker={false}
                                        value={this.state.newManufacturePlan.date}
                                        onChange={(e) => {
                                            const d = e === null ? null : e.toDate();
                                            this.setState(prevState =>
                                                ({
                                                    newManufacturePlan: {
                                                        ...prevState.newManufacturePlan,
                                                        date: d === null ?
                                                            null :
                                                            `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`
                                                    }
                                                }))
                                        }}
                                    />
                                </td>
                            </tr>
                        </React.Fragment>
                    )}
                </tbody>
            </table>
        )
    }

    render() {
        const {modalVisible} = this.state;
        if (this.state.products.length > 0)
            return (
                <Modal
                    isOpen={modalVisible}
                    // onRequestClose={this.closeModal}
                    style={modalStyle}
                    closeTimeoutMS={1000}
                    onRequestClose={() => {
                        this.setState({modalVisible: false});
                    }}
                >
                    <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
                        <div className="">
                            <div className="iziModal-header">
                                Kiválasztottak küldése gyártásra
                            </div>
                            {this.renderProducts()}
                            {this.renderManufacturePlanForm()}
                            <div className="iziModal-footer" style={{marginTop: 10}}>
                                <button type="button" className="button bicolor icon icon-fa-send"
                                        onClick={this.handleSave}
                                        disabled={!this.state.newManufacturePlan.visible &
                                        (this.state.manufacture_plan.id === null ||
                                        this.state.manufacture_plan.id === 0) ||
                                        (this.state.newManufacturePlan.visible &&
                                        (this.state.newManufacturePlan.date === null ||
                                        this.state.newManufacturePlan.number.length === 0))}
                                >
                                    Küldés
                                </button>
                                <button type="button" className="button no bicolor icon icon-fa-times"
                                        onClick={this.handleClose}>
                                    Mégsem
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal>
            );
        return (null)
    }
}
