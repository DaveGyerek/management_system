import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import {AppContainer} from 'react-hot-loader'

import registerServiceWorker from './order-details/registerServiceWorker';
import Details from './order-details/component/Details'
import {details} from './order-details/interact'

window.addEventListener('load', function () {
    const root = document.querySelector('#order-details-root');
    const render = () => {
        Modal.setAppElement(root);
        ReactDOM.render(
            <React.Fragment>
                <AppContainer>
                    <Details initFunction={details} openEnabled={typeof window.setToOpenUrl !== "undefined"}/>
                </AppContainer>
            </React.Fragment>,
            root,
        )
    };
    render();

    if (module.hot) {
        module.hot.accept('./App', () => {
            render()
        })
    }
});


registerServiceWorker();
