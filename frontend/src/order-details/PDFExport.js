import React, {Component} from 'react';
import {clients} from './interact';
import Modal from 'react-modal';
import './index.css';
import {modalStyle, getCookie} from './App';

export default class extends Component {
  initState = {
    clients: [],
    stores: [],

    selectedClient: {id: 0},
    selectedStore: {id: 0},
    ready_made: false,

    modalVisible: false,
    isLoading: false,
    error: null,
  };

  state = this.initState;

  constructor(props) {
    super(props);
    clients(this.handleClients);
  }

  handleClients = (context) => {
    this.setState({
      clients: context.clients,
      modalVisible: true
    })
  };

  handleSelectClient = ({target: {value}}) => {
    value = parseInt(value);
    if (value === 0)
      this.setState({
        selectedClient: {id: 0},
        stores: []
      });
    else {
      const client = this.state.clients.find(e => e.id === value);
      this.setState({
        selectedClient: client,
        stores: client.stores
      })
    }
  };

  handleSelectStore = ({target: {value}}) => {
    value = parseInt(value);
    if (value === 0)
      this.setState({
        selectedStore: {id: 0},
      });
    else {
      const store = this.state.selectedClient.stores.find(e => e.id === value);
      this.setState({
        selectedStore: store,
      })
    }
  };

  setState(nextState, callback) {
    console.log({
      currentState: this.state,
      nextState
    });
    super.setState(nextState, callback);
  }

  handleSave = () => {
    let data = {
      client: this.state.selectedClient.id,
      store: this.state.selectedStore.id,
      ready_made: this.state.ready_made
    };
    console.log(JSON.stringify(data));

    fetch(window.pdfUrl, {
      credentials: 'same-origin',
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': getCookie('csrftoken')
      },
      body: JSON.stringify(data)
    }).then(response => {
      if (response.ok)
        return window.location.reload();
      throw new Error(response.statusText);
    }).catch(console.error);
  };

  renderForm() {
    return (
      <table className="modal-table table inline-table" style={{marginTop: 0}}>
        <tbody>
        <tr>
          <td style={{width: '15%'}}>
            Ügyfél
          </td>
          <td>
            <select name="dn_id"
                    value={this.state.selectedClient.id}
                    onChange={this.handleSelectClient}
            >
              <option value="0">Összes</option>
              {this.state.clients.map((client) =>
                <option value={client.id} key={client.id}>{client.name}</option>
              )}
            </select>
          </td>
        </tr>
        <tr>
          <td>
            Üzlet
          </td>
          <td>
            <select
              disabled={this.state.stores.length < 1}
              name="dn_id"
              value={this.state.selectedStore.id}
              onChange={this.handleSelectStore}
            >
              <option value="0">Összes</option>
              {this.state.stores.map((store) =>
                <option value={store.id} key={store.id}>{store.name} ({store.delivery_address})</option>
              )}
            </select>
          </td>
        </tr>
        <tr>
          <td style={{width: '30%'}}>
            Foglalt készáruk hozzáadása
          </td>
          <td style={{textAlign: 'left'}}>
            <input
              name="duplicate"
              type="checkbox"
              checked={this.state.ready_made}
              onChange={(e) => this.setState({ready_made: e.target.checked})}
            />
          </td>
        </tr>
        <tr>
          <td colSpan={2}>
            <a type="button" className="button bicolor icon icon-fa-send"
              // onClick={this.handleSave}
               href={`${window.pdfUrl}/${this.state.selectedClient.id}/${this.state.selectedStore.id}?ready_made=${this.state.ready_made}`}
               target="_blank"
            >
              Export
            </a>
          </td>
        </tr>
        </tbody>
      </table>
    )
  }

  render() {
    const {modalVisible} = this.state;
    return (
      <Modal
        isOpen={modalVisible}
        // onRequestClose={this.closeModal}
        style={modalStyle}
        closeTimeoutMS={1000}
        onRequestClose={() => {
          this.setState({modalVisible: false});
        }}
      >
        <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
          <div className="">
            <div className="iziModal-header">
              PDF Export
            </div>
            {this.renderForm()}
          </div>
        </div>
      </Modal>
    );
  }
}
