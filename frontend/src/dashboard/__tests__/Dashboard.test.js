import App from '../App'
import React from 'react'
import ReactDOM from 'react-dom'
import {shallow, mount} from 'enzyme';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});


it('renders doughnut chart wrapper successfully', () => {
  const wrapper = mount(<App />);
  expect(wrapper.exists(".doughnut-chart-container")).toEqual(true);

});


it('renders line chart wrapper successfully', () => {
  const wrapper = mount(<App />);
  expect(wrapper.exists(".line-chart-container")).toEqual(true);

});


it('renders bar chart wrapper successfully', () => {
  const wrapper = mount(<App />);
  expect(wrapper.exists(".bar-chart-container")).toEqual(true);

});


