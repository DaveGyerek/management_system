import React from 'react'
import ReactDOM from 'react-dom'
import App from './dashboard/App'

window.addEventListener('load', function () {
    const ConnectedApp = (App) => (
        <App />
    )

    const root = document.querySelector('.form-wrapper')
    ReactDOM.render(ConnectedApp(App), root)

    if (module.hot) {
        module.hot.accept('./', () => {
            const NextApp = require('./dashboard/App').default
            const NextConnectedApp = ConnectedApp(NextApp)
            ReactDOM.render(
                NextConnectedApp,
                root
            )
        })
    }
})