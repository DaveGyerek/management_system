import {Doughnut, Line, Bar} from 'react-chartjs-2';
import React, {Component} from 'react';
import {Grid, Paper} from '@material-ui/core';
import classNames from 'classnames';

const getDoughnutData = () => {
  const data = window.data.product_types
  const labels = data.map(product_type => product_type.name)
  const values = data.map(product_type => product_type.count)
  const colors = Array.from(Array(10), () => "#" + ((1 << 24) * Math.random() | 0).toString(16))

  return {
    labels: labels,
    datasets: [{
      data: values,
      backgroundColor: colors,
      hoverBackgroundColor: colors,
    }]
  }
}

const getLineData = () => {
  const data = window.data.last_days
  const labels = data.map(day => day.date).reverse()
  const values = data.map(day => day.count).reverse()

  return {
    labels: labels,
    datasets: [
      {
        label: 'Manufactured goods count/day',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: values,
      }
    ]
  }
}

const getBarData = () => {
  const data = window.data.last_months
  const labels = data.map(day => day.date).reverse()
  const values = data.map(day => day.count).reverse()

  return {
    labels: labels,
    datasets: [
      {
        label: 'Manufactured goods/month',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: values,
      }
    ]
  }
}


export default class extends Component {
  render() {
    return (
      <Grid container spacing={24}>
        <Grid item xs={6}>
          <Paper className={"doughnut-chart-container"}>
            <Doughnut data={getDoughnutData()}/>
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={"bar-chart-container"}>
            <Bar
              data={getBarData()}
              options={{
                maintainAspectRatio: true
              }}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={"line-chart-container"}>
            <Line data={getLineData()}/>
          </Paper>
        </Grid>
      </Grid>

    );
  }
}
