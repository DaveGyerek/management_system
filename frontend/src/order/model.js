import getStore from './store'

const MODEL = {
    id: {
      defaultValue: 1,
    },
    parts: {
        defaultValue: [],
    },
    product: {
        defaultValue: 1,
        column: 'Termék',
        width: 20,
    },
    quantity: {
        defaultValue: 1,
        column: 'Mennyiség',
        width: 10,
    },
    price: {
        defaultValue: 1000,
        column: 'Ár',
        width: 10,
    },
    custom_client_po: {
        defaultValue: '',
        column: 'Egyedi ügyfél azonosító',
        width: 10,
    },
    note: {
        defaultValue: '',
        column: 'Megjegyzés',
        width: 40,
    },
};

const keys = Object.keys(MODEL);
let idCounter = 1;

export function columns() {
    return keys.map(key => (
        {
            column: MODEL[key].column,
            width: MODEL[key].width
        }
    )).filter(x => x.column);
}

export function getProduct(productId=null) {
    const store = getStore();
    const {data:{products, prices, materials}} = store.getState();
    const product = productId ? products.find(({id}) => id === productId) : products[0];
    const price = prices[product.id];
    const materialPrice = id => {
        const material = materials.find(x => x.id === id);
        return parseFloat(material ? material.price : 0);
    };
    const materialNetPrice = id => {
        const material = materials.find(x => x.id === id);
        return parseFloat(material ? material.net_price : 0);
    };
    const materialVat = id => {
        const material = materials.find(x => x.id === id);
        return parseFloat(material ? material.vat : 0);
    };
    return {
        ...product,
        price,
        parts: product.parts.map(({material, ...rest}) => ({
            ...rest,
            material,
            price: materialPrice(material),
            net_price: materialNetPrice(material),
            vat: materialVat(material)
        }))
    }
}

export function create() {
    let model = {};

    for (const index in keys)
        if(keys.hasOwnProperty(index))
            model[keys[index]] = MODEL[keys[index]].defaultValue;

    const {id, parts, price} = getProduct();

    model.id = idCounter++;
    model.product = id;
    model.parts = parts;
    model.price = price;

    return model;
}