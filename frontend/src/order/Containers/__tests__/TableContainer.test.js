import App from '../../App'
import React from 'react'
import ReactDOM from 'react-dom'
import {shallow, mount} from 'enzyme'
import store from '../../Store'


it('button visible', () => {
  const wrapper = mount(<App />);
  expect(wrapper.exists(".button")).toEqual(true);
});


