import React from 'react';
import {connect} from 'react-redux';
import {selectProduct} from '../reducers/state';
import ProductDetailsModal from "../Components/ProductDetailsModal";

export default connect(({state: {selectedProductId, products}, data: {products: dataProducts}}) => ({
        model: selectedProductId && products.find(x => x.id === selectedProductId),
        productName: selectedProductId && dataProducts.find(x => x.id === products.find(y => y.id === selectedProductId).product).name
    }), {
        selectProduct
    }
)(ProductDetailsModal);