import React from 'react';
import {connect} from 'react-redux';
import {Table, TableBody, TableHead} from "../Components/Table";
import Form from "../Components/Form";
import {moveProductDown, moveProductUp, removeProduct, selectProduct, updateProduct} from '../reducers/state';
import {columns} from '../model';

function TableContainer({
                            products,
                            dataProducts,
                            updateProduct,
                            removeProduct,
                            moveProductDown,
                            moveProductUp,
                            selectProduct
                        }) {
    return (
        <Table>
            <TableHead columns={columns()}/>
            <TableBody>
                {products.map((x, i) => (
                    <Form
                        key={x.id}
                        model={x}
                        index={i}
                        count={products.length}
                        dataProducts={dataProducts}
                        handleChange={updateProduct}
                        handleRemove={removeProduct}
                        moveProductDown={moveProductDown}
                        moveProductUp={moveProductUp}
                        selectProduct={selectProduct}
                    />
                ))}
            </TableBody>
        </Table>
    )
}

export default connect(({state: {products}, data: {products: dataProducts}}) => ({
        products, dataProducts
    }), ({
        updateProduct, removeProduct, moveProductDown, moveProductUp, selectProduct
    })
)(TableContainer);