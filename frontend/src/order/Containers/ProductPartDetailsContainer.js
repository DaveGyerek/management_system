import React from 'react';
import {connect} from 'react-redux';
import {updateProduct} from '../reducers/state';
import PartsForm from "../Components/PartsForm";


function ProductPartDetailsTable({title, parts, selectedProduct, dataMaterials, updateProduct}) {
    return (
        <React.Fragment>
            {title && <h2 style={{marginLeft: 10}}>{title}</h2>}
            <table className="modal-table table inline-table">
                <thead>
                <tr>
                    <th>Megnevezés</th>
                    <th>Alapanyag</th>
                    <th>Mennyiség</th>
                    <th>Egységár</th>
                    <th>Összesen</th>
                </tr>
                </thead>
                <tbody>
                {parts.map(({id, ...rest}) => (
                    <PartsForm
                        key={id}
                        selectedProduct={selectedProduct}
                        model={{id, ...rest}}
                        dataMaterials={dataMaterials}
                        handleChange={updateProduct}
                    />
                ))}
                </tbody>
            </table>
        </React.Fragment>
    )
}

function ProductPartDetails({
                                selectedProduct,
                                dataMaterials,
                                updateProduct,
                            }) {
    const textures = selectedProduct.parts
        .filter(({material}) =>
            material &&
            dataMaterials.find(({id}) => material == id).category__name === 'Szövet');
    const other = selectedProduct.parts
        .filter(({material}) =>
            !material ||
            dataMaterials.find(({id}) => material == id).category__name !== 'Szövet');
    return (
        <React.Fragment>
            {
                textures.length > 0 &&
                <ProductPartDetailsTable
                    title={textures.length > 0 && other.length > 0 && 'Szövetek'}
                    parts={textures}
                    selectedProduct={selectedProduct}
                    dataMaterials={dataMaterials}
                    updateProduct={updateProduct}
                />
            }
            {
                other.length > 0 &&
                <ProductPartDetailsTable
                    title={textures.length > 0 && other.length > 0 && 'Egyéb'}
                    parts={other}
                    selectedProduct={selectedProduct}
                    dataMaterials={dataMaterials}
                    updateProduct={updateProduct}
                />
            }
        </React.Fragment>
    )
}

export default connect(({state: {products, selectedProductId}, data: {materials: dataMaterials}}) => ({
        selectedProduct: selectedProductId ? products.find(x => x.id === selectedProductId) : {parts: []},
        dataMaterials
    }), ({
        updateProduct
    })
)(ProductPartDetails);