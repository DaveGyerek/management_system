import React from 'react';
import {Control, LocalForm} from 'react-redux-form';
import {IconButton, BIN, ORDER_DOWN, ORDER_UP, COGS} from "./Button";


export default class extends React.Component {

    moveDown = () => {
        const {moveProductDown, model: {id}} = this.props;
        moveProductDown(id);
    };

    moveUp = () => {
        const {moveProductUp, model: {id}} = this.props;
        moveProductUp(id);
    };

    handleChange = (values) => {
        const {handleChange, model: {id}} = this.props;
        handleChange(id, {
            ...values,
            product: parseInt(values.product)
        });
    };

    handleEdit = () => {
        const {selectProduct, model: {id}} = this.props;
        selectProduct(id);
    };

    handleRemove = () => {
        const {model: {id}, handleRemove} = this.props;
        handleRemove(id);
    };

    changedModelExtra = (id) => {
        const {model: {product, parts: currentParts}, dataProducts} = this.props;

        if (product !== id)
            return "";
        const {parts: originalParts} = dataProducts.find(({id: productId}) => productId === id);
        for (let i = 0; i < originalParts.length; i++) {
            if (originalParts[i].material != currentParts[i].material)
                return "*";
            if (originalParts[i].material_quantity != currentParts[i].material_quantity)
                return "*";
        }
        return "";
    };

    renderProductSelectOptions() {
        const {dataProducts} = this.props;

        const groupedProducts = dataProducts.reduce((result, item) => {
            result[item.product_type] = result[item.product_type] || [];
            result[item.product_type].push(item);
            return result;
        }, {});

        return Object.keys(groupedProducts)
            .map(label => (
                <optgroup key={label} label={label}>
                    {groupedProducts[label].map(({id, name}) => (
                        <option key={id} value={id + ''}>
                            {name}
                            {this.changedModelExtra(id)}
                        </option>
                    ))}
                </optgroup>
            ));
    }

    render() {
        const {model} = this.props;
        const notOk = !!model.parts.find(x => !x.material);

        return (
            <tr style={{background: notOk ? '#e7505a' : 'unset'}}>
                <td>
                    <div className="input">
                        <select
                            value={model.product}
                            onChange={(e) => this.handleChange({...model, product: e.target.value})}
                        >
                            {this.renderProductSelectOptions()}
                        </select>
                    </div>
                </td>
                <td>
                    <div className="input">
                        <input
                            className="order-input"
                            type="number"
                            name="quantity"
                            value={model.quantity}
                            onChange={e => this.handleChange({...model, quantity: parseInt(e.target.value)})}
                        />
                    </div>
                </td>
                <td>
                    <div className="input">
                        <input
                            className="order-input"
                            type="number"
                            name="price"
                            value={model.price}
                            onChange={e => this.handleChange({...model, price: parseInt(e.target.value)})}
                        />
                    </div>
                </td>
                <td>
                    <div className="input">
                        <input
                            className="order-input"
                            name="custom_client_po"
                            value={model.custom_client_po}
                            onChange={e => this.handleChange({...model, custom_client_po: e.target.value})}
                        />
                    </div>
                </td>
                <td>
                    <div className="input">
                        <input
                            className="order-input"
                            type="text-area"
                            name="po_note"
                            value={model.note}
                            onChange={e => this.handleChange({...model, note: e.target.value})}
                        />
                    </div>
                </td>
                <td className="text-center">
                    <IconButton icon={COGS} onClick={this.handleEdit}/>
                    <IconButton icon={BIN} onClick={this.handleRemove}/>
                    {/*{index !== count - 1 && count > 1 && <IconButton icon={ORDER_DOWN} onClick={this.moveDown}/>}*/}
                    {/*{index !== 0 && count > 1 && <IconButton icon={ORDER_UP} onClick={this.moveUp}/>}*/}
                </td>
            </tr>
        )
    }
}