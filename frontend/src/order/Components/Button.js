import React from 'react';


export const ORDER_UP = 'order-up';
export const ORDER_DOWN = 'order-down';
export const BIN = 'bin';
export const COGS = 'cogs';

export function IconButton({icon, title, onClick}) {
    return (
        <button
            type="button"
            className={`button icon text-replace white icon-${icon}`}
            title={title}
            onClick={onClick}
        >
            {title}
        </button>
    )
}

function _addButtonScroll() {
    window.scrollTo(0, document.body.scrollHeight);
    return true;
}

export function AddButton({title, onClick}) {
    return (
        <div className="add">
            <button type="button" className={`button bicolor icon icon-plus`} onClick={() => _addButtonScroll() && onClick()}>
                {title}
            </button>
        </div>
    )
}