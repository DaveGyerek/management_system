import React from 'react';

export function Table({children, className="table empty order-table inline-table"}) {
    return (
        <table className={className}>
            {children}
        </table>
    )
}

export function TableHead({columns}) {
    return (
        <thead>
        <tr>
            {columns.map((({column, width}, i) => (
                <th key={i} scope="col" style={{width: `${width}%`}}>{column}</th>
            )))}
            <th scope="col">
                Műveletek
            </th>
        </tr>
        </thead>
    )
}

export function TableBody({children}) {
    return (
        <tbody>
        {children}
        </tbody>
    )
}