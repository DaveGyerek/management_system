import React from 'react';


export default class extends React.Component {

    handleChange = (values) => {
        const {handleChange, model: {id: partId}, selectedProduct, dataMaterials} = this.props;
        const material = values.material && dataMaterials.find(x => x.id == values.material);
        const parts = selectedProduct.parts.map(({id, ...rest}) => (
            id === partId ? {...values, price: material ? parseFloat(material.price) : 0} : {id, ...rest, price: rest.price || 0}
        ));
        handleChange(selectedProduct.id, selectedProduct, parts);
    };

    renderMaterialSelectOptions() {
        const {dataMaterials} = this.props;

        const groupedMaterials = dataMaterials.reduce((result, item) => {
            result[item.category__name] = result[item.category__name] || [];
            result[item.category__name].push(item);
            return result;
        }, {});

        return [].concat.apply([
            <option value="" key="none">
                --------
            </option>
        ], Object.keys(groupedMaterials)
            .map(label => (
                <optgroup key={label} label={label}>
                    {groupedMaterials[label].map(({id, name}) => (
                        <option key={id} value={id + ''}>
                            {name}
                        </option>
                    ))}
                </optgroup>
            )));
    }

    render() {
        const {model, dataMaterials} = this.props;
        const material = model.material && dataMaterials.find(x => x.id == model.material);
        return (
            <tr
                style={{background: material ? 'unset' : '#e7505a'}}
            >
                <td>
                    {model.name}{model.starred && <span>&nbsp;<i className="icon icon-fa-star"/></span>}
                </td>
                <td>
                    <div className="input">
                        <select
                            value={model.material || ''}
                            onChange={(e) => this.handleChange({...model, material: e.target.value || null})}
                        >
                            {this.renderMaterialSelectOptions()}
                        </select>
                    </div>
                </td>
                <td>
                    <div className="input">
                        <input
                            type="number"
                            name="material_quantity"
                            value={model.material_quantity}
                            onChange={e => this.handleChange({...model, material_quantity: parseInt(e.target.value)})}
                        />
                    </div>
                </td>
                <td>
                    {material ? `${parseInt(material.price)} Ft` : '-'}
                </td>
                <td>
                    {material ? `${material.price*model.material_quantity} Ft` : '-'}
                </td>
            </tr>
        )
    }
}