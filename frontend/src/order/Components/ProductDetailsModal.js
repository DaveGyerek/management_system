import React from 'react';
import Modal from 'react-modal';

const modalStyle = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(71, 71, 71, 0.9)',
        zIndex: 100,
    },
    content: {
        position: 'absolute',
        top: '15%',
        left: '25%',
        width: '50%',
        bottom: 'unset',
        right: 'unset',
        border: '1px solid #ccc',
        background: '#fff',
        overflow: 'auto',
        WebkitOverflowScrolling: 'touch',
        borderRadius: '4px',
        outline: 'none',
        padding: '20px',
        maxHeight: '75vh',
    }
};

export default class extends React.Component {
    state = {
        showErrorMessage: false
    };

    handleSave = () => {
        const {model, selectProduct} = this.props;
        const noMaterial = !!model.parts.find(x => !x.material);
        if (noMaterial) {
            this.setState({showErrorMessage: true})
        } else {
            this.setState({showErrorMessage: false});
            selectProduct(null, true);
        }
    };

    render() {
        const {showErrorMessage} = this.state;
        const {model, productName, selectProduct, children} = this.props;
        return (
            <Modal
                isOpen={!!model}
                contentLabel={productName}
                onRequestClose={() => selectProduct(null)}
                style={modalStyle}
                closeTimeoutMS={500}
            >
                <div className="" style={{display: "block", position: "unset"}}>
                    <div className="iziModal-content">
                        <div className="iziModal-header" style={{padding: "14px 18px 15px"}}>
                            {productName} anyagnormája
                            {/*<div className="iziModal-header-buttons">*/}
                                {/*<button*/}
                                    {/*className="iziModal-button iziModal-button-close"*/}
                                    {/*onClick={e => e.stopPropagation() || selectProduct(null)}>*/}
                                {/*</button>*/}
                            {/*</div>*/}
                        </div>
                        {showErrorMessage && <span>Minden részhez kötelező az alapanyag megadása</span>}
                        {children}
                        <div className="iziModal-footer">
                            <button type="button" className="button" onClick={this.handleSave}>
                                Mentés
                            </button>
                            <button type="button" className="button no" onClick={() => selectProduct(null)}>
                                Mégsem
                            </button>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}