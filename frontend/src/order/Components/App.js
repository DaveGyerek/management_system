import React from 'react';
import {connect} from 'react-redux';

import {AddButton} from "./Button"
import TableContainer from "../Containers/TableContainer";
import {addProduct} from "../reducers/state";
import Modal from '../Containers/ProductDetailsModalContainer';
import PartsForm from '../Containers/ProductPartDetailsContainer';


function Order({addProduct}) {
    return (
        <div className="order-root">
            <TableContainer/>
            <AddButton title="Termék hozzáadása" onClick={addProduct}/>
            <Modal>
                <PartsForm/>
            </Modal>
        </div>
    )
}

export default connect(({}) => ({}), {
    addProduct
})(Order);