import createStore from "./store";
import {fetchClient} from "./reducers/data";

function outputState(store) {
    const jsonInput = document.querySelector('input[name="product_orders"]') || (() => {
        const input = document.createElement('input');
        input.type = "hidden";
        input.name = "product_orders";
        try {
            document.querySelector(".content form").appendChild(input);
        } catch (TypeError) {
            document.querySelector(".form-wrapper").appendChild(input);
        }
        return input
    })();
    const updateForm = () => {
        jsonInput.value = JSON.stringify(store.getState().state.products);
    };
    updateForm();
    store.subscribe(updateForm);
}

function checkClients(store) {
    try {

        const clientSelect = document.querySelector('#id_client'),
            storeSelect = document.querySelector('#id_store'),
            deliveryAddressInput = document.querySelector('#id_delivery_address'),
            billingAddressInput = document.querySelector('#id_billing_address'),
            orderRoot = document.querySelector('.order-root');

        !clientSelect.value && orderRoot.classList.add('hidden');
        const setFields = ({billing_address, delivery_address, stores}) => {
            billingAddressInput.value = billing_address;
            while (storeSelect.lastChild) storeSelect.removeChild(storeSelect.lastChild);
            for (let i in stores) {
                if (stores.hasOwnProperty(i)) {
                    const {id, name, delivery_address} = stores[i];
                    const option = document.createElement('option');
                    option.value = id;
                    option.innerHTML = name;
                    option.setAttribute('data-address', delivery_address || '');
                    storeSelect.appendChild(option);
                }
            }
            storeSelect.value = stores.length > 0 && stores[0].id;
            deliveryAddressInput.value = stores.length > 0 && stores[0].delivery_address
                ? stores[0].delivery_address
                : delivery_address ? delivery_address : billing_address;
        };
        clientSelect.onchange = function () {
            if (clientSelect.value) {
                fetchClient(clientSelect.value, setFields)(store.dispatch);
                orderRoot.classList.remove('hidden');
            } else {
                orderRoot.classList.add('hidden');
            }

        };
        storeSelect.onchange = function (e) {
            const address = e.target.selectedOptions[0].dataset['address'];

            if (address.length > 0)
                deliveryAddressInput.value = address;
        }
    }

    catch (TypeError) {

    }
}

export function getInteractiveStore() {
    let initial = '{"products": [{"id": 2, "name": "Hot rod leather vinyl front power bucket seat", "product_type": "Bench seat", "price": "300000.0000", "parts": [{"id": 2, "name": "asdasdsa", "short_name": "asdsad", "material": 17, "material_quantity": 11.0, "sort_order": 0}, {"id": 3, "name": "asdsa", "short_name": "asdasd", "material": 20, "material_quantity": 1.0, "sort_order": 1}]}], "materials": [{"id": 4, "name": "Ash", "unit": 3, "category": 6, "category__name": "Wood", "price": "4000.0000", "net_price": "3149.6100", "vat": 27}, {"id": 3, "name": "Beech", "unit": 3, "category": 6, "category__name": "Wood", "price": "3000.0000", "net_price": "2362.2000", "vat": 27}, {"id": 20, "name": "Black Stingray Pebbled Cow Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "70000.0000", "net_price": "55118.1100", "vat": 27}, {"id": 17, "name": "Espresso Stretch Lamb Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "30000.0000", "net_price": "23622.0500", "vat": 27}, {"id": 16, "name": "Large Red Doral Half Cow Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "50000.0000", "net_price": "39370.0800", "vat": 27}, {"id": 6, "name": "Mahogany", "unit": 3, "category": 6, "category__name": "Wood", "price": "10000.0000", "net_price": "7874.0200", "vat": 27}, {"id": 7, "name": "Maple", "unit": 3, "category": 6, "category__name": "Wood", "price": "5000.0000", "net_price": "3937.0100", "vat": 27}, {"id": 13, "name": "Medium Brown Doral Half Cow Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "50000.0000", "net_price": "39370.0800", "vat": 27}, {"id": 10, "name": "Medium Brown Escada Half Cow Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "60000.0000", "net_price": "47244.0900", "vat": 27}, {"id": 15, "name": "Medium Dark Green Doral Half Cow Leather", "unit": 2, "category": 3, "category__name": "Leather", "price": "45000.0000", "net_price": "35433.0700", "vat": 27}, {"id": 14, "name": "Medium White Doral Half Cow Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "70000.0000", "net_price": "55118.1100", "vat": 27}, {"id": 5, "name": "Oak", "unit": 3, "category": 6, "category__name": "Wood", "price": "5000.0000", "net_price": "3937.0100", "vat": 27}, {"id": 9, "name": "Pine", "unit": 3, "category": 6, "category__name": "Wood", "price": "2500.0000", "net_price": "1968.5000", "vat": 27}, {"id": 21, "name": "Realeather Metallic Bronze Deerskin Fringe Trim", "unit": 3, "category": 3, "category__name": "Leather", "price": "3000.0000", "net_price": "2362.2000", "vat": 27}, {"id": 23, "name": "Realeather Metallic Gunmetal Deerskin Fringe Trim", "unit": 2, "category": 3, "category__name": "Leather", "price": "3000.0000", "net_price": "2362.2000", "vat": 27}, {"id": 22, "name": "Realeather Metallic Silver Deerskin Fringe Trim", "unit": 2, "category": 3, "category__name": "Leather", "price": "3000.0000", "net_price": "2362.2000", "vat": 27}, {"id": 18, "name": "Red Stretch Lamb Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "70000.0000", "net_price": "55118.1100", "vat": 27}, {"id": 11, "name": "Small Brown Escada Half Cow Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "50000.0000", "net_price": "39370.0800", "vat": 27}, {"id": 12, "name": "Small Burnt Orange Doral Half Cow Leather", "unit": 2, "category": 3, "category__name": "Leather", "price": "37000.0000", "net_price": "29133.8600", "vat": 27}, {"id": 8, "name": "Walnut", "unit": 3, "category": 6, "category__name": "Wood", "price": "6000.0000", "net_price": "4724.4100", "vat": 27}, {"id": 19, "name": "White Patent Lamb Leather", "unit": 3, "category": 3, "category__name": "Leather", "price": "45000.0000", "net_price": "35433.0700", "vat": 27}]}';
    let productOrdersInput = [];

    try {
        initial = document.querySelector('input[name="initial-data"]').value;
        productOrdersInput = document.querySelector('input[name="product_orders"]');
    } catch (TypeError) {

    }

    const productOrders = productOrdersInput && productOrdersInput.value;
    const store = createStore(JSON.parse(initial), productOrders && JSON.parse(productOrders));
    outputState(store);
    checkClients(store);
    return store;
}


export function getRoot() {
    const root = document.querySelector('.react-root');
    let actualRoot;

    if (!root.classList.contains('cleaned')) {
        const unnecessaryItems = root.querySelectorAll(':not(input):not(link)');
        for (const item in unnecessaryItems)
            if (unnecessaryItems.hasOwnProperty(item)) {
                const current = unnecessaryItems[item];
                current.remove();
            }
        actualRoot = document.createElement('div');
        root.appendChild(actualRoot);
        root.classList.add('cleaned');
        root.classList.remove('hidden');
    }
    else {
        actualRoot = root.querySelector('div');
    }
    return actualRoot;
}