import {create, getProduct} from '../model';

const actions = {
    UPDATE_PRODUCT: 'UPDATE_PRODUCT',
    updateProduct: (id, values, parts) =>
        ({type: actions.UPDATE_PRODUCT, id, values, parts}),

    ADD_PRODUCT: 'ADD_PRODUCT',
    addProduct: () =>
        ({type: actions.ADD_PRODUCT}),

    REMOVE_PRODUCT: 'REMOVE_PRODUCT',
    removeProduct: (id) =>
        ({type: actions.REMOVE_PRODUCT, id}),

    MOVE_PRODUCT_DOWN: 'MOVE_PRODUCT_DOWN',
     moveProductDown: (id) =>
        ({type: actions.MOVE_PRODUCT_DOWN, id}),

    MOVE_PRODUCT_UP: 'MOVE_PRODUCT_UP',
    moveProductUp: (id) =>
        ({type: actions.MOVE_PRODUCT_UP, id}),

    SELECT_PRODUCT: 'SELECT_PRODUCT',
    selectProduct: (id, submit) =>
        ({type: actions.SELECT_PRODUCT, id, submit}),
};

export function updateProduct(id, values, parts) {
    return (dispatch) => {
        dispatch(actions.updateProduct(id, values, parts));
    }
}

export function addProduct() {
    return (dispatch) => {
        dispatch(actions.addProduct());
    }
}

export function removeProduct(id) {
    return (dispatch) => {
        dispatch(actions.removeProduct(id));
    }
}

export function moveProductDown(id) {
    return (dispatch) => {
        dispatch(actions.moveProductDown(id));
    }
}

export function moveProductUp(id) {
    return (dispatch) => {
        dispatch(actions.moveProductUp(id));
    }
}

export function selectProduct(id, submit = false) {
    return (dispatch) => {
        dispatch(actions.selectProduct(id, submit));
    }
}

function _detectProductPrice(productData, newProductState, oldProductState) {
    if (newProductState.product != oldProductState.product)
        return productData.price;
    return newProductState.price;
}

export default function (products) {
    const initState = {
        products: products || [],
        selectedProductId: null,
        backupProducts: null
    };
    return (state = initState, action) => {
        let products, index;
        switch (action.type) {
            case actions.UPDATE_PRODUCT:
                const productData = getProduct(action.values.product);
                const {parts} = productData;
                let productState = state.products.find(({id}) => id === action.id);
                productState = {
                    ...productState,
                    ...action.values,
                    price: _detectProductPrice(productData, action.values, productState),
                    parts: action.parts || (
                        action.values.product != productState.product ?
                            parts.map(x => ({...x, price: x.price || 0})) :
                            productState.parts
                    ),
                };
                products = state.products.map(({id, ...rest}) => id === action.id ? productState : {id, ...rest});
                return {...state, products};
            case actions.ADD_PRODUCT:
                products = [...state.products, create()];
                return {...state, products};
            case actions.REMOVE_PRODUCT:
                products = state.products.filter(({id}) => id !== action.id);
                return {...state, products};
            case actions.MOVE_PRODUCT_DOWN:
                index = state.products.findIndex(({id}) => id === action.id);
                products = Object.assign([], state.products, {
                    [index]: state.products[index + 1],
                    [index + 1]: state.products[index]
                });
                return {...state, products};
            case actions.MOVE_PRODUCT_UP:
                index = state.products.findIndex(({id}) => id === action.id);
                products = Object.assign([], state.products, {
                    [index]: state.products[index - 1],
                    [index - 1]: state.products[index]
                });
                return {...state, products};
            case actions.SELECT_PRODUCT:
                if (action.submit)
                    return {...state, selectedProductId: null, backupProducts: null};
                if (action.id)
                    return {...state, selectedProductId: action.id, backupProducts: state.products};
                return {...state, products: state.backupProducts, selectedProductId: null, backupProducts: null};
            default:
                return state;
        }
    }
}