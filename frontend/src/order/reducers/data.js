const actions = {
    FETCH_CLIENT: 'FETCH_CLIENT',
    fetchClient: (prices,) =>
        ({type: actions.FETCH_CLIENT, prices}),

};

export function fetchClient(id, interact) {
    return (dispatch) => {
        return fetch(`/api/1/client/${id}/`, {credentials: 'include'})
            .then(x => x.json())
            .then(({prices, ...client}) => {
                interact(client);
                dispatch(actions.fetchClient(prices));
            }).catch(console.error);
    }
}

export default function (initState) {
    return (state = initState, action) => {

        switch (action.type) {
            case actions.FETCH_CLIENT:
                return {...state, prices: action.prices};
            default:
                return state;
        }
    }
}