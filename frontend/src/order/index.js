import React from 'react'
import ReactDOM from 'react-dom'
import App from './order/App'
import {getInteractiveStore, getRoot} from "./order/interact";
// import './order/index.css';

window.addEventListener('load', function () {
    const store = getInteractiveStore();
    const ConnectedApp = (App) => (
        <App store={getInteractiveStore()}/>
    )
    const root = document.querySelector('.order-root')
    ReactDOM.render(ConnectedApp(App), root)

    if (module.hot) {
        module.hot.accept('./', () => {
            const NextApp = require('./order/Components/App').default
            const NextConnectedApp = ConnectedApp(NextApp)
            ReactDOM.render(
                NextConnectedApp,
                root
            )
        })
    }
})