import React, {Component} from 'react';
import {Provider} from 'react-redux';
import Order from './Components/App';



export default class extends Component {
    render() {
        const {store} = this.props;
        return (
            <Provider store={store}>
                <Order/>
            </Provider>
        );
    }
}
