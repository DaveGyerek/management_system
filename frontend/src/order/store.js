import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import state from './reducers/state';
import data from './reducers/data';

const middlewares = [thunk, logger];
let store = null;
export default function (initialData, productOrders) {
    return store || (store = createStore(
        combineReducers({
            state: state(productOrders), data: data(initialData)
        }),
        compose(applyMiddleware(...middlewares)),
    ));
}