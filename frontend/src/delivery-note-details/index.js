import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import {AppContainer} from 'react-hot-loader'
import registerServiceWorker from './delivery-note-details/registerServiceWorker';
import App from './delivery-note-details/App';

import CustomModal from './delivery-note-details/component/CustomModal';
import InvoiceModal from './delivery-note-details/component/InvoiceModal';
import Details from './delivery-note-details/component/Details';
import {details, deleteAll, invoice} from './delivery-note-details/interact';

window.addEventListener('load', function () {
    const root = document.querySelector('#delivery-note-root');
    const render = () => {
        Modal.setAppElement(root);
        ReactDOM.render(
            <React.Fragment>
                <App/>
                <Details initFunction={details}/>
                <CustomModal
                    initFunction={deleteAll}
                    title="Teljes törlés"
                    content="Biztos vagy benne, hogy törlöd az összes terméket ebben a szállítólevélben?"
                    url={`/api/1/ordered-product/delete-from-delivery-note/${window.deliveryNoteId}/`}
                />
                {/*<CustomModal*/}
                    {/*initFunction={invoice}*/}
                    {/*title="Számlázás"*/}
                    {/*content="Biztosan továbbítsuk a rendeléseket a számlázóba?"*/}
                    {/*url={`/api/1/invoice/create-for-delivery-note/${window.deliveryNoteId}/`}*/}
                {/*/>*/}
                <InvoiceModal
                    initFunction={invoice}
                    title="Számlázás"
                    content="Biztosan továbbítsuk a rendeléseket a számlázóba?"
                    url={`/api/1/invoice/create-for-delivery-note/${window.deliveryNoteId}`}
                />
            </React.Fragment>,
            root,
        )
    };
    render();

    if (module.hot) {
        module.hot.accept('./App', () => {
            render()
        });
    }
});


registerServiceWorker();
