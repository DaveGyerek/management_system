import React, {Component} from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

import {modalStyle, getCookie} from './utils';

class CustomModal extends Component {
    state = {
        modalVisible: false,
        waiting: false,
    };

    constructor(props) {
        super(props);
        this.props.initFunction(this.showModal);
    }


    showModal = () => {
        this.setState({
            modalVisible: true
        })
    };

    handleApiCall = () => {
        this.setState({waiting: true});

        fetch(this.props.url, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
        }).then(response => {
            // if (response.ok)
            window.location.href = '/deliverynote/';
            // throw new Error(response.statusText);
        }).catch(console.error);
    };

    render() {
        const {modalVisible} = this.state;
        return (
            <Modal
                isOpen={modalVisible}
                // onRequestClose={this.closeModal}
                style={modalStyle}
                closeTimeoutMS={1000}
                onRequestClose={() => {
                    this.setState({modalVisible: false});
                }}
            >
                <div className="dialog">
                    <div className="dialog-header">
                        {this.props.title}
                    </div>
                    <div className="dialog-content">
                        <p>
                            {this.props.content}
                        </p>
                    </div>
                </div>
                <div className="dialog-footer">
                    {this.state.waiting ? (
                        <button className="button icon icon-spinner"/>
                    ) : (
                        <button
                            type="button"
                            className="button bicolor icon icon-fa-check"
                            onClick={this.handleApiCall}
                        >
                            Mehet
                        </button>
                    )}
                </div>
            </Modal>
        );
    }
}

CustomModal.propTypes = {
    initFunction: PropTypes.func.isRequired,

    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
};

export default CustomModal;
