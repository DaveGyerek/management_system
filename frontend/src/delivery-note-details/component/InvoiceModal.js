import React, {Component} from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

import {modalStyle, getCookie} from './utils';

class InvoiceModal extends Component {
    state = {
        modalVisible: false,
        waiting: false,

        closeDelivery: true,
    };

    constructor(props) {
        super(props);
        this.props.initFunction(this.showModal);
    }


    showModal = () => {
        this.setState({
            modalVisible: true
        })
    };

    handleCheckBox = (e) => {
        this.setState({
            closeDelivery: e.target.checked
        })
    };

    handleApiCall = () => {
        this.setState({waiting: true});

        let url = this.props.url;
        if (this.state.closeDelivery)
            url += "?close=true";

        fetch(url, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
        }).then(response => {
            if (response.ok)
                return window.location.reload();
            throw new Error(response.statusText);
        }).catch(console.error);
    };

    render() {
        const {modalVisible} = this.state;
        return (
            <Modal
                isOpen={modalVisible}
                // onRequestClose={this.closeModal}
                style={modalStyle}
                closeTimeoutMS={1000}
                onRequestClose={() => {
                    this.setState({modalVisible: false});
                }}
            >
                <div className="dialog">
                    <div className="dialog-header">
                        {this.props.title}
                    </div>
                    <div className="dialog-content">
                        <p>
                            {this.props.content}
                        </p>
                        <p>
                            Lezárás:

                            <input style={{margin: "5px"}} type="checkbox" checked={this.state.closeDelivery}
                                   onChange={this.handleCheckBox}/>
                        </p>
                    </div>
                </div>
                <div className="dialog-footer">
                    {this.state.waiting ? (
                        <button className="button icon icon-spinner"/>
                    ) : (
                        <button
                            type="button"
                            className="button bicolor icon icon-fa-check"
                            onClick={this.handleApiCall}
                        >
                            Mehet
                        </button>
                    )}
                </div>
            </Modal>
        );
    }
}

InvoiceModal.propTypes = {
    initFunction: PropTypes.func.isRequired,

    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
};

export default InvoiceModal;
