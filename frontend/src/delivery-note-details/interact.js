import $ from 'jquery';

export function setToReady(callback) {
    const $ = window.$;
    const moveButton = $("#move-btn");
    const deleteButton = $("#delete-btn");

    const func = e => {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const rows = $("table.listing tbody tr");
        const products = rows.map(function () {
            return {
                id: parseInt($(this).find("input[type=checkbox]").attr("id") || $(this).attr("data-object-pk")),
                checked: $(this).find("input[type=checkbox]")[0].checked,
                name: $(this).find("td").eq(1).html(),
                quantity: parseInt($(this).find("td").eq(2).html()),
            }
        });
        callback(products.get(), e.target.id === "delete");
    };

    moveButton.off('click');
    moveButton.click(func);

    deleteButton.off('click');
    deleteButton.click(func);
}

export function details(callback) {
    const $ = window.$;
    // const details = window.details['product_orders'];

    const detailsFunc = e => {
        if (e.target.type === "checkbox" || e.target.classList.contains('field-checkbox')) {
            e.stopPropagation();
        } else {
            const row = $(e.target.closest('tr'));
            // const context = details.find(i => i.id === parseInt(row.attr('id')));
            // callback(context);

            fetch(`/api/1/delivery-note/${window.deliveryNoteId}/ordered-product/${parseInt(row.attr('id'))}/`, {credentials: 'include'})
                .then(x => x.json())
                .then((data) => {
                    callback(data);
                }).catch(console.error);
        }
    };

    const rows = $(".clickable-row");
    rows.off('click');
    rows.click(detailsFunc);

}

export function deleteAll(callback) {
    const $ = window.$;
    const deleteButton = $("#all-delete");

    const deleteAllFunc = () => {
        callback()
    };

    deleteButton.off('click');
    deleteButton.click(deleteAllFunc);
}

export function invoice(callback) {
    const $ = window.$;
    const invoiceButton = $("#invoice");

    const invoiceFunc = () => {
        callback()
    };

    invoiceButton.off('click');
    invoiceButton.click(invoiceFunc);
}

export function printLabels(body) {
    createIframe();
    $('#loading-modal').show();
    $('#labels-iframe').on('load', () => {
        $('#loading-modal').fadeOut();
        openLabels();
    });

    function createIframe() {
        let url = '/api/1/generate-label/';
        if (body.ordered_products.length > 0)
            url += '?ordered_products=' + body.ordered_products;
        if (body.labels.length > 0)
            url += body.ordered_products.length > 0 ? `&labels=${body.labels}` : `?labels=${body.labels}`;

        if (!document.getElementById("labels-iframe")) {
            let i = document.createElement("iframe");
            i.id = "labels-iframe";
            i.src = url;
            i.name = "pdf";
            document.getElementById("iframe-div").appendChild(i);
        }
        else {
            let i = document.getElementById("labels-iframe");
            i.id = "labels-iframe";
            i.src = url;
            i.name = "pdf";
        }

    }

    function openLabels() {
        const frame = window.frames['pdf'];
        frame.focus();
        frame.print()
    }
}

export default {setToReady, details, deleteAll, invoice, printLabels}