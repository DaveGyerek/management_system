import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import {AppContainer} from 'react-hot-loader'
import registerServiceWorker from './manufact-details/registerServiceWorker';

import {details} from './manufact-details/interact'
import App from './manufact-details/App';
// import Details from './Details';
import Details from './manufact-details/component/Details';

window.addEventListener('load', function () {
    const root = document.querySelector('#manufact-details-root');
    const render = () => {
        Modal.setAppElement(root);
        ReactDOM.render(
            <React.Fragment>
                <AppContainer>
                    <App/>
                </AppContainer>
                <AppContainer>
                    <Details initFunction={details} />
                </AppContainer>
            </React.Fragment>,
            root,
        )
    };
    render();

    if (module.hot) {
        module.hot.accept('./App', () => {
            render()
        });
    }
});


registerServiceWorker();
