import $ from 'jquery';

export function setToReady(callback) {
    const $ = window.$;
    const func = e => {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const isDelete = e.target.hasAttribute("isDelete");
        const rows = $("table.listing tbody tr");
        const products = rows.map(function () {
            let quantityValues = $(this).find("td").eq(2).html().split('/');
            let quantity = parseInt(quantityValues[1]) - parseInt(quantityValues[0]);

            return {
                id: parseInt($(this).find("input[type=checkbox]").attr("id") || $(this).attr("data-object-pk")),
                checked: $(this).find("input[type=checkbox]")[0].checked,
                name: $(this).find("td").eq(3).html(),
                quantity: quantity,
            }
        });
        callback(products.get(), isDelete);
    };
    const single_func = e => {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        const row = $(e.target.closest('tr'));
        let quantityValues = row.find("td").eq(2).html().split('/');
        let quantity = parseInt(quantityValues[1]) - parseInt(quantityValues[0]);

        const product = [{
            id: parseInt(row.find("input[type=checkbox]").attr("id") || row.attr("data-object-pk")),
            checked: true,
            name: row.find("td").eq(3).html(),
            quantity: quantity
        }];
        callback(product, true);
    };
    const submitBtn = $("#submit-btn");
    const submitDeleteBtn = $("#submit-delete-btn");
    const deleteButtons = $('.delete-btn');

    submitBtn.off('click');
    submitBtn.click(func);
    submitDeleteBtn.off('click');
    submitDeleteBtn.click(func);

    deleteButtons.off('click');
    deleteButtons.click(single_func);
}

export function details(callback) {
    const $ = window.$;
    // const details = window.details['product_orders'];

    const detailsFunc = e => {
        if (e.target.type === "checkbox" || e.target.classList.contains('field-checkbox')) {
            e.stopPropagation();
        } else {
            const row = $(e.target.closest('tr'));
            // const context = details.find(i => i.id === parseInt(row.attr('id')));

            fetch(`/api/1/manufacture-plan/${window.manufactureId}/ordered-product/${parseInt(row.attr('id'))}/`, {credentials: 'include'})
                .then(x => x.json())
                .then((data) => {
                    callback(data);
                }).catch(console.error);

            // callback(context);
        }
    };

    const rows = $(".clickable-row");
    rows.off('click');
    rows.click(detailsFunc);

}

export function printLabels(body) {
    createIframe();
    $('#loading-modal').show();
    $('#labels-iframe').on('load', () => {
        $('#loading-modal').fadeOut();
        openLabels();
    });

    function createIframe() {
        let url = '/api/1/generate-label/';
        if (body.ordered_products.length > 0)
            url += '?ordered_products=' + body.ordered_products;
        if (body.labels.length > 0)
            url += body.ordered_products.length > 0 ? `&labels=${body.labels}` : `?labels=${body.labels}`;

        if (!document.getElementById("labels-iframe")) {
            let i = document.createElement("iframe");
            i.id = "labels-iframe";
            i.src = url;
            i.name = "pdf";
            document.getElementById("iframe-div").appendChild(i);
        }
        else {
            let i = document.getElementById("labels-iframe");
            i.id = "labels-iframe";
            i.src = url;
            i.name = "pdf";
        }

    }

    function openLabels() {
        const frame = window.frames['pdf'];
        frame.focus();
        frame.print()
    }
}

export default {setToReady, details, printLabels}