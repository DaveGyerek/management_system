import React, {Component} from 'react';
import {setToReady} from './interact';
import Modal from 'react-modal';

export const modalStyle = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(71, 71, 71, 0.9)',
        zIndex: 100,
    },
    content: {
        position: 'absolute',
        top: '15%',
        left: '25%',
        width: '50%',
        bottom: 'unset',
        right: 'unset',
        border: '1px solid #ccc',
        background: '#fff',
        overflow: 'auto',
        WebkitOverflowScrolling: 'touch',
        borderRadius: '4px',
        outline: 'none',
        padding: '20px',
        maxHeight: '75vh'
    }
};

Array.prototype.updateItem = function (selector, getNewItem) {
    return this.map(x => selector(x) ? getNewItem(x) : x);
};

export function getCookie(name) {
    const value = "; " + document.cookie;
    const parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}


export default class extends Component {
    initState = {
        products: [],
        modalVisible: false,
        isLoading: false,
        error: null,
        isDelete: false,
    };

    state = this.initState;

    constructor(props) {
        super(props);
        setToReady(this.handleProducts);
    }

    setState(nextState, callback) {
        console.log({
            currentState: this.state,
            nextState
        });
        super.setState(nextState, callback);
    }

    handleProducts = (products, isDelete) => {
        this.setState({
            products: products.filter(({checked}) => checked).map(({quantity, ...rest}) => ({
                ...rest,
                quantity,
                originalQuantity: quantity
            })),
            modalVisible: true,
            isDelete: isDelete
        })
    };

    handleClose = () => {
        this.setState(this.initState);
    };

    handleChangeQuantity = (id, quantity) => {
        this.setState({
            products: this.state.products.updateItem(
                ({id: currentId}) => id === currentId,
                props => ({
                    ...props,
                    quantity
                }))
        })
    };

    handleSave = () => {
        window.manufactureId && fetch(window.submitUrl, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify({
                id: window.manufactureId,
                product_orders: this.state.products.map(({id, quantity}) => ({
                    id, quantity
                }))
            })
        }).then(response => {
            if (response.ok)
                return window.location.reload();
            throw new Error(response.statusText);
        }).catch(console.error);
    };

    handleDelete = () => {
        window.manufactureId && fetch(window.deleteUrl, {
            credentials: 'same-origin',
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': getCookie('csrftoken')
            },
            body: JSON.stringify({
                id: window.manufactureId,
                product_orders: this.state.products.map(({id, quantity}) => ({
                    id, quantity
                }))
            })
        }).then(response => {
            if (response.ok)
                return window.location.reload();
            throw new Error(response.statusText);
        }).catch(console.error);
    };

    renderProducts() {
        const {products} = this.state;
        return (
            <table className="modal-table table inline-table" style={{marginTop: 0}}>
                <thead>
                <tr>
                    <th style={{textAlign: 'left'}}>Megnevezés</th>
                    <th>Mennyiség</th>
                </tr>
                </thead>
                <tbody>

                {products.map(({id, name, quantity, originalQuantity}) => (
                    <tr key={id}>
                        <td>{name}</td>
                        <td>
                            <input
                                type="number"
                                value={quantity}
                                max={originalQuantity}
                                min={1}
                                onChange={(e) => this.handleChangeQuantity(id, parseInt(e.target.value))}
                            />
                        </td>
                    </tr>
                ))}

                </tbody>
            </table>
        )
    }

    render() {
        const {modalVisible} = this.state;
        const headerText = this.state.isDelete ?
            "Kiválasztottak törlése a gyártási tervből" :
            "Kiválasztottak küldése készáru raktárba";
        if (this.state.products.length > 0)
            return (
                <Modal
                    isOpen={modalVisible}
                    // onRequestClose={this.closeModal}
                    style={modalStyle}
                    closeTimeoutMS={1000}
                    onRequestClose={() => {
                        this.setState({modalVisible: false});
                    }}
                >
                    <div className="iziModal hasScroll" style={{display: "block", position: "unset"}}>
                        <div className="">
                            <div className="iziModal-header">
                                {headerText}
                            </div>
                            {this.renderProducts()}
                            <div className="iziModal-footer" style={{marginTop: 10}}>
                                {this.state.isDelete ? (
                                    <button type="button" className="button no bicolor icon icon-fa-times"
                                            onClick={this.handleDelete}>
                                        Törlés
                                    </button>
                                ) : (
                                    <button type="button" className="button bicolor icon icon-fa-send"
                                            onClick={this.handleSave}>
                                        Küldés
                                    </button>
                                )}
                            </div>
                        </div>
                    </div>
                </Modal>
            );
        return (null)
    }
}
