import faker from "faker";
import puppeteer from "puppeteer";

const APP = "http://127.0.0.1:19004/order/create/";

const lead = {
  client: faker.random.words(),
  store: faker.random.words(),
  billing_address: faker.random.words(),
  delivery_address: faker.random.words(),
  order_timestamp: faker.timestamp(),
  delivery_timestamp: faker.datetime(),
  note: faker.random.words()
};

let page;
let browser;
const width = 1920;
const height = 1080;